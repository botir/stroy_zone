-- MySQL dump 10.13  Distrib 8.0.20, for Linux (x86_64)
--
-- Host: localhost    Database: mb_db2
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `advantages`
--

DROP TABLE IF EXISTS `advantages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `advantages` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advantages`
--

LOCK TABLES `advantages` WRITE;
/*!40000 ALTER TABLE `advantages` DISABLE KEYS */;
INSERT INTO `advantages` VALUES (12,'Имеем международный опыт','5ecf37952b2a0.svg',1,'2020-05-28 04:01:25','2020-05-28 04:01:25'),(13,'Креативные идеи и новаторские решения','5ecf37cfab5f5.svg',2,'2020-05-28 04:02:23','2020-06-16 15:34:55'),(14,'Новые технологии проектирования','5ecf37eaaf237.svg',3,'2020-05-28 04:02:50','2020-06-16 15:34:49'),(15,'Индивидуальный подход к каждому клиенту и личный проект-менеджер','5ecf37f68adfa.svg',4,'2020-05-28 04:03:02','2020-06-16 15:36:30'),(16,'Гарантируем высокое качество','5ecf3802449b3.svg',5,'2020-05-28 04:03:14','2020-06-09 15:38:42'),(17,'Примеры конкретных объектов, уже сданных в эксплуатацию','5ecf380d2d5a4.svg',6,'2020-05-28 04:03:25','2020-05-28 04:03:25');
/*!40000 ALTER TABLE `advantages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contents` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `meta_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `logo_white` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo_black` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_img` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `about_img` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advantage_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `target_img` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `portfolio_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `portfolio_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `portfolio_img` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `service_img` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `carrier_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `carrier_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `carrier_img` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jobs_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jobs_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `jobs_img` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `partner_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partner_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `partner_img` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_with_us_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `contact_img` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `facebook` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telegram` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telegram2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents`
--

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;
INSERT INTO `contents` VALUES (3,'Архитектурная компания, предоставляющая комплекс услуг по проектированию и реализации дизайна жилых и общественных зданий в Ташкенте','Компания предлагает широкий спектр услуг в сфере архитектурного проектирования, дизайна и строительства жилых и общественных зданий, применяя современные методы проектирования и систематизации процессов.','проектирование, строительства, дизайн,дизайн квартир в ташкенте,архитектура и дизайн в ташкенте,mbc,жк феличита,жк дустлар,жк кембридж резиденс,Cambridge Residence,дизайн проект интерьера квартиры,дизайн проект ремонта квартиры,проект жилого комплекса,проекты новых жилых комплексов,проект строительство жилого комплекса','5ede67c188b79.png','5ede67c1a4671.png','Индивидуальные решения и грамотное воплощение!','5f3d118e1746f.jpg','О нас','«MB Studio» — это прогрессивно развивающаяся студия дизайна, архитектурного проектирования и строительства с международным потенциалом.','5f1dbd6307569.png','Наши преимущества','Корпоративная философия','<p><b>                                Миссия</b> - претворять самые смелые идеи в жизнь;\r\n⠀\r\n</p><p><b>Видение</b> - развивать архитектурную культуру и стать ведущей архитектурно-строительной компанией в мире;\r\n⠀\r\n</p><p><b>Принципы </b>- новые технологии, креативность и индивидуальный подход.\r\n                            </p>','5ee8ba8fb3e5f.jpg','Портфолио','Наше портфолио пересекает стандартные границы, которые вы давно знаете. Познакомтесь с инновационными проектами от MB STUDIO.','5eeb5b0eb47ca.jpg','Услуги','Мы ознакомим Вас с ассортиментом рынка строительных материалов и будем согласовывать каждый элемент вашей мечты, и Вы будете знать все экономические аспекты до начала реализации проекта','5ede67c3ed9e1.jpg','Карьера','Команда амбициозных специалистов, которые используют инновации и креативность, самая важная часть нашего пути к реализации успешных проектов вашей мечты отвечающие всем современным тенденциям!','5ee8af3078a00.jpg','Вакансии','Ставим амбициозные цели и никогда не останавливаемся на достигнутом. У тебя будет возможность реализовывать WOW-проекты, прокачивать свои навыки и смело заявлять о себе.\r\nLet’s go к нам!','5ee9e2a975c0c.jpg','Наша команда','Наша команда сочетает в себе творческий потенциал, свежие идеи и новации. Смешивая это с богатым опытом  и талантом осуществляем прорывы, создаем новые проекты, которые превосходят ожидания потребителей.','Партнеры','Наша команда активно работает над развитием своей партнерской сети.\r\nДля того, чтобы эффективно решать задачи клиентов, мы используем в проектах решения наших профессиональных партнеров.\r\nНаши партнеры имеют многолетний опыт работы на рынке строительства. Мы всегда готовы порекомендовать вам партнера, имеющего необходимую специализацию.','5f3bca1caa446.jpg','Сотрудничаем','Контакты','Будем рады Вас консультировать по любым интересующим Вас вопросам!','5eeb1cd1dd295.jpg','<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d5994.448245862793!2d69.243326802655!3d41.30398770619608!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2s!4v1590648674228!5m2!1sru!2s\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>','https://www.facebook.com/mbstudio.uz','https://instagram.com/mbstudio_uz/',NULL,'Ойбек 38 А, г.Ташкент','+998 99 8186100','info@mbstudio.uz',NULL,'2020-06-08 21:30:57','2020-10-13 15:14:56');
/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint unsigned NOT NULL,
  `reserved_at` int unsigned DEFAULT NULL,
  `available_at` int unsigned NOT NULL,
  `created_at` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2020_03_21_034646_create_jobs_table',1),(5,'2020_04_24_214517_create_contents_table',1),(6,'2020_04_24_214547_create_partners_table',1),(7,'2020_04_24_214620_create_galleries_table',1),(8,'2020_04_24_214700_create_news_table',1),(9,'2020_05_28_073608_create_teams_table',2),(10,'2020_05_28_074632_create_portfolios_table',2),(11,'2020_05_28_074724_create_advantages_table',2),(12,'2020_07_25_224329_add_contacts_to_contents_table',3),(13,'2020_07_26_114634_create_portfolio_categories_table',3),(14,'2020_07_26_120536_change_portfolios_table',3),(15,'2020_08_20_094711_add_work_with_us_title_to_contents',4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `news` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `img` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `sort` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (5,'5ecf38416673a.jpg','Концептуальное проектирование','Концептуальный проект является предварительной стадией проектирование. Именно на этом этапе определяются основные параметры будущего объекта: его архитектура, стиль, основной концепт внешнего вида и внутренней схематичной планировки',1,'2020-05-28 04:04:17','2020-10-13 14:32:12'),(6,'5ecf385648e6c.jpg','Эскизный дизайн','Эскизный проект дает возможность получить детальное представление о будущем строении, а именно увидеть чертежи внутренней планировки этажей и внешний вид дома. Технико-экономическое обоснование проекта - вся информация для расчета финансовой модели.',2,'2020-05-28 04:04:38','2020-10-13 14:31:32'),(7,'5f4a46c449192.jpg','Дизайн-проект интерьера','Дизайн-проект интерьера — это идея решения внутреннего пространства.',3,'2020-05-28 04:04:57','2020-10-14 17:48:52'),(8,'5f3d1684a3d4a.jpg','Рабочий проект','Это комплект документации, согласно которым будут вестись ремонтные и строительные работы.',4,'2020-05-28 04:05:29','2020-10-13 12:33:39');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `partners` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `img` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partners`
--

LOCK TABLES `partners` WRITE;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
INSERT INTO `partners` VALUES (11,'5efc522082e5b.png','Murad Buildings','https://www.mbc.uz/','2020-06-08 21:37:36','2020-07-01 14:06:40'),(12,'5efb2f91f0c03.png','Cambridge Residence','https://cr.mbc.uz/','2020-06-08 21:42:53','2020-06-30 17:26:58'),(13,'5efc541b5142e.png','NRG','https://u-nrg.uz/','2020-06-08 21:43:10','2020-07-01 14:15:07'),(16,'5efc524f0155c.png','Darkhan Avenue','http://darkhan-avenue.uz/','2020-07-01 10:45:41','2020-07-01 14:07:33');
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio_categories`
--

DROP TABLE IF EXISTS `portfolio_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `portfolio_categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio_categories`
--

LOCK TABLES `portfolio_categories` WRITE;
/*!40000 ALTER TABLE `portfolio_categories` DISABLE KEYS */;
INSERT INTO `portfolio_categories` VALUES (1,'Реализованные проекты',NULL,1,'2020-07-26 22:28:23','2020-07-26 22:28:23'),(2,'Концепты творчество',NULL,2,'2020-07-26 22:28:31','2020-07-26 22:28:31');
/*!40000 ALTER TABLE `portfolio_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolios`
--

DROP TABLE IF EXISTS `portfolios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `portfolios` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `portfolios` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` tinyint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolios`
--

LOCK TABLES `portfolios` WRITE;
/*!40000 ALTER TABLE `portfolios` DISABLE KEYS */;
INSERT INTO `portfolios` VALUES (36,'ЖК \"Cambridge Residence\"',1,'[{\"title\":null,\"photo\":\"5f1dbe2b2f8ef.jpg\"},{\"title\":null,\"photo\":\"5f1f32aaa0ee9.jpg\"},{\"title\":null,\"photo\":\"5f1f30faebf69.jpg\"},{\"title\":null,\"photo\":\"5f1f3002a6bfd.jpg\"},{\"title\":null,\"photo\":\"5f1f333aa0d4b.jpg\"},{\"title\":null,\"photo\":\"5f1f341c2d934.jpg\"},{\"title\":null,\"photo\":\"5f1f33ba4a371.jpg\"},{\"title\":null,\"photo\":\"5f1f33bac371e.jpg\"},{\"title\":null,\"photo\":\"5f1f33bb42caf.jpg\"}]','2020-07-26 22:32:27','2020-07-28 01:07:56',1),(37,'ЖК NRG \"Oybek\"',2,'[{\"title\":null,\"photo\":\"5f1dbe6e1d404.jpg\"},{\"title\":null,\"photo\":\"5f3d0dc5b2db2.jpg\"},{\"title\":null,\"photo\":\"5f3d0dc619d38.jpg\"},{\"title\":null,\"photo\":\"5f3d0dd845a41.jpg\"},{\"title\":null,\"photo\":\"5f3d0dd8d719a.jpg\"},{\"title\":null,\"photo\":\"5f3d0f20c5b06.jpg\"},{\"title\":null,\"photo\":\"5f3d0f2127cac.jpg\"},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null}]','2020-07-26 22:33:34','2020-08-19 16:38:09',1),(38,'CIS Kindergarten',3,'[{\"title\":null,\"photo\":\"5f1dbe9ad72ff.jpg\"},{\"title\":null,\"photo\":\"5f3d1c258d1a5.jpg\"},{\"title\":null,\"photo\":\"5f35a39e6a43c.jpg\"},{\"title\":null,\"photo\":\"5f36f8c31ecdf.jpg\"},{\"title\":null,\"photo\":\"5f3d1c555721d.jpg\"},{\"title\":null,\"photo\":\"5f3d1c7df21d0.jpg\"},{\"title\":null,\"photo\":\"5f3d1c9977b74.jpg\"},{\"title\":null,\"photo\":\"5f3d1c9aebb48.jpg\"},{\"title\":null,\"photo\":\"5f3d1c9c0622b.jpg\"}]','2020-07-26 22:34:18','2020-08-19 17:35:40',1),(39,'ЖК \"Felicita\"',5,'[{\"title\":null,\"photo\":\"5f1dbec9403aa.jpg\"},{\"title\":null,\"photo\":\"5f3d322fb0a1a.jpg\"},{\"title\":null,\"photo\":\"5f3d32304af2f.jpg\"},{\"title\":null,\"photo\":\"5f3d3230d8b6f.jpg\"},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null}]','2020-07-26 22:35:05','2020-08-19 19:07:45',1),(40,'CIS International School',4,'[{\"title\":null,\"photo\":\"5f1dbf1a4219b.jpg\"},{\"title\":null,\"photo\":\"5f3d299c63991.jpg\"},{\"title\":null,\"photo\":\"5f3d29bf61605.jpg\"},{\"title\":null,\"photo\":\"5f3d29c00154e.jpg\"},{\"title\":null,\"photo\":\"5f3d2a576411e.jpg\"},{\"title\":null,\"photo\":\"5f3d2a6e7b9fb.jpg\"},{\"title\":null,\"photo\":\"5f3d2aca0c6c1.jpg\"},{\"title\":null,\"photo\":\"5f3d2aca96ca7.jpg\"},{\"title\":null,\"photo\":\"5f3d2acb3c8cc.jpg\"}]','2020-07-26 22:36:26','2020-08-19 18:36:11',1),(41,'\"Ko`kcha Village\"',6,'[{\"title\":null,\"photo\":\"5f1dbf3a0082b.jpg\"},{\"title\":null,\"photo\":\"5f3d066cbb23d.jpg\"},{\"title\":null,\"photo\":\"5f3d067dc5735.jpg\"},{\"title\":null,\"photo\":\"5f3d067e3e93e.jpg\"},{\"title\":null,\"photo\":\"5f3d06967c24e.jpg\"},{\"title\":null,\"photo\":\"5f3d0696f337d.jpg\"},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null}]','2020-07-26 22:36:58','2020-08-21 12:15:20',1),(42,'ЖК \"Do`stlar\"',7,'[{\"title\":null,\"photo\":\"5f2bb48c9cb88.jpg\"},{\"title\":null,\"photo\":\"5f2bb4e4a0684.jpg\"},{\"title\":null,\"photo\":\"5f2bb5019088e.jpg\"},{\"title\":null,\"photo\":\"5f2bb517b55c5.jpg\"},{\"title\":null,\"photo\":\"5f2bb52d3d792.jpg\"},{\"title\":null,\"photo\":\"5f2bb5474c574.jpg\"},{\"title\":null,\"photo\":\"5f2bb56ba7e8c.jpg\"},{\"title\":null,\"photo\":\"5f2bb5808f22d.jpg\"},{\"title\":null,\"photo\":\"5f2bb580e4f85.jpg\"}]','2020-07-26 22:37:32','2020-08-06 12:47:13',1),(43,'Частный жилой дом',8,'[{\"title\":null,\"photo\":\"5f1dbf83ce143.jpg\"},{\"title\":null,\"photo\":\"5f359c3b7a2de.jpg\"},{\"title\":null,\"photo\":\"5f359c3c4494f.jpg\"},{\"title\":null,\"photo\":\"5f359f148b7b3.jpg\"},{\"title\":null,\"photo\":\"5f35a1e7a6c94.jpg\"},{\"title\":null,\"photo\":\"5f35a1f9c45aa.jpg\"},{\"title\":null,\"photo\":\"5f35a26313f68.jpg\"},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null}]','2020-07-26 22:38:11','2020-08-14 01:28:19',1),(44,'ЖК \"Darkhan Avenue\"',9,'[{\"title\":null,\"photo\":\"5f1dbfd948809.jpg\"},{\"title\":null,\"photo\":\"5f3cfec09225d.jpg\"},{\"title\":null,\"photo\":\"5f3cff1a7dfd5.jpg\"},{\"title\":null,\"photo\":\"5f3cff332b7b8.jpg\"},{\"title\":null,\"photo\":\"5f3cff33d0885.jpg\"},{\"title\":null,\"photo\":\"5f3cff75c2806.jpg\"},{\"title\":null,\"photo\":\"5f3cff6047b68.jpg\"},{\"title\":null,\"photo\":\"5f3cff766e8b0.jpg\"},{\"title\":null,\"photo\":\"5f3cffe0532d8.jpg\"}]','2020-07-26 22:39:37','2020-08-19 17:53:10',1),(45,'Портфолио 1',1,'[{\"title\":null,\"photo\":\"5f8442c6b06f9.jpg\"},{\"title\":null,\"photo\":\"5f8446705aa7a.jpg\"},{\"title\":null,\"photo\":\"5f84467115743.jpg\"},{\"title\":null,\"photo\":\"5f84464e8eea8.jpg\"},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null}]','2020-07-26 22:40:28','2020-10-12 17:05:05',2),(46,'Yunusabad',2,'[{\"title\":null,\"photo\":\"5f1dc0369dca9.jpg\"},{\"title\":null,\"photo\":\"5f479b2992314.jpg\"},{\"title\":null,\"photo\":\"5f479b2a4d045.jpg\"},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null}]','2020-07-26 22:41:10','2020-08-27 16:38:18',2),(47,'Частный жилой дом',3,'[{\"title\":null,\"photo\":\"5f8437b480a0e.jpg\"},{\"title\":null,\"photo\":\"5f806278749fd.jpg\"},{\"title\":null,\"photo\":\"5f806278ed3a2.jpg\"},{\"title\":null,\"photo\":\"5f83f19c54b5f.jpg\"},{\"title\":null,\"photo\":\"5f83f1acd1757.jpg\"},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null}]','2020-07-26 22:41:35','2020-10-12 16:02:13',2),(48,'Оффис',4,'[{\"title\":null,\"photo\":\"5f47a304c4ca5.jpg\"},{\"title\":null,\"photo\":\"5f47a3052ee18.jpg\"},{\"title\":null,\"photo\":\"5f47a32d8e085.jpg\"},{\"title\":null,\"photo\":\"5f47a32deba58.jpg\"},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null}]','2020-07-26 22:42:13','2020-08-27 17:12:30',2),(49,'\"CROWN\"',5,'[{\"title\":null,\"photo\":\"5f84260f9c96a.jpg\"},{\"title\":null,\"photo\":\"5f8425b2ae52f.jpg\"},{\"title\":null,\"photo\":\"5f8425c479101.jpg\"},{\"title\":null,\"photo\":\"5f8425f3a17ae.jpg\"},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null}]','2020-07-26 22:43:05','2020-10-12 14:46:55',2),(50,'Портфолио 6',6,'[{\"title\":null,\"photo\":\"5f84066fa46c9.jpg\"},{\"title\":null,\"photo\":\"5f84069863cea.jpg\"},{\"title\":null,\"photo\":\"5f8406d060183.jpg\"},{\"title\":null,\"photo\":\"5f8406d0d409a.jpg\"},{\"title\":null,\"photo\":\"5f8406e42bbec.jpg\"},{\"title\":null,\"photo\":\"5f8406e49d1ec.jpg\"},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null}]','2020-07-26 22:44:33','2020-10-12 12:33:57',2),(51,'Дизайн интерьера частного дома',7,'[{\"title\":null,\"photo\":\"5f882548c58e8.jpg\"},{\"title\":null,\"photo\":\"5f882549726c5.jpg\"},{\"title\":null,\"photo\":\"5f88258d9a0af.jpg\"},{\"title\":null,\"photo\":\"5f88258e37c88.jpg\"},{\"title\":null,\"photo\":\"5f88249d5e5a2.jpg\"},{\"title\":null,\"photo\":\"5f88258ed1266.jpg\"},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null}]','2020-07-26 22:45:16','2020-10-15 15:33:51',2),(52,'Портфолио 8',8,'[{\"title\":null,\"photo\":\"5f1dc15bee9b3.jpg\"},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null}]','2020-07-26 22:46:03','2020-07-26 22:46:04',2),(53,'IT School',9,'[{\"title\":null,\"photo\":\"5f3d201ca86e9.jpg\"},{\"title\":null,\"photo\":\"5f4793cfe69fe.jpg\"},{\"title\":null,\"photo\":\"5f4793d0314a6.jpg\"},{\"title\":null,\"photo\":\"5f4793d0a6ea6.jpg\"},{\"title\":null,\"photo\":\"5f4793d115066.jpg\"},{\"title\":null,\"photo\":\"5f4793d1753c8.jpg\"},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null},{\"title\":null,\"photo\":null}]','2020-07-26 22:46:41','2020-08-27 16:06:57',2);
/*!40000 ALTER TABLE `portfolios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teams` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `img` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (3,'5f10bb70b17ce.png','Мираббос Обидов','Архитектор-Дизайнер',NULL,'mirabbos.obidov@mbstudio.uz',6,'2020-07-17 01:26:18','2020-08-06 04:46:53'),(4,'5f10bbcc2e29c.png','Шерзод Маматов','Архитектор-Дизайнер',NULL,'sherzod.mamatov@mbstudio.uz',5,'2020-07-17 01:42:52','2020-08-06 04:45:46'),(5,'5f10bc53d16d6.png','Зафар Ташмухамедов','Главный Архитектор Проекта',NULL,'zafar.tashmukhamedov@mbstudio.uz',4,'2020-07-17 01:45:07','2020-08-06 04:46:12'),(6,'5f10bc9f84c51.png','Хуршид Абдукамилов','Главный инженер проекта',NULL,'xurshid.abdukamilov@mbstudio.uz',3,'2020-07-17 01:46:23','2020-08-06 04:46:21'),(7,'5f10bcc6830d2.png','Хикматилла Миртожиев','Главный архитектор проекта',NULL,'xikmatilla.mirtojiev@mbstudio.uz',2,'2020-07-17 01:47:02','2020-08-06 04:46:29'),(8,'5f10bcf38e508.png','Бахром Музаффаров','Основатель и глава компании',NULL,'bahrom.muzaffarov@mbstudio.uz',1,'2020-07-17 01:47:37','2020-10-14 11:51:45');
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` tinyint DEFAULT NULL COMMENT '1-woman, 2-man',
  `region_id` int DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` tinyint NOT NULL DEFAULT '0',
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'Admin',NULL,NULL,NULL,NULL,NULL,'your@email.com',NULL,'$2y$10$IgKAZ2iZFchh17tRuJ2ooOw9Nj4avSPBk1cqWr6DRTGb/hs05dqgW',5,NULL,'2020-04-25 09:29:12','2020-04-25 09:29:12');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-19  7:38:08
