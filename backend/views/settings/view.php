<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var common\models\SystemSettings $model
 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'System Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-settings-view">
    <div class="card">
        <div class="card-header">
            <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php echo Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
        <div class="card-body">
            <?php echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'store_name',
                    'phone_number',
                    'email_main:email',
                    'email_support:email',
                    'address',
                    'landmark',
                    'schedule',
                    'maps_address',
                    'maps_lat',
                    'maps_lon',
                    'fb_link',
                    'ints_link',
                    'tw_link',
                    'you_link',
                    
                ],
            ]) ?>
        </div>
    </div>
</div>
