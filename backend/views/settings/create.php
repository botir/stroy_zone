<?php

/**
 * @var yii\web\View $this
 * @var common\models\SystemSettings $model
 */

$this->title = 'Create System Settings';
$this->params['breadcrumbs'][] = ['label' => 'System Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-settings-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
