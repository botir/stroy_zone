<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'System Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="system-settings-index">
    <div class="card">
        <div class="card-header">
            <?php echo Html::a('Create System Settings', ['create'], ['class' => 'btn btn-success']) ?>
        </div>

        <div class="card-body p-0">
    
            <?php echo GridView::widget([
                'layout' => "{items}\n{pager}",
                'options' => [
                    'class' => ['gridview', 'table-responsive'],
                ],
                'tableOptions' => [
                    'class' => ['table', 'text-nowrap', 'table-striped', 'table-bordered', 'mb-0'],
                ],
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'store_name',
                    'phone_number',
                    'email_main:email',
                    'email_support:email',
                    // 'address',
                    // 'landmark',
                    // 'schedule',
                    // 'maps_address',
                    // 'maps_lat',
                    // 'maps_lon',
                    // 'fb_link',
                    // 'ints_link',
                    // 'tw_link',
                    // 'you_link',
                    
                    ['class' => \common\widgets\ActionColumn::class],
                ],
            ]); ?>
    
        </div>
        <div class="card-footer">
            <?php echo getDataProviderSummary($dataProvider) ?>
        </div>
    </div>

</div>
