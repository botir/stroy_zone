<?php

/**
 * @var yii\web\View $this
 * @var common\models\SystemSettings $model
 */

$this->title = 'настройки';
$this->params['breadcrumbs'][] = ['label' => 'настройки', 'url' => ['index']];
?>
<div class="system-settings-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
