<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\web\JsExpression;
use trntv\filekit\widget\Upload;
/**
 * @var yii\web\View $this
 * @var common\models\SystemSettings $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>

<div class="system-settings-form">
    <?php $form = ActiveForm::begin(); ?>
        <div class="card">
            <div class="card-body">
                <?php echo $form->errorSummary($model); ?>
                <div class="row">
                  <div class="col-sm-3">
                    <?php echo $form->field($model, 'store_name')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-sm-3">
                    <?php echo $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-sm-3">
                    <?php echo $form->field($model, 'email_main')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-sm-3">
                    <?php echo $form->field($model, 'email_support')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-3">
                    <?php echo $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-sm-3">
                    <?php echo $form->field($model, 'landmark')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-sm-3">
                    <?php echo $form->field($model, 'maps_lat')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-sm-3">
                    <?php echo $form->field($model, 'maps_lon')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <?php echo $form->field($model, 'schedule')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-sm-6">
                    <?php echo $form->field($model, 'maps_iframe')->textarea(['rows' => 6]) ?>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-3">
                      <?php echo $form->field($model, 'fb_link')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-sm-3">
                    <?php echo $form->field($model, 'ints_link')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-sm-3">
                    <?php echo $form->field($model, 'tw_link')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-sm-3">
                    <?php echo $form->field($model, 'you_link')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6">
                    <?php echo $form->field($model, 'thumbnail1')->widget(
                        Upload::class,
                        [
                            'url' => ['/file/storage/upload2'],
                            'maxFileSize' => 5000000, // 5 MiB,
                            'acceptFileTypes' => new JsExpression('/(\.|\/)(svg|jpe?g|png)$/i'),
                        ]
                    ) ?>
                  </div>
                  <div class="col-sm-6">
                    <?php echo $form->field($model, 'thumbnail2')->widget(
                        Upload::class,
                        [
                            'url' => ['/file/storage/upload2'],
                            'maxFileSize' => 5000000, // 5 MiB,
                            'acceptFileTypes' => new JsExpression('/(\.|\/)(svg|jpe?g|png)$/i'),
                        ]
                    ) ?>
                  </div>
                </div>












            </div>
            <div class="card-footer">
                <?php echo Html::submitButton('Cохранить', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
