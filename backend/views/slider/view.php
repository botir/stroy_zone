<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var common\models\ContentSlider $model
 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Слайдеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-slider-view">
    <div class="card">
        <div class="card-header">
            <?php echo Html::a('Изменить ', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

        </div>
        <div class="card-body">
            <?php echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'title_ru',
                    'title_uz',
                    'img_url:url',
                    'img_path',
                    'link_to',

                ],
            ]) ?>
        </div>
    </div>
</div>
