<?php

/**
 * @var yii\web\View $this
 * @var common\models\ContentSlider $model
 */

$this->title = 'Create Content Slider';
$this->params['breadcrumbs'][] = ['label' => 'Content Sliders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-slider-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
