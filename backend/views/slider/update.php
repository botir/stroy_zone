<?php

/**
 * @var yii\web\View $this
 * @var common\models\ContentSlider $model
 */

$this->title = 'Изменение : ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Слайдеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="content-slider-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
