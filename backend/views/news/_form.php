<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\web\JsExpression;
use trntv\filekit\widget\Upload;

/**
 * @var yii\web\View $this
 * @var common\models\ContentNews $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>

<div class="content-news-form">
    <?php $form = ActiveForm::begin(); ?>
        <div class="card">
            <div class="card-body">
                <?php echo $form->errorSummary($model); ?>

                <div class="row">
                  <div class="col-sm-6">
                    <?php echo $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-sm-6">
                    <?php echo $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>

                <div class="row">

                    <div class="col-sm-4">
                  <?php echo $form->field($model, 'is_active')->checkbox() ?>
                  </div>


                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <?php echo $form->field($model, 'image')->widget(
                        Upload::class,
                        [
                            'url' => ['/file/storage/upload'],
                            'maxFileSize' => 5000000, // 5 MiB,
                            'acceptFileTypes' => new JsExpression('/(\.|\/)(svg|jpe?g|png)$/i'),
                        ]
                    ) ?>
                  </div>

                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <?php echo $form->field($model, 'description_ru')->textarea(['rows' => 6]) ?>
                  </div>
                  <div class="col-sm-6">
                    <?php echo $form->field($model, 'content_ru')->widget(
                        \yii\imperavi\Widget::class,
                        [
                            'plugins' => ['fullscreen', 'fontcolor'],
                            'options' => [
                                'minHeight' => 400,
                                'maxHeight' => 400,
                                'buttonSource' => true,
                                'convertDivs' => false,
                                'removeEmptyTags' => true,
                                'imageUpload' => Yii::$app->urlManager->createUrl(['/file/storage/upload-imperavi']),
                            ],
                        ]
                    ) ?>
                  </div>
                </div>


            </div>
            <div class="card-footer">
                <?php echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
