<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var common\models\ContentNews $model
 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-news-view">
    <div class="card">
        <div class="card-header">
            <?php echo Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php echo Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
        <div class="card-body">
            <?php echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'slug',
                    'is_active:boolean',
                    'created_at',

                ],
            ]) ?>
        </div>
    </div>
</div>

<div class="content-news-view">
    <div class="card">
        <div class="card-header">
            <h2><?=$model->title_ru?></h2>
        </div>
        <div class="card-body">
            <b><?=$model->description_ru?></b>
            <img src="<?=$model->img_url.$model->img_path?>">
            <p><?=$model->content_ru?></p>
        </div>
    </div>
</div>
