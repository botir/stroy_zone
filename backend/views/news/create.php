<?php

/**
 * @var yii\web\View $this
 * @var common\models\ContentNews $model
 */

$this->title = 'создание новостей';
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-news-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
