<?php

/**
 * @var yii\web\View $this
 * @var common\models\ContentNews $model
 */

$this->title = 'Изменение новости: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="content-news-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
