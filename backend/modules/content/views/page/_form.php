<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\ContentPage $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>

<div class="content-page-form">
    <?php $form = ActiveForm::begin(); ?>
        <div class="card">
            <div class="card-body">
                <?php echo $form->errorSummary($model); ?>
                <div class="row">
                  <div class="col-sm-6">
                    <?php echo $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-sm-6">
                    <?php echo $form->field($model, 'slug')
                        ->hint(Yii::t('backend', 'If you leave this field empty, the slug will be generated automatically'))
                        ->textInput(['maxlength' => true]) ?>
                  </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                      <?php echo $form->field($model, 'content_ru')->textarea(['rows' => 6]) ?>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="col-sm-12">
                            <?php echo $form->field($model, 'is_active')->checkbox() ?>
                          </div>

                        </div>
                        <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                            <?php echo $form->field($model, 'is_menu')->checkbox() ?>
                            </div>
                          </div>
                          </div>
                      </div>
                    </div>
                </div>




            </div>
            <div class="card-footer">
                <?php echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
