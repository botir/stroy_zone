<?php

/**
 * @var yii\web\View $this
 * @var common\models\ContentPage $model
 */

$this->title = 'Создание новой страницы';
$this->params['breadcrumbs'][] = ['label' => 'Cтраницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-page-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
