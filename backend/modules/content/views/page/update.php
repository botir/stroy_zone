<?php

/**
 * @var yii\web\View $this
 * @var common\models\ContentPage $model
 */

$this->title = 'Редактирование: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cтраницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="content-page-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
