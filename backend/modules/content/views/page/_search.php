<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\ContentPage $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>

<div class="content-page-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php echo $form->field($model, 'id') ?>
    <?php echo $form->field($model, 'slug') ?>
    <?php echo $form->field($model, 'title_uz') ?>
    <?php echo $form->field($model, 'title_ru') ?>
    <?php echo $form->field($model, 'content_uz') ?>
    <?php // echo $form->field($model, 'content_ru') ?>
    <?php // echo $form->field($model, 'is_active')->checkbox() ?>
    <?php // echo $form->field($model, 'is_menu')->checkbox() ?>
    <?php // echo $form->field($model, 'created_at') ?>
    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton('Reset', ['class' => 'btn btn-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
