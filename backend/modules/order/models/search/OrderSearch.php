<?php

namespace backend\modules\order\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrderOrder;

/**
 * OrderSearch represents the model behind the search form about `common\models\OrderOrder`.
 */
class OrderSearch extends OrderOrder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_code', 'region_id', 'district_id', 'status'], 'integer'],
            [['user_full_name', 'user_phone', 'user_email', 'comments', 'shipping_address', 'created_at', 'updated_at'], 'safe'],
            [['total_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderOrder::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at' => SORT_ASC]],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'order_code' => $this->order_code,
            'total_amount' => $this->total_amount,
            'region_id' => $this->region_id,
            'district_id' => $this->district_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'user_full_name', $this->user_full_name])
            ->andFilterWhere(['ilike', 'user_phone', $this->user_phone])
            ->andFilterWhere(['ilike', 'user_email', $this->user_email])
            ->andFilterWhere(['ilike', 'comments', $this->comments])
            ->andFilterWhere(['ilike', 'shipping_address', $this->shipping_address]);

        return $dataProvider;
    }
}
