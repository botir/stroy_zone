<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var common\models\OrderOrder $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>

<div class="order-order-form">

  <div class="card">

      <div class="card-body">
          <?php echo DetailView::widget([
              'model' => $model,
              'attributes' => [
                  'order_code',
                  'total_amount',
                  [
                    'label' => 'Область',
                    'attribute' => 'region.name_ru',
                  ],
                  [
                    'label' => 'Регион',
                    'attribute' => 'district.name_ru',
                  ],
                  'created_at',
                  'updated_at',

              ],
          ]) ?>
      </div>
  </div>

    <?php $form = ActiveForm::begin(); ?>
        <div class="card">
            <div class="card-body">
                <?php echo $form->errorSummary($model); ?>

                <div class="row">
                  <div class="col-sm-4">
                    <?php echo $form->field($model, 'user_full_name')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-sm-4">
                    <?php echo $form->field($model, 'user_phone')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-sm-4">
                    <?php echo $form->field($model, 'user_email')->textInput(['maxlength' => true]) ?>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-4">
                    <?php echo $form->field($model, 'comments')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-sm-4">
                    <?php echo $form->field($model, 'shipping_address')->textInput(['maxlength' => true]) ?>
                  </div>
                  <div class="col-sm-4">
                    <?php echo $form->field($model, 'status')->dropDownList($model->getStatusList(), ['prompt'=>'']) ?>
                  </div>
                </div>

            </div>
            <div class="card-footer">
                <?php echo Html::submitButton($model->isNewRecord ? 'Cохранить' : 'Cохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
