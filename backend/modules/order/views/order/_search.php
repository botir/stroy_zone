<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\OrderOrder $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>

<div class="order-order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php echo $form->field($model, 'id') ?>
    <?php echo $form->field($model, 'user_full_name') ?>
    <?php echo $form->field($model, 'user_phone') ?>
    <?php echo $form->field($model, 'user_email') ?>
    <?php echo $form->field($model, 'order_code') ?>
    <?php // echo $form->field($model, 'comments') ?>
    <?php // echo $form->field($model, 'total_amount') ?>
    <?php // echo $form->field($model, 'region_id') ?>
    <?php // echo $form->field($model, 'district_id') ?>
    <?php // echo $form->field($model, 'shipping_address') ?>
    <?php // echo $form->field($model, 'status') ?>
    <?php // echo $form->field($model, 'created_at') ?>
    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton('Reset', ['class' => 'btn btn-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
