<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\OrderOrder;

/**
 * @var yii\web\View $this
 * @var backend\modules\order\models\search\OrderSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-order-index">
    <div class="card">
        <div class="card-body p-0">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?php echo GridView::widget([
                'layout' => "{items}\n{pager}",
                'options' => [
                    'class' => ['gridview', 'table-responsive'],
                ],
                'tableOptions' => [
                    'class' => ['table', 'text-nowrap', 'table-striped', 'table-bordered', 'mb-0'],
                ],
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'order_code',
                    [
                      'attribute'=>'status',
                        'value'=>function ($model) {
                            return $model->getStatus();
                        },
                        'filter'=>OrderOrder::getStatusList()

                    ],
                    'user_full_name',
                    'user_phone',
                    'total_amount',
                    'created_at',
                    // 'comments',
                    // 'total_amount',
                    // 'region_id',
                    // 'district_id',
                    // 'shipping_address',
                    // 'status',
                    // 'created_at',
                    // 'updated_at',

                    [
                    'class' => \common\widgets\ActionColumn::class,
                    'template'=>'{update}'
                    ]
                ],
            ]); ?>

        </div>
        <div class="card-footer">
            <?php echo getDataProviderSummary($dataProvider) ?>
        </div>
    </div>

</div>
