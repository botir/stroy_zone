<?php

/**
 * @var yii\web\View $this
 * @var common\models\OrderOrder $model
 */

$this->title = 'Create Order Order';
$this->params['breadcrumbs'][] = ['label' => 'Order Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-order-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
