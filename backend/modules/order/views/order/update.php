<?php

/**
 * @var yii\web\View $this
 * @var common\models\OrderOrder $model
 */
 use yii\helpers\Url;
$this->title = 'Заказ #'.$model->order_code;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = '#'.$model->order_code;
?>
<div class="order-order-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
<?php if ($lines && count($lines) > 0):?>
<div class="uk-card-body">
  <div class="uk-grid-small uk-child-width-1-1 uk-child-width-1-2@m uk-flex-middle" uk-grid>




          <div class="card">
        <div class="card-header">
            Товары
        </div>
        <div class="card-body p-0">
            <div id="w0" class="gridview table-responsive">
              <table class="table text-nowrap table-striped table-bordered mb-0">
                <thead>
                  <tr>
                    <th>Название</th>
                    <th>Цена</th>
                    <th>Количество</th>
                    <th>Сумма</th>
                  </tr>
                </thead>
<tbody>
  <?php foreach ($lines as $data): ?>
    <tr>
      <td>
        <a class="tm-media-box" href="<?=Url::to(['/catalog/product/view', 'id'=>$data['id']])?>" target="_blank">
          <figure class="tm-media-box-wrap"><img src="<?=$this->getListImage($data)?>" />
          </figure>


        <?=$data['title']?>
      </a>
      </td>
      <th><?=$this->numFormat($data['price'])?></th>
      <td><?=$data['quantity']?></td>
      <td><?=$this->numFormat($data['total_amount'])?></td>
    </tr>

<?php endforeach; ?>

</tbody></table>
</div>
        </div>
    </div>







  </div>
</div>
<?php endif;?>
