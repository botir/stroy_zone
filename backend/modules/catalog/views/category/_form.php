<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use trntv\filekit\widget\Upload;

/**
 * @var yii\web\View $this
 * @var common\models\CatalogCategory $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>

<div class="catalog-category-form">
    <?php $form = ActiveForm::begin(); ?>
        <div class="card">
            <div class="card-body">
                <?php echo $form->errorSummary($model); ?>
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'description_ru')->textArea() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                      <?php
                    echo $form->field($model, 'parent_id')->widget(Select2::classname(), [
                        'data' =>$model->getListData(),
                        'language' => 'ru',
                        'options' => ['placeholder' => 'Родительских категорий', 'multiple' => false],
                        'pluginOptions' => ['allowClear' => true],
                    ]);
                  ?>
                    </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <?php echo $form->field($model, 'icon')->widget(
                        Upload::class,
                        [
                            'url' => ['/file/storage/upload2'],
                            'maxFileSize' => 5000000, // 5 MiB,
                            'acceptFileTypes' => new JsExpression('/(\.|\/)(svg|jpe?g|png)$/i'),
                        ]
                    ) ?>
                  </div>

                </div>
                <div class="row">
                  <div class="col-sm-6">
                      <div class="form-group">
                      <?php echo $form->field($model, 'is_main')->checkbox() ?>
                      </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <?php echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
