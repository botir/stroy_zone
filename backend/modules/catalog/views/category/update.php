<?php

/**
 * @var yii\web\View $this
 * @var common\models\CatalogCategory $model
 */

$this->title = 'Изменение категории: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="catalog-category-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
