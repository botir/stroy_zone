<?php

/**
 * @var yii\web\View $this
 * @var common\models\CatalogCategory $model
 */

$this->title = 'Создание новых категорий';
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-category-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
