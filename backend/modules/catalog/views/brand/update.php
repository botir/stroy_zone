<?php

/**
 * @var yii\web\View $this
 * @var common\models\CatalogBrand $model
 */

$this->title = 'Изменение бренда : ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Бренди', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="catalog-brand-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
