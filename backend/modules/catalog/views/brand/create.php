<?php

/**
 * @var yii\web\View $this
 * @var common\models\CatalogBrand $model
 */

$this->title = 'Добавление нового бренда';
$this->params['breadcrumbs'][] = ['label' => 'Бренди', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-brand-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
