<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\web\JsExpression;
use trntv\filekit\widget\Upload;

/**
 * @var yii\web\View $this
 * @var common\models\CatalogBrand $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>

<div class="catalog-brand-form">
    <?php $form = ActiveForm::begin(); ?>
        <div class="card">
            <div class="card-body">
                <?php echo $form->errorSummary($model); ?>
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                      <?php echo $form->field($model, 'thumbnail')->widget(
                          Upload::class,
                          [
                              'url' => ['/file/storage/upload2'],
                              'maxFileSize' => 5000000, // 5 MiB,
                              'acceptFileTypes' => new JsExpression('/(\.|\/)(svg|jpe?g|png)$/i'),
                          ]
                      ) ?>
                    </div>
                </div>



            </div>
            <div class="card-footer">
                <?php echo Html::submitButton($model->isNewRecord ? 'Добавить' : 'Cохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
