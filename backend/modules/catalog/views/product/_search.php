<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\CatalogProduct $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>

<div class="catalog-product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php echo $form->field($model, 'id') ?>
    <?php echo $form->field($model, 'content_txt') ?>
    <?php echo $form->field($model, 'slug') ?>
    <?php echo $form->field($model, 'category_id') ?>
    <?php echo $form->field($model, 'brand_id') ?>
    <?php // echo $form->field($model, 'model_id') ?>
    <?php // echo $form->field($model, 'status') ?>
    <?php // echo $form->field($model, 'price') ?>
    <?php // echo $form->field($model, 'search_vector') ?>
    <?php // echo $form->field($model, 'created_at') ?>
    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton('Reset', ['class' => 'btn btn-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
