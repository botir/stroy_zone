<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var common\models\CatalogProduct $model
 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Каталог продуктов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="card card-solid">
  <div class="card-header">
      <?php echo Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?php echo Html::a('Удалить', ['delete', 'id' => $model->id], [
          'class' => 'btn btn-danger',
          'data' => [
              'confirm' => 'Are you sure you want to delete this item?',
              'method' => 'post',
          ],
      ]) ?>
  </div>
  <div class="card-body">
            <div class="row">
              <div class="col-12 col-sm-6">
                <h3 class="d-inline-block d-sm-none"><?=$model->title_ru?></h3>
                <?php if ($model->attachments):?>
                  <?php foreach ($model->attachments as $data):?>
                    <?php if ($data === reset( $model->attachments)):?>
                      <div class="col-12">
                        <img src="<?=$data['base_url'].$data['path']?>" class="product-image" alt="Product Image">
                      </div><div class="col-12 product-image-thumbs">
                    <?php else:?>
                      <div class="product-image-thumb active"><img src="<?=$data['base_url'].$data['path']?>" alt="Product Image"></div>
                    <?php endif;?>
                  <?php endforeach;?>


              <?php endif; ?>
              </div>
              <div class="col-12 col-sm-6">
                <h3 class="my-3"><?=$model->title_ru?></h3>
                <p><?=$model->description_ru?></p>

                <hr>
                <?php echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'slug',
                    [
                      'label' => 'Категория',
                      'attribute' => 'category.title_ru',
                    ],
                    [
                      'label' => 'Бренд',
                      'attribute' => 'brand.title',
                    ],
                    [
                      'label' => 'Мдел',
                      'attribute' => 'model.title',
                    ],
                    'status',
                    'price',
                    'created_at',
                    'updated_at',

                ],
            ]) ?>





              </div>
            </div>

          </div>
</div>
