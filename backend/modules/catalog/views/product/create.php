<?php

/**
 * @var yii\web\View $this
 * @var common\models\CatalogProduct $model
 */

$this->title = 'Добавление нового продукта';
$this->params['breadcrumbs'][] = ['label' => 'Каталог продуктов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-product-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
