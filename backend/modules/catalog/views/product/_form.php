<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use trntv\filekit\widget\Upload;

/**
 * @var yii\web\View $this
 * @var common\models\CatalogProduct $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>

<div class="catalog-product-form">
    <?php $form = ActiveForm::begin(); ?>
        <div class="card">
            <div class="card-body">
                <?php echo $form->errorSummary($model); ?>
                <div class="row">
                  <div class="col-sm-4">
                    <?php
                        echo $form->field($model, 'category_id')->widget(Select2::classname(), [
                            'data' =>\common\models\CatalogCategory::getTreeCategories(),
                            'language' => 'ru',
                            'options' => ['placeholder' => 'категорий', 'multiple' => false],
                            'pluginOptions' => ['allowClear' => true],
                        ]);
                      ?>
                  </div>
                    <div class="col-sm-4">
                      <?php
                          echo $form->field($model, 'brand_id')->widget(Select2::classname(), [
                              'data' =>\common\models\CatalogBrand::getListData(),
                              'language' => 'ru',
                              'options' => ['placeholder' => 'Выберите бренд', 'multiple' => false],
                              'pluginOptions' => ['allowClear' => true],
                          ]);
                          ?>
                    </div>
                    <div class="col-sm-4">
                      <?php
                          echo $form->field($model, 'model_id')->widget(Select2::classname(), [
                              'data' =>\common\models\CatalogModel::getListData(),
                              'language' => 'ru',
                              'options' => ['placeholder' => 'Выберите модел', 'multiple' => false],
                              'pluginOptions' => ['allowClear' => true],
                          ]);
                          ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                      <?php echo $form->field($model, 'slug')
                          ->hint(Yii::t('backend', 'If you leave this field empty, the slug will be generated automatically'))
                          ->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                      <?php echo $form->field($model, 'description_ru')->widget(
                          \yii\imperavi\Widget::class,
                          [
                              'plugins' => ['fullscreen', 'fontcolor'],
                              'options' => [
                                  'minHeight' => 400,
                                  'maxHeight' => 400,
                                  'buttonSource' => true,
                                  'convertDivs' => false,
                                  'removeEmptyTags' => true,
                                  'imageUpload' => Yii::$app->urlManager->createUrl(['/file/storage/upload-imperavi']),
                              ],
                          ]
                      ) ?>
                    </div>
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="row">
                          <div class="col-sm-12">
                            <?php echo $form->field($model, 'price')->textInput() ?>
                          </div>

                        </div>
                        <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                            <?php echo $form->field($model, 'status')->checkbox() ?>
                            </div>
                          </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-12">
                              <div class="form-group">
                              <?php echo $form->field($model, 'top_selling')->checkbox() ?>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-12">
                              <div class="form-group">
                              <?php echo $form->field($model, 'new_product')->checkbox() ?>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-12">
                              <div class="form-group">
                              <?php echo $form->field($model, 'is_tranding')->checkbox() ?>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                </div>




                <div class="row">
                  <div class="col-sm-12">
                    <?php echo $form->field($model, 'attachments')->widget(
                        Upload::class,
                        [
                            'url' => ['/file/storage/upload'],
                            'sortable' => true,
                            'maxFileSize' => 10000000, // 10 MiB
                            'maxNumberOfFiles' => 10,
                        ]
                    ) ?>
                    </div>
                </div>


            </div>
            <div class="card-footer">
                <?php echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
