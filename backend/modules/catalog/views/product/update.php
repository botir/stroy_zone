<?php

/**
 * @var yii\web\View $this
 * @var common\models\CatalogProduct $model
 */

$this->title = 'Редактировать: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Каталог продуктов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="catalog-product-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
