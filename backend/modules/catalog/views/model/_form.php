<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\select2\Select2;

/**
 * @var yii\web\View $this
 * @var common\models\CatalogModel $model
 * @var yii\bootstrap4\ActiveForm $form
 */
?>

<div class="catalog-model-form">
    <?php $form = ActiveForm::begin(); ?>
        <div class="card">
            <div class="card-body">
                <?php echo $form->errorSummary($model); ?>
                <div class="row">
                    <div class="col-sm-6">
                        <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                      <?php
                          echo $form->field($model, 'brand_id')->widget(Select2::classname(), [
                              'data' =>\common\models\CatalogBrand::getListData(),
                              'language' => 'ru',
                              'options' => ['placeholder' => 'Выберите бренд', 'multiple' => false],
                              'pluginOptions' => ['allowClear' => true],
                          ]);
                          ?>
                    </div>
                </div>


            </div>
            <div class="card-footer">
                <?php echo Html::submitButton($model->isNewRecord ? 'Добавить' : 'Cохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
