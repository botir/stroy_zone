<?php

/**
 * @var yii\web\View $this
 * @var common\models\CatalogModel $model
 */

$this->title = 'Редактирование: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Catalog Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="catalog-model-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
