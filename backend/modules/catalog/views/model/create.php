<?php

/**
 * @var yii\web\View $this
 * @var common\models\CatalogModel $model
 */

$this->title = 'Добавление нового модель бренда';
$this->params['breadcrumbs'][] = ['label' => 'Модели', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-model-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
