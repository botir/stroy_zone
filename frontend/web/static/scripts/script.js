// Google Maps
var cart_content = document.getElementById('cart-offcanvas');
Number.prototype.format = function(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

function initMap() {
  var elements = document.querySelectorAll('.js-map');
  Array.prototype.forEach.call(elements, function(el) {
    var lat = +el.dataset.latitude,
        lng = +el.dataset.longitude,
        zoom = +el.dataset.zoom;
    if ((lat !== '') && (lng !== '') && (zoom > 0)) {
      var map = new google.maps.Map(el, {
        zoom: zoom,
        center: { lat: lat, lng: lng },
        disableDefaultUI: true
      });
      var marker = new google.maps.Marker({
        map: map,
        animation: google.maps.Animation.DROP,
        position: { lat: lat, lng: lng }
      });
    }
  });
}

// Change view

(function() {
  var container = document.getElementById('products');

  if(container) {
    var grid = container.querySelector('.js-products-grid'),
        viewClass = 'tm-products-',
        optionSwitch = Array.prototype.slice.call(container.querySelectorAll('.js-change-view a'));

    function init() {
      optionSwitch.forEach(function(el, i) {
        el.addEventListener('click', function(ev) {
          ev.preventDefault();
          _switch(this);
        }, false );
      });
    }

    function _switch(opt) {
      optionSwitch.forEach(function(el) {
        grid.classList.remove(viewClass + el.getAttribute('data-view'));
      });
      grid.classList.add(viewClass + opt.getAttribute('data-view'));
    }

    init();
  }
})();

// Increment

function increment(incrementor, target, price, vl) {
  var value = parseInt(document.getElementById(target).value, 10);
  value = isNaN(value) ? 0 : value;
  if(incrementor < 0) {
    if(value > 1) {
      value+=incrementor;
    }
  } else {
    value+=incrementor;
  }

  var csrfToken = document.querySelector('meta[name="csrf-token"]').content;

  var xhr4 = new XMLHttpRequest();
  xhr4.open('POST', '/cart/add', true);
  xhr4.withCredentials = true;
  //xhr.setRequestHeader('Content-Type', 'application/json')
  xhr4.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');;
  xhr4.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xhr4.onload = function () {
      var result = JSON.parse(xhr4.responseText);
      if (xhr4.readyState == 4 && xhr4.status == '200') {



        //UIkit.offcanvas('#cart-offcanvas').show();
      }
  };
  var params = 'product='+target.split("-")[1]+'&_csrf='+csrfToken;
  //xhr.send(JSON.stringify({product: 22, _csrf : csrfToken}));
  xhr4.send(params);


  document.getElementById(target).value = value;
  document.getElementById('total-'+target).innerHTML = (value*price).format(2, 3, ' ', ' ') + ' ' + vl;
  // document.getElementById('total-price').innerHTML = value*price + ' ' + vl;
  var qnt_buttons = document.querySelectorAll('.tm-quantity-input');

  total = 0;
  Array.prototype.forEach.call(qnt_buttons, function(el) {
    total = total + el.value*el.getAttribute('data-price');
  });
  document.getElementById('total-price').innerHTML = total.format(2, 3, ' ', ' ') + ' ' + vl;
}

// Scroll to description

(function() {
  UIkit.scroll('.js-scroll-to-description', {
    duration: 300,
    offset: 58
  });
})();

// Update sticky tabs

(function() {
  UIkit.util.on('.js-product-switcher', 'show', function() {
    UIkit.update();
  });
})();


function listCart(sh_nav)
{
  var xhr2 = new XMLHttpRequest();
  try {
      xhr2.open('GET', '/cart/get', true);
      xhr2.withCredentials = true;
      xhr2.setRequestHeader('Content-Type', 'application/json');
      xhr2.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xhr2.onload = function () {
          var result = JSON.parse(xhr2.responseText);
          if (xhr2.readyState == 4 && xhr2.status == '200') {
            if (!result.products || result.products.length < 1) {
                cart_content.querySelector('footer').style.display = 'none';
                document.getElementById('cart-tot').innerHTML= '';
                cart_content.querySelector('ul').innerHTML= '';
                document.getElementById('cart_badge').innerHTML= '';
                if (sh_nav){
                  UIkit.offcanvas('#cart-offcanvas').show();
                }
            }else{
              var cart_data = result['products'];
              var contents = '';
              document.getElementById('cart_badge').innerHTML= result['products'].length;

              for(var k in cart_data) {
                contents +='<li class="uk-visible-toggle">'+
                        '<arttcle>'+
                          '<div class="uk-grid-small" uk-grid>'+
                            '<div class="uk-width-1-4">'+
                              '<div class="tm-ratio tm-ratio-4-3"><a class="tm-media-box" href="'+cart_data[k].url+'">'+
                                  '<figure class="tm-media-box-wrap"><img src="'+cart_data[k].image+'">'+
                                  '</figure></a></div>'+
                            '</div>'+
                            '<div class="uk-width-expand">'+
                              '<div class="uk-text-meta uk-text-xsmall">'+cart_data[k].category_title+'</div><a class="uk-link-heading uk-text-small" href="'+cart_data[k].url+'">'+cart_data[k].title+'</a>'+
                              '<div class="uk-margin-xsmall uk-grid-small uk-flex-middle" uk-grid>'+
                                '<div class="uk-text-bolder uk-text-small">'+cart_data[k].price+'</div>'+
                                '<div class="uk-text-meta uk-text-xsmall">'+cart_data[k].quantity+' × '+cart_data[k].total_price+'</div>'+
                              '</div>'+
                            '</div>'+
                            '<div><a class="uk-icon-link uk-text-danger uk-invisible-hover" href="#" uk-icon="icon: close; ratio: .75" uk-tooltip="Remove" onclick="removeCart('+cart_data[k].id+', 1);"></a></div>'+
                          '</div>'+
                        '</arttcle>'+
                      '</li>';
              }
              cart_content.querySelector('ul').innerHTML= contents;
              cart_content.querySelector('footer').style.display = 'block';
              document.getElementById('cart-tot').innerHTML= result.total;
              // cart_content.querySelector('footer').innerHTML='<div class="uk-grid-small" uk-grid>'+
              //                     '<div class="uk-width-expand uk-text-muted uk-h4">Subtotal</div>'+
              //                     '<div class="uk-h4 uk-text-bolder">'+result.total+'</div>'+
              //                   '</div><div class="uk-grid-small uk-child-width-1-1 uk-child-width-1-2@m uk-margin-small" uk-grid>'+
              //                   '<div><a class="uk-button uk-button-primary uk-margin-small uk-width-1-1" href="/order/checkout">checkout</a></div>'+
              //                 '</div>';
                if (sh_nav){
                  UIkit.offcanvas('#cart-offcanvas').show();
                }
            }
          }
      }
  } catch (exception) {
      console.log(exception.message);
  }
  xhr2.send(null);
}


function removeCart(id, tp)
{

  var csrfToken = document.querySelector('meta[name="csrf-token"]').content;

  var xhr = new XMLHttpRequest();
  xhr.open('POST', '/cart/remove', true);
  xhr.withCredentials = true;
  //xhr.setRequestHeader('Content-Type', 'application/json')
  xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');;
  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xhr.onload = function () {
      var result = JSON.parse(xhr.responseText);
      if (xhr.readyState == 4 && xhr.status == '200') {
        if (tp == 2)
          location.reload();
        else{
          listCart();
        }
        //UIkit.offcanvas('#cart-offcanvas').show();
      }
  };
  var params = 'product='+id+'&_csrf='+csrfToken;
  //xhr.send(JSON.stringify({product: 22, _csrf : csrfToken}));
  xhr.send(params);

  //listCart();
  return false;
}

// Add to cart

(function() {


  var addToCartButtons = document.querySelectorAll('.js-add-to-cart');

  Array.prototype.forEach.call(addToCartButtons, function(el) {
    el.onclick = function() {

      var csrfToken = document.querySelector('meta[name="csrf-token"]').content;

      var xhr = new XMLHttpRequest();
      xhr.open('POST', '/cart/add', true);
      xhr.withCredentials = true;
      //xhr.setRequestHeader('Content-Type', 'application/json')
      xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');;
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xhr.onload = function () {
          var result = JSON.parse(xhr.responseText);
          if (xhr.readyState == 4 && xhr.status == '200') {


                listCart(true);
            //UIkit.offcanvas('#cart-offcanvas').show();
          }
      };
      var params = 'product='+el.getAttribute('data-id')+'&_csrf='+csrfToken;
      //xhr.send(JSON.stringify({product: 22, _csrf : csrfToken}));
      xhr.send(params);
    };
  });
})();

// Action buttons

(function() {
  var addToButtons = document.querySelectorAll('.js-add-to');

  Array.prototype.forEach.call(addToButtons, function(el) {
    var link;
    var message = '<span class="uk-margin-small-right" uk-icon=\'check\'></span>Added to '  ;
    var links = {
      favorites: '<a href="/favorites">favorites</a>',
      compare: '<a href="/compare">compare</a>',
    };
    if(el.classList.contains('js-add-to-favorites')) {
      link = links.favorites;
    };
    if(el.classList.contains('js-add-to-compare')) {
      link = links.compare;
    }
    el.onclick = function() {
      if(!this.classList.contains('js-added-to')) {
        UIkit.notification({
          message: message + link,
          pos: 'bottom-right'
        });
      }
      this.classList.toggle('tm-action-button-active');
      this.classList.toggle('js-added-to');
    };
  });
})();

if (document.getElementById('orderform-region_id')){
  var region_select = document.getElementById('orderform-region_id');
  var dis_select = document.getElementById('orderform-district_id');
  region_select.addEventListener("change", function(){
  dis_select.innerHTML = "";
    var xhr3 = new XMLHttpRequest();
    xhr3.open('GET', '/region/get/'+this.value, true);
    xhr3.withCredentials = true;
    xhr3.setRequestHeader('Content-Type', 'application/json');
    xhr3.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr3.onload = function () {
        var result = JSON.parse(xhr3.responseText);
        if (xhr3.readyState == 4 && xhr3.status == '200') {
            for(var i in result) {
              var opt = document.createElement('option');
                opt.value = i;
                opt.innerHTML = result[i];
                dis_select.appendChild(opt);
          }
        }
    }
    xhr3.send(null);

  });

}


listCart();
