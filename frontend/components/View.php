<?php
namespace frontend\components;

use Yii;
use yii\helpers\Url;
use yii\helpers\Json;

class View extends \yii\web\View
{
    private $_settings = false;
    private $_categories = false;
    private $_brands = false;
    private $_menupage = false;

    public function init()
    {
        parent::init();
        $this->settings = null;
        $this->categories = null;
        $this->brands = null;
        $this->menupage = null;
    }

    public function getSettings()
    {
        return $this->_settings;
    }

    public function setSettings($value)
    {
        $this->_settings = $value;
    }

    public function getCategories()
    {
        return $this->_categories;
    }

    public function setCategories($value)
    {
        $this->_categories = $value;
    }

    public function getBrands()
    {
        return $this->_brands;
    }

    public function setBrands($value)
    {
        $this->_brands = $value;
    }

    public function getMenupage()
    {
        return $this->_menupage;
    }

    public function setMenupage($value)
    {
        $this->_menupage = $value;
    }
    public function getListImage($data){
        return $data['base_url'].'/thumbnails/_medium/'.$data['image_name'].'_medium.'.$data['image_ext'];
    }

    public static function numFormat($price)
    {
        $format = number_format($price, 0, ' ', ' ');
        return $format. ' сум';
    }

    public function getPublishDate($date,$type=1)
    {
      $time = strtotime($date);
      $now = date('Y-m-d');
      if ($now==date('Y-m-d',$time)){
          return date("H:i",$time);
      }else{
        return date("H:i / d.m.Y",$time);
      }      
    }



}
