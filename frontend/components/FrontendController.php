<?php

namespace frontend\components;

use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\helpers\Url;
use yii\db\Query;

class FrontendController extends Controller
{
	public $settings;
	public $categories;
	public $brands;
	public $menupage;


	public function beforeAction($action) {

		$query = (new Query())
		->select('*, coalesce(logo1_url, \'\')||coalesce(logo1_path, \'\') AS "logo1", coalesce(logo2_url, \'\')||coalesce(logo2_path, \'\') AS "logo2"')
		->from('system_settings')
		->where('id=1')
		->limit(1);

		$this->settings = $query->one();
		$this->categories = Yii::$app->db->createCommand('WITH categories AS(
SELECT id, parent_id, slug, content_txt->>\'title_ru\' as title, content_txt->>\'description_ru\' as description, coalesce(icon_url, \'\')||coalesce(icon_path, \'\') AS "icon"
FROM "catalog_category"
limit 500
)
select *
from categories
LEFT JOIN ( SELECT parent_id, array_to_json(array_agg(row_to_json(categories.*))) AS cats FROM categories
  GROUP by parent_id) childs ON categories.id = childs.parent_id
where categories.parent_id is null
order by id
limit 40')->queryAll();
$this->brands = Yii::$app->db->createCommand('select title, slug,  coalesce(logo_url, \'\')||coalesce(logo_path, \'\') AS "logo"
from catalog_brand
order by id
limit 20')->queryAll();
$this->menupage = Yii::$app->db->createCommand('select title_ru as title, slug,  content_ru as content_text
from content_page where is_menu is true
order by id
limit 7')->queryAll();
		return parent::beforeAction($action);

	}

	public function substr_unicode($str, $s, $l = null) {
        return join("", array_slice(
            preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY), $s, $l));
    }

}
