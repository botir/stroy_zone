<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\OrderOrder;
use common\models\OrderLine;

/**
 * OrderForm is the model behind the contact form.
 */
class OrderForm extends Model
{
    public $full_name;
    public $phone_number;
    public $email;
    public $region_id;
    public $district_id;
    public $comments;
    public $shipping_address;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['full_name', 'phone_number', 'region_id', 'district_id'], 'required'],
            // We need to sanitize them
            [['shipping_address'], 'string', 'max' => 254],
            [['comments'], 'string', 'max' => 1024],
            // email has to be a valid email address
            ['email', 'email']

        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'full_name' => 'ФИО',
            'phone_number' => 'Номер телефона',
            'email' => 'E-mail',
            'region_id' => 'Область',
            'district_id' => 'Регион',
            'shipping_address' => 'Адрес',
            'comments' => 'Комментарии'
        ];
    }

    public function save($products)
    {
        $phone_number = $this->phone_number;
        if ($this->validate()){
            $phone_number = str_replace("(","",$phone_number);
            $phone_number = str_replace(")","",$phone_number);
            $phone_number = str_replace("-","",$phone_number);
            $phone_number = str_replace(" ","",$phone_number);


            $result = false;
            $price = 0;
            if ($products){
                foreach ($products as $product) {
                  $price += $product['total_price'];
                }

                $order = new OrderOrder();
                $code = $this->generateCode();
                $order->user_phone = $phone_number;
                $order->user_full_name = $this->full_name;
                $order->user_email = $this->email;
                $order->user_email = $this->email;
                $order->order_code = $code;
                $order->region_id = $this->region_id;
                $order->district_id = $this->district_id;
                $order->comments = $this->comments;
                $order->status = 1;
                $order->total_amount = $price;

                $connection = \Yii::$app->db;
                $transaction = $connection->beginTransaction();
                if ($order->save()){

                  foreach ($products as $product) {
                    $line = new OrderLine();
                    $line->order_id = $order->id;
                    $line->product_name = $product['title'];
                    $line->product_id = $product['id'];
                    $line->quantity = $product['quantity'];
                    $line->price = $product['price'];
                    $line->total_amount = $product['total_price'];
                    if ($line->save()){
                      $result = true;
                    }else{
                      print_r($line->getErrors()); exit;
                      $result = false;
                      $transaction->rollback();
                    }
                  }

                }else{
                  $transaction->rollback();
                }
                if ($result){
                  $transaction->commit();
                  return $order;
                }else {
                  return false;
                }
            }
            return false;
    }
    return false;
    }


    public function generateCode()
    {
        $number=100000;
        while(true){
           $number = rand(1000000,9999999);
           $result = OrderOrder::find()
            ->select(['order_code'])
            ->where(['order_code' => $number])
            ->asArray()
            ->scalar();
            if (!$result){
                break;
            }
        }
        return $number;
    }




}
