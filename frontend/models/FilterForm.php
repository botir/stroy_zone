<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * FilterForm is the model behind the contact form.
 */
class FilterForm extends Model
{
    public $pfrom;
    public $pto;
    public $brands;
    public $body;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['pfrom', 'pto', 'brands'], 'integer']
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'pfrom' => 'From',
            'pto' => 'TO',
            'brands' => 'Brands'

        ];
    }

    
}
