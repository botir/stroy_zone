<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use common\assets\Html5shiv;
use rmrevin\yii\fontawesome\NpmFreeAssetBundle;
use yii\bootstrap4\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Frontend application asset
 */
class IconAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@frontend/web/static';

    /**
     * @var array
     */
    public $js = [
        'scripts/uikit.js',
        'scripts/uikit-icons.js',
    ];

    public $jsOptions = ['version' => '1.1', 'position' => \yii\web\View::POS_HEAD];
}
