<?php

namespace frontend\controllers;

use common\models\ContentNews;

use yii\web\NotFoundHttpException;
use frontend\models\FilterForm;
use frontend\components\FrontendController;
use Yii;

/**
 * News controller
 */
class NewsController extends FrontendController
{

    /**
     * @return string
     */
    public function actionIndex()
    {
        $news = ContentNews::getListNews();
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('frontend','default_description_title')], 'description');
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('frontend','default_keywords_title')], 'keywords');
        \Yii::$app->view->registerMetaTag(['property'=>'og:title', 'content' => Yii::t('frontend','default_page_news_title')], 'og:title');
        \Yii::$app->view->registerMetaTag(['property'=>'og:description', 'content' => Yii::t('frontend','default_og_description')], 'og:description');
        \Yii::$app->view->registerMetaTag(['property'=>'og:type', 'content' => 'website'], 'og:type');
        \Yii::$app->view->registerMetaTag(['property'=>'og:url', 'content' => 'https://'.Yii::$app->request->serverName.Yii::$app->request->url], 'og:url');
        \Yii::$app->view->registerMetaTag(['property'=>'og:site_name', 'content' => Yii::$app->name], 'og:site_name');
        Yii::$app->view->title=Yii::t('frontend','default_page_news_title');
        return $this->render('index', [
          'settings'=>$this->settings,
          'categories'=>$this->categories,
          'brands'=>$this->brands,
          'menupage'=>$this->menupage,
          'news'=>$news
        ]);
    }

    public function actionView($slug)
    {

        $slug_s = strip_tags(stripslashes ( $slug ));

        $news = ContentNews::getOneNews($slug_s);
        if (!$news){
          throw new NotFoundHttpException;
        }


        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $news['description']], 'description');
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('frontend','default_keywords_title')], 'keywords');
        \Yii::$app->view->registerMetaTag(['property'=>'og:title', 'content' => $news['title']], 'og:title');
        \Yii::$app->view->registerMetaTag(['property'=>'og:description', 'content' => $news['description']], 'og:description');
        \Yii::$app->view->registerMetaTag(['property'=>'og:type', 'content' => 'website'], 'og:type');
        \Yii::$app->view->registerMetaTag(['property'=>'og:url', 'content' => 'https://'.Yii::$app->request->serverName.Yii::$app->request->url], 'og:url');
        \Yii::$app->view->registerMetaTag(['property'=>'og:site_name', 'content' => Yii::$app->name], 'og:site_name');
        Yii::$app->view->title=$news['title'];

        return $this->render('view', [
          'settings'=>$this->settings,
          'categories'=>$this->categories,
          'brands'=>$this->brands,
          'menupage'=>$this->menupage,
          'content'=>$news,


        ]);
    }


}
