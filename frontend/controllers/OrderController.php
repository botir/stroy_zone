<?php

namespace frontend\controllers;

use common\models\CatalogCategory;
use common\models\CatalogBrand;
use common\models\CatalogProduct;
use common\models\OrderOrder;
use yii\web\NotFoundHttpException;
use frontend\models\OrderForm;
use frontend\components\FrontendController;
use Yii;

/**
 * Order controller
 */
class OrderController extends FrontendController
{

    /**
     * @return string
     */
    public function actionCheckout()
    {
      \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('frontend','default_description_title')], 'description');
      \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('frontend','default_keywords_title')], 'keywords');
      \Yii::$app->view->registerMetaTag(['property'=>'og:title', 'content' => Yii::t('frontend','default_page_checkout_title')], 'og:title');
      \Yii::$app->view->registerMetaTag(['property'=>'og:description', 'content' => Yii::t('frontend','default_og_description')], 'og:description');
      \Yii::$app->view->registerMetaTag(['property'=>'og:type', 'content' => 'website'], 'og:type');
      \Yii::$app->view->registerMetaTag(['property'=>'og:url', 'content' => 'https://'.Yii::$app->request->serverName.Yii::$app->request->url], 'og:url');
      \Yii::$app->view->registerMetaTag(['property'=>'og:site_name', 'content' => Yii::$app->name], 'og:site_name');
      Yii::$app->view->title=Yii::t('frontend','default_page_checkout_title');
        $model = new OrderForm();
        $session = Yii::$app->session;
        $carts = $session['cart_data'];
        $is_new = true;
        $quantity = 1;
        $ids = [];
        foreach($carts as  $cart_data){
          $ids[]=$cart_data['product_id'];
        }
        if (count($ids) > 0)
          $products = CatalogProduct::getCartProducts($ids);
        else {
          $products = null;
        }
        $price = 0;
        $result = [];
        if ($products){

            foreach ($products as $product) {
              $image = $product['base_url'].'/thumbnails/_medium/'.$product['image_name'].'_medium.'.$product['image_ext'];
              $carts = $session['cart_data'];
              $quantity = 1;
              foreach($carts as $cart_data){
                    if ($cart_data['product_id'] == $product['id']){
                      $quantity = $cart_data['quantity'];
                      break;
                    }
              }
              $price += $product['price']*$quantity;
              $result['products'] [] = [
                  'id'=>$product['id'],
                  'title'=>$product['title'],
                  'category_title'=>$product['category_title'],
                  'image'=>$image,
                  'price'=>$product['price'],
                  'total_price'=>$product['price']*$quantity,
                  'quantity'=>$quantity,
                ];
            }
            $result['total'] = $price.' сум';

            if ($model->load(Yii::$app->request->post())) {
                $order = $model->save($result['products']);
                if ($order)
                  return $this->redirect(['view', 'id'=>$order->order_code]);
            }

        }else{
            $result['products'] = null;
            $result['total'] = '0 сум';
        }
        return $this->render('checkout', [
          'settings'=>$this->settings,
          'categories'=>$this->categories,
          'brands'=>$this->brands,
          'menupage'=>$this->menupage,
          'model' => $model,
          'result' => $result
        ]);
    }

    /**
     * @return string
     */
    public function actionView($id)
    {
      $model = $this->findModel($id);
      return $this->render('view', [
        'settings'=>$this->settings,
        'categories'=>$this->categories,
        'brands'=>$this->brands,
        'menupage'=>$this->menupage,
        'model' => $model,
      ]);
    }


   protected function findModel($order_code)
   {
       if (($model = OrderOrder::find()->where(['order_code'=>$order_code])->One()) !== null) {
           return $model;
       }
       throw new NotFoundHttpException('The requested page does not exist.');
   }

}
