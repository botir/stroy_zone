<?php

namespace frontend\controllers;

use common\models\CatalogCategory;
use common\models\CatalogBrand;
use common\models\CatalogProduct;
use yii\web\NotFoundHttpException;
use frontend\models\FilterForm;
use frontend\components\FrontendController;
use Yii;

/**
 * Brand controller
 */
class BrandController extends FrontendController
{

    /**
     * @return string
     */
    public function actionIndex()
    {
      \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('frontend','default_page_catalog_descriptio')], 'description');
      \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('frontend','default_keywords_title')], 'keywords');
      \Yii::$app->view->registerMetaTag(['property'=>'og:title', 'content' => Yii::t('frontend','default_page_brand_title')], 'og:title');
      \Yii::$app->view->registerMetaTag(['property'=>'og:description', 'content' => Yii::t('frontend','default_page_catalog_descriptio')], 'og:description');
      \Yii::$app->view->registerMetaTag(['property'=>'og:type', 'content' => 'website'], 'og:type');
      \Yii::$app->view->registerMetaTag(['property'=>'og:url', 'content' => 'https://'.Yii::$app->request->serverName.Yii::$app->request->url], 'og:url');
      \Yii::$app->view->registerMetaTag(['property'=>'og:site_name', 'content' => Yii::$app->name], 'og:site_name');
      Yii::$app->view->title=Yii::t('frontend','default_page_brand_title');

        $all_brands = CatalogBrand::getAllList();
        $letters = [];
        foreach ($all_brands as $brand) {
          $letters[mb_substr($brand['title'], 0, 1, 'utf-8')][] = $brand;
        }
        return $this->render('index', [
          'settings'=>$this->settings,
          'categories'=>$this->categories,
          'brands'=>$this->brands,
          'menupage'=>$this->menupage,
          'brands'=>$all_brands,
          'letters'=>$letters
        ]);
    }

    public function actionView($slug)
    {

        $slug_s = strip_tags(stripslashes ( $slug ));
        $brand = CatalogBrand::getOneBrand($slug_s);
        if (!$brand){
          throw new NotFoundHttpException;
        }

        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('frontend','default_page_catalog_descriptio')], 'description');
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('frontend','default_keywords_title')], 'keywords');
        \Yii::$app->view->registerMetaTag(['property'=>'og:title', 'content' => $brand['title'].'|'.Yii::$app->name], 'og:title');
        \Yii::$app->view->registerMetaTag(['property'=>'og:description', 'content' => Yii::t('frontend','default_page_catalog_descriptio')], 'og:description');
        \Yii::$app->view->registerMetaTag(['property'=>'og:type', 'content' => 'website'], 'og:type');
        \Yii::$app->view->registerMetaTag(['property'=>'og:url', 'content' => 'https://'.Yii::$app->request->serverName.Yii::$app->request->url], 'og:url');
        \Yii::$app->view->registerMetaTag(['property'=>'og:site_name', 'content' => Yii::$app->name], 'og:site_name');
        Yii::$app->view->title=$brand['title'].' | '.Yii::$app->name;

        $categories = CatalogProduct::getBrendCategories($brand['id']);
        if (isset($_GET['pfrom'])&& $_GET['pfrom'] && is_numeric($_GET['pfrom']) && intval($_GET['pfrom'])> 0) {
          $pfrom = intval($_GET['pfrom']);
        }
        else{
          $pfrom =null;
        }
        if (isset($_GET['pto'])&& $_GET['pto'] && is_numeric($_GET['pto']) && intval($_GET['pto'])> 0) {
          $pto = intval($_GET['pto']);
        }
        else{
          $pto =null;
        }
        if (isset($_GET['order'])&& $_GET['order'] && $_GET['order']=='price') {
          $order = 'catalog_product.price asc, catalog_product.created_at desc';
        }else{
          $order = 'catalog_product.created_at desc';
        }
        $products = CatalogProduct::getBrandProducts($brand['id'], $order, $pfrom, $pto);

        return $this->render('view', [
          'settings'=>$this->settings,
          'categories'=>$this->categories,
          'brands'=>$this->brands,
          'menupage'=>$this->menupage,
          'brand'=>$brand,
          'brand_categories'=>$categories,
          'products'=>$products['models'],
          'pagination'=>$products['pages'],
          'count_products'=>$products['count'],

          'pfrom'=>$pfrom,
          'pto'=>$pto,

        ]);
    }


}
