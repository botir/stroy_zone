<?php

namespace frontend\controllers;

use common\models\GeoDistrict;
use yii\web\NotFoundHttpException;
use frontend\models\FilterForm;
use frontend\components\FrontendController;
use yii\web\Response;
use yii\helpers\Url;
use Yii;

/**
 * Region controller
 */
class RegionController extends FrontendController
{

  /**
   * @return string
   */
  public function actionView($id)
  {
      if (\Yii::$app->request->isAjax){
        Yii::$app->response->format = Response::FORMAT_JSON;
            $result = GeoDistrict::getListDataFromRegion($id);
            return $result;
        }
  }


    /**
     * @return string
     */
    public function actionAdd()
    {
        if (\Yii::$app->request->isAjax){
          $session = Yii::$app->session;
          Yii::$app->response->format = Response::FORMAT_JSON;

          if (!$session->has('cart_data')){
              $session['cart_data'] = [];
          }
          if (isset($_REQUEST['product']))
    	       $product=$_REQUEST['product'];
          else {
            throw new \yii\web\HttpException(404, 'Not Found');
          }
          $product = CatalogProduct::getByID($product);
          if ($product){
            $carts = $session['cart_data'];
            $is_new = true;
            $quantity = 1;
            foreach($carts as  $index=>$cart_data){
                  if ($cart_data['product_id'] == $product['id']){
                    $quantity = $cart_data['quantity'] + 1;
                    unset($carts[$index]);
                    break;
                  }
            }
            $carts[] = [
              'product_id'            => $product['id'],
			        'quantity'              => $quantity
            ];
            $session['cart_data'] = $carts;
          }


          return ['success'=>true];
        }
    }

    /**
     * @return string
     */
    public function actionRemove()
    {
        if (\Yii::$app->request->isAjax){
          $session = Yii::$app->session;
          Yii::$app->response->format = Response::FORMAT_JSON;

          if (!$session->has('cart_data')){
              $session['cart_data'] = [];
          }
          if (isset($_REQUEST['product']))
             $product=$_REQUEST['product'];
          else {
            throw new \yii\web\HttpException(404, 'Not Found');
          }
          $product = CatalogProduct::getByID($product);
          if ($product){
            $carts = $session['cart_data'];
            foreach($carts as  $index=>$cart_data){
                  if ($cart_data['product_id'] == $product['id']){
                    unset($carts[$index]);
                    break;
                  }
            }
            $session['cart_data'] = $carts;
          }
          return ['success'=>true];
        }
    }

}
