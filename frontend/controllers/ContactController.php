<?php

namespace frontend\controllers;

use yii\web\NotFoundHttpException;
use frontend\components\FrontendController;
use Yii;

/**
 * Contact controller
 */
class ContactController extends FrontendController
{

    /**
     * @return string
     */
    public function actionIndex()
    {
      \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('frontend','default_description_title')], 'description');
      \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('frontend','default_keywords_title')], 'keywords');
      \Yii::$app->view->registerMetaTag(['property'=>'og:title', 'content' => Yii::t('frontend','default_page_contact_title')], 'og:title');
      \Yii::$app->view->registerMetaTag(['property'=>'og:description', 'content' => Yii::t('frontend','default_og_description')], 'og:description');
      \Yii::$app->view->registerMetaTag(['property'=>'og:type', 'content' => 'website'], 'og:type');
      \Yii::$app->view->registerMetaTag(['property'=>'og:url', 'content' => 'https://'.Yii::$app->request->serverName.Yii::$app->request->url], 'og:url');
      \Yii::$app->view->registerMetaTag(['property'=>'og:site_name', 'content' => Yii::$app->name], 'og:site_name');
      Yii::$app->view->title=Yii::t('frontend','default_page_contact_title');
        return $this->render('index', [
          'settings'=>$this->settings,
          'categories'=>$this->categories,
          'brands'=>$this->brands,
          'menupage'=>$this->menupage
        ]);
    }




}
