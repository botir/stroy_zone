<?php

namespace frontend\controllers;

use common\models\CatalogCategory;
use common\models\CatalogBrand;
use common\models\CatalogProduct;
use yii\web\NotFoundHttpException;
use frontend\models\FilterForm;
use frontend\components\FrontendController;
use yii\web\Response;
use yii\helpers\Url;
use Yii;

/**
 * Cart controller
 */
class CartController extends FrontendController
{

  /**
   * @return string
   */
  public function actionIndex()
  {
    $session = Yii::$app->session;
    if (!$session->has('cart_data')){
        return [
          'products'=>[],
          'total'=>0
        ];
    }
    $carts = $session['cart_data'];
    $is_new = true;
    $quantity = 1;
    $ids = [];
    foreach($carts as  $cart_data){
      $ids[]=$cart_data['product_id'];
    }
    if (count($ids) > 0)
      $products = CatalogProduct::getCartProducts($ids);
    else {
      $products = null;
    }


    $price = 0;

    if ($products){
        $result = [];
        foreach ($products as $product) {
          $image = $product['base_url'].'/thumbnails/_medium/'.$product['image_name'].'_medium.'.$product['image_ext'];
          $carts = $session['cart_data'];
          $quantity = 1;
          foreach($carts as $cart_data){
                if ($cart_data['product_id'] == $product['id']){
                  $quantity = $cart_data['quantity'];
                  break;
                }
          }
          $price += $product['price']*$quantity;
          $result['products'] [] = [
              'id'=>$product['id'],
              'title'=>$product['title'],
              'category_title'=>$product['category_title'],
              'url'=>Url::to(['/product/view', 'slug'=>$product['slug']]),
              'image'=>$image,
              'price'=>$product['price'],
              'price_num'=>$product['price'],
              'total_price'=>$product['price']*$quantity,
              'quantity'=>$quantity,
            ];
        }

        $result['total'] = $price.' сум';
      if (\Yii::$app->request->isAjax){

        Yii::$app->response->format = Response::FORMAT_JSON;


            return $result;
        }

      }
      else{
          $result['products'] = null;
          $result['total'] = '0 сум';
          if (\Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
                return $result;
            }
      }
      \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('frontend','default_description_title')], 'description');
      \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('frontend','default_keywords_title')], 'keywords');
      \Yii::$app->view->registerMetaTag(['property'=>'og:title', 'content' => Yii::t('frontend','default_page_cart_title')], 'og:title');
      \Yii::$app->view->registerMetaTag(['property'=>'og:description', 'content' => Yii::t('frontend','default_og_description')], 'og:description');
      \Yii::$app->view->registerMetaTag(['property'=>'og:type', 'content' => 'website'], 'og:type');
      \Yii::$app->view->registerMetaTag(['property'=>'og:url', 'content' => 'https://'.Yii::$app->request->serverName.Yii::$app->request->url], 'og:url');
      \Yii::$app->view->registerMetaTag(['property'=>'og:site_name', 'content' => Yii::$app->name], 'og:site_name');
      Yii::$app->view->title=Yii::t('frontend','default_page_cart_title');
      return $this->render('index', [
        'settings'=>$this->settings,
        'categories'=>$this->categories,
        'brands'=>$this->brands,
        'menupage'=>$this->menupage,
        'result' => $result
      ]);



  }


    /**
     * @return string
     */
    public function actionAdd()
    {
        if (\Yii::$app->request->isAjax){
          $session = Yii::$app->session;
          Yii::$app->response->format = Response::FORMAT_JSON;

          if (!$session->has('cart_data')){
              $session['cart_data'] = [];
          }
          if (isset($_REQUEST['product']))
    	       $product=$_REQUEST['product'];
          else {
            throw new \yii\web\HttpException(404, 'Not Found');
          }
          $product = CatalogProduct::getByID($product);
          if ($product){
            $carts = $session['cart_data'];
            $is_new = true;
            $quantity = 1;
            foreach($carts as  $index=>$cart_data){
                  if ($cart_data['product_id'] == $product['id']){
                    $quantity = $cart_data['quantity'] + 1;
                    unset($carts[$index]);
                    break;
                  }
            }
            $carts[] = [
              'product_id'            => $product['id'],
			        'quantity'              => $quantity
            ];
            $session['cart_data'] = $carts;
          }


          return ['success'=>true];
        }
    }

    /**
     * @return string
     */
    public function actionRemove()
    {
        if (\Yii::$app->request->isAjax){
          $session = Yii::$app->session;
          Yii::$app->response->format = Response::FORMAT_JSON;

          if (!$session->has('cart_data')){
              $session['cart_data'] = [];
          }
          if (isset($_REQUEST['product']))
             $product=$_REQUEST['product'];
          else {
            throw new \yii\web\HttpException(404, 'Not Found');
          }
          $product = CatalogProduct::getByID($product);
          if ($product){
            $carts = $session['cart_data'];
            foreach($carts as  $index=>$cart_data){
                  if ($cart_data['product_id'] == $product['id']){
                    unset($carts[$index]);
                    break;
                  }
            }
            $session['cart_data'] = $carts;
          }
          return ['success'=>true];
        }
    }

}
