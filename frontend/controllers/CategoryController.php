<?php

namespace frontend\controllers;

use common\models\CatalogCategory;
use common\models\CatalogProduct;
use yii\web\NotFoundHttpException;
use frontend\models\FilterForm;
use frontend\components\FrontendController;
use Yii;

/**
 * Category controller
 */
class CategoryController extends FrontendController
{

    /**
     * @return string
     */
    public function actionIndex()
    {
      \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('frontend','default_page_catalog_descriptio')], 'description');
      \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('frontend','default_keywords_title')], 'keywords');
      \Yii::$app->view->registerMetaTag(['property'=>'og:title', 'content' => Yii::t('frontend','default_page_catalog_title')], 'og:title');
      \Yii::$app->view->registerMetaTag(['property'=>'og:description', 'content' => Yii::t('frontend','default_page_catalog_descriptio')], 'og:description');
      \Yii::$app->view->registerMetaTag(['property'=>'og:type', 'content' => 'website'], 'og:type');
      \Yii::$app->view->registerMetaTag(['property'=>'og:url', 'content' => 'https://'.Yii::$app->request->serverName.Yii::$app->request->url], 'og:url');
      \Yii::$app->view->registerMetaTag(['property'=>'og:site_name', 'content' => Yii::$app->name], 'og:site_name');
      Yii::$app->view->title=Yii::t('frontend','default_page_catalog_title');

        return $this->render('index', [
          'settings'=>$this->settings,
          'categories'=>$this->categories,
          'brands'=>$this->brands,
          'menupage'=>$this->menupage
        ]);
    }

    public function actionView($slug)
    {

        $slug_s = strip_tags(stripslashes ( $slug ));
        $category = CatalogCategory::getOneCategory($slug_s);
        if (!$category){
          throw new NotFoundHttpException;
        }

        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('frontend','default_page_catalog_descriptio')], 'description');
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('frontend','default_keywords_title')], 'keywords');
        \Yii::$app->view->registerMetaTag(['property'=>'og:title', 'content' => $category['title'].''], '|'.Yii::$app->name);
        \Yii::$app->view->registerMetaTag(['property'=>'og:description', 'content' => Yii::t('frontend','default_page_catalog_descriptio')], 'og:description');
        \Yii::$app->view->registerMetaTag(['property'=>'og:type', 'content' => 'website'], 'og:type');
        \Yii::$app->view->registerMetaTag(['property'=>'og:url', 'content' => 'https://'.Yii::$app->request->serverName.Yii::$app->request->url], 'og:url');
        \Yii::$app->view->registerMetaTag(['property'=>'og:site_name', 'content' => Yii::$app->name], 'og:site_name');
        Yii::$app->view->title=$category['title'].'|'.Yii::$app->name;

        if ($category['parent_id'] && $category['parent_id']>0){
            $main_id = $category['parent_id'];
            $childs = CatalogCategory::getparentChilds($main_id);
            $ids = [$category['id']];
        }else{
            $main_id = $category['id'];
            $childs = CatalogCategory::getparentChilds($main_id);
            $ids = [$main_id];
            if ($childs and count($childs)> 0){
              foreach ($childs as $child) {
                  $ids[] = $child['id'];
              }
            }
        }
        $brands = CatalogProduct::getBrends($ids);


        $filter_form = new FilterForm();
        if (isset($_GET['brands'])&& $_GET['brands'] && is_array($_GET['brands']) && count($_GET['brands'])> 0) {
          $selected_brands = $_GET['brands'];
        }else{
          $selected_brands=null;
        }
        if (isset($_GET['pfrom'])&& $_GET['pfrom'] && is_numeric($_GET['pfrom']) && intval($_GET['pfrom'])> 0) {
          $pfrom = intval($_GET['pfrom']);
        }
        else{
          $pfrom =null;
        }
        if (isset($_GET['pto'])&& $_GET['pto'] && is_numeric($_GET['pto']) && intval($_GET['pto'])> 0) {
          $pto = intval($_GET['pto']);
        }
        else{
          $pto =null;
        }
        if (isset($_GET['order'])&& $_GET['order'] && $_GET['order']=='price') {
          $order = 'catalog_product.price asc, catalog_product.created_at desc';
        }else{
          $order = 'catalog_product.created_at desc';
        }
        $products = CatalogProduct::getCategoryProducts($ids, $order, $selected_brands, $pfrom, $pto);
        return $this->render('view', [
          'settings'=>$this->settings,
          'categories'=>$this->categories,
          'brands'=>$this->brands,
          'menupage'=>$this->menupage,
          'childs'=>$childs,
          'category'=>$category,
          'filter_brands'=>$brands,
          'products'=>$products['models'],
          'pagination'=>$products['pages'],
          'count_products'=>$products['count'],
          'filter_form'=>$filter_form,
          'selected_brands'=>$selected_brands,
          'pfrom'=>$pfrom,
          'pto'=>$pto,
        ]);
    }


}
