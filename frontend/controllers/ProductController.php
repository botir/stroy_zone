<?php

namespace frontend\controllers;

use common\models\CatalogCategory;
use common\models\CatalogProduct;
use yii\web\NotFoundHttpException;
use frontend\models\FilterForm;
use frontend\components\FrontendController;
use Yii;

/**
 * Product
 */
class ProductController extends FrontendController
{

    /**
     * @return string
     */
    public function actionIndex()
    {

        return $this->render('index', [
          'settings'=>$this->settings,
          'categories'=>$this->categories,
          'brands'=>$this->brands,
          'menupage'=>$this->menupage
        ]);
    }

    public function actionView($slug)
    {

        $slug_s = strip_tags(stripslashes ( $slug ));
        $product = CatalogProduct::getBySlug($slug_s);
        if (!$product){
          throw new NotFoundHttpException;
        }
        $images = CatalogProduct::getAllImages($product['id']);
        $realted = CatalogProduct::getRelatedProducts($product['category_id'], $product['id']);


        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => $this->substr_unicode($product['description'],0, 200)], 'description');
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('frontend','default_keywords_title')], 'keywords');
        \Yii::$app->view->registerMetaTag(['property'=>'og:title', 'content' => Yii::t('frontend','default_og_title')], 'og:title');
        \Yii::$app->view->registerMetaTag(['property'=>'og:description', 'content' => $this->substr_unicode($product['description'],0, 200)], 'og:description');
        \Yii::$app->view->registerMetaTag(['property'=>'og:type', 'content' => 'website'], 'og:type');
        \Yii::$app->view->registerMetaTag(['property'=>'og:url', 'content' => 'https://'.Yii::$app->request->serverName.Yii::$app->request->url], 'og:url');
        \Yii::$app->view->registerMetaTag(['property'=>'og:site_name', 'content' => Yii::$app->name], 'og:site_name');
        Yii::$app->view->title=$product['title'];

        return $this->render('view', [
          'settings'=>$this->settings,
          'categories'=>$this->categories,
          'brands'=>$this->brands,
          'menupage'=>$this->menupage,
          'product'=>$product,
          'images'=>$images,
          'realted'=>$realted,

        ]);
    }

    public function actionSearch()
    {
        $lang = \Yii::$app->language;
        if (isset($_GET['q']) && $_GET['q'] !='/list'){
            $search_text = strip_tags(stripslashes ($_GET['q']));
        }else{
            $search_text = null;
        }

        if (isset($_GET['pfrom'])&& $_GET['pfrom'] && is_numeric($_GET['pfrom']) && intval($_GET['pfrom'])> 0) {
          $pfrom = intval($_GET['pfrom']);
        }
        else{
          $pfrom =null;
        }
        if (isset($_GET['pto'])&& $_GET['pto'] && is_numeric($_GET['pto']) && intval($_GET['pto'])> 0) {
          $pto = intval($_GET['pto']);
        }
        else{
          $pto =null;
        }
        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' =>Yii::t('frontend','default_page_catalog_descriptio')], 'description');
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('frontend','default_keywords_title')], 'keywords');
        \Yii::$app->view->registerMetaTag(['property'=>'og:title', 'content' => Yii::t('frontend','default_og_title')], 'og:title');
        \Yii::$app->view->registerMetaTag(['property'=>'og:description', 'content' =>  Yii::t('frontend','default_page_serch_title')], 'og:description');
        \Yii::$app->view->registerMetaTag(['property'=>'og:type', 'content' => 'website'], 'og:type');
        \Yii::$app->view->registerMetaTag(['property'=>'og:url', 'content' => 'https://'.Yii::$app->request->serverName.Yii::$app->request->url], 'og:url');
        \Yii::$app->view->registerMetaTag(['property'=>'og:site_name', 'content' => Yii::$app->name], 'og:site_name');
        Yii::$app->view->title=Yii::t('frontend','default_page_serch_title');
        $products = CatalogProduct::getSearching(mb_strtolower($search_text, 'UTF-8'), $pfrom, $pto);
        return $this->render('search', [
          'settings'=>$this->settings,
          'categories'=>$this->categories,
          'brands'=>$this->brands,
          'menupage'=>$this->menupage,
          'products'=>$products['models'],
          'pagination'=>$products['pages'],
          'count_products'=>$products['count'],

          'pfrom'=>$pfrom,
          'pto'=>$pto,

        ]);

    }




}
