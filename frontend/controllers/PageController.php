<?php

namespace frontend\controllers;

use common\models\ContentPage;
use yii\web\NotFoundHttpException;
use frontend\models\FilterForm;
use frontend\components\FrontendController;
use Yii;

/**
 * Page
 */
class PageController extends FrontendController
{

    public function actionView($slug)
    {

        $slug_s = strip_tags(stripslashes ( $slug ));
        $content = ContentPage::getOnePage($slug_s);
        if (!$content){
          throw new NotFoundHttpException;
        }

        \Yii::$app->view->registerMetaTag(['name' => 'description', 'content' => Yii::t('frontend','default_description_title')], 'description');
        \Yii::$app->view->registerMetaTag(['name' => 'keywords', 'content' => Yii::t('frontend','default_keywords_title')], 'keywords');
        \Yii::$app->view->registerMetaTag(['property'=>'og:title', 'content' => $content['title']], 'og:title');
        \Yii::$app->view->registerMetaTag(['property'=>'og:description', 'content' => Yii::t('frontend','default_og_description')], 'og:description');
        \Yii::$app->view->registerMetaTag(['property'=>'og:type', 'content' => 'website'], 'og:type');
        \Yii::$app->view->registerMetaTag(['property'=>'og:url', 'content' => 'https://'.Yii::$app->request->serverName.Yii::$app->request->url], 'og:url');
        \Yii::$app->view->registerMetaTag(['property'=>'og:site_name', 'content' => Yii::$app->name], 'og:site_name');
        Yii::$app->view->title=$content['title'];

        return $this->render('view', [
          'settings'=>$this->settings,
          'categories'=>$this->categories,
          'brands'=>$this->brands,
          'menupage'=>$this->menupage,
          'content'=>$content
        ]);
    }


}
