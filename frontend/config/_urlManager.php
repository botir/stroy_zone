<?php

use Sitemaped\Sitemap;

return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [


        // Pages
        ['pattern' => 'page/<slug>', 'route' => 'page/view'],
        ['pattern' => 'region/get/<id>', 'route' => 'region/view'],

        // categories
        ['pattern' => 'catalog', 'route' => 'category/index'],
        ['pattern' => 'catalog/<slug>', 'route' => 'category/view'],

        // products
        ['pattern' => 'products', 'route' => 'product/index'],
        ['pattern' => 'product/<slug>', 'route' => 'product/view'],
        ['pattern' => 'search', 'route' => 'product/search'],

        ['pattern' => 'brands', 'route' => 'brand/index'],
        ['pattern' => 'brand/<slug>', 'route' => 'brand/view'],

        ['pattern' => 'contacts', 'route' => 'contact/index'],
        ['pattern' => 'news', 'route' => 'news/index'],
        ['pattern' => 'news/<slug>', 'route' => 'news/view'],

        ['pattern' => 'cart/add', 'route' => 'cart/add'],
        ['pattern' => 'cart/get', 'route' => 'cart/index'],
        ['pattern' => 'cart/remove', 'route' => 'cart/remove'],
        ['pattern' => 'checkout', 'route' => 'order/checkout'],
        ['pattern' => 'order/<id>', 'route' => 'order/view'],

        ['pattern'=>'', 'route'=>'site/index'],

        // Sitemap
        ['pattern' => 'sitemap.xml', 'route' => 'site/sitemap', 'defaults' => ['format' => Sitemap::FORMAT_XML]],
        ['pattern' => 'sitemap.txt', 'route' => 'site/sitemap', 'defaults' => ['format' => Sitemap::FORMAT_TXT]],
        ['pattern' => 'sitemap.xml.gz', 'route' => 'site/sitemap', 'defaults' => ['format' => Sitemap::FORMAT_XML, 'gzip' => true]],
    ]
];
