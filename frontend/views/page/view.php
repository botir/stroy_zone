<?php
/**
 * @var yii\web\View $this
 */
 use yii\helpers\Url;
 use yii\helpers\Json;


$this->settings = $settings;
$this->categories = $categories;
$this->brands = $brands;
$this->menupage = $menupage;
?>
<section class="uk-section uk-section-small">
  <div class="uk-container">
    <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
      <section class="uk-text-center">

      </section>
       <section>
         <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
           <section>
             <article class="uk-card uk-card-default uk-card-body uk-article tm-ignore-container">
               <header class="uk-text-center">
                  <h1 class="uk-article-title"><?=$content['title']?></h1>
                  <div class="uk-article-meta">

                  </div>
                </header>
                <section class="uk-article-body">
                  <p><?=$content['content_text']?></p>
                </section>
              </<article>
            </section>
         </div>
        </section>
    </div>
  </div>
</section>
