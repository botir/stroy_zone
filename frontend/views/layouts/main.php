<?php
/**
 * @var yii\web\View $this
 * @var string $content
 */
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Html;

$this->beginContent('@frontend/views/layouts/_clear.php')
?>
<div class="uk-offcanvas-content">
  <header>
<div class="uk-navbar-container uk-light uk-visible@m tm-toolbar-container">
  <div class="uk-container" uk-navbar>
    <div class="uk-navbar-left">
      <nav>
        <ul class="uk-navbar-nav">
          <?php if ($this->settings): ?>
          <li><a href="tel:<?=$this->settings['phone_number']?>"><span class="uk-margin-xsmall-right" uk-icon="icon: receiver; ratio: .75;"></span><span class="tm-pseudo"><?=$this->settings['phone_number']?></span></a></li>
          <?php endif;?>
          <li>
            <a href="<?=Url::to(['/contact/index'])?>" onclick="return false">
              <span class="uk-margin-xsmall-right" uk-icon="icon: location; ratio: .75;"></span>
              <span class="tm-pseudo"><?=$this->settings['address']?></span>
              <span uk-icon="icon: triangle-down; ratio: .75;"></span>
            </a>
            <div class="uk-margin-remove" uk-drop="mode: click; pos: bottom-center;">
              <div class="uk-card uk-card-default uk-card-small uk-box-shadow-xlarge uk-overflow-hidden uk-padding-small uk-padding-remove-horizontal uk-padding-remove-bottom">
                <figure class="uk-card-media-top uk-height-small js-map" data-latitude="<?=$this->settings['maps_lat']?>" data-longitude="<?=$this->settings['maps_lon']?>" data-zoom="14"></figure>
                <div class="uk-card-body"><div class="uk-text-small"><div class="uk-text-bolder"><?=$this->settings['store_name']?></div>
                <div><?=$this->settings['address']?></div><div><?=$this->settings['schedule']?></div></div>
                </div></div></div>
                </li>
                <li><div class="uk-navbar-item"><span class="uk-margin-xsmall-right" uk-icon="icon: clock; ratio: .75;"></span><?=$this->settings['schedule']?></div></li>
              </ul>
            </nav>
          </div>
          <div class="uk-navbar-right">
            <nav>
              <ul class="uk-navbar-nav">
                <li><a href="<?=Url::to(['/news/index'])?>"><?=Yii::t('frontend', 'News')?></a></li>
                <li><a href="<?=Url::to(['/page/view', 'slug'=>'oplata'])?>"><?=Yii::t('frontend', 'Payment')?></a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
      <div class="uk-navbar-container tm-navbar-container" uk-sticky="cls-active: tm-navbar-container-fixed">
        <div class="uk-container" uk-navbar>
          <div class="uk-navbar-left">
            <button class="uk-navbar-toggle uk-hidden@m" uk-toggle="target: #nav-offcanvas" uk-navbar-toggle-icon></button>
            <a class="uk-navbar-item uk-logo" href="<?=Url::to(['/site/index'])?>"><img src="<?=$this->settings['logo1']?>" width="90" height="32" alt="Logo"></a>
            <nav class="uk-visible@m">
              <ul class="uk-navbar-nav">

                <li>
                  <a href="<?=Url::to(['/category/index'])?>"><?=Yii::t('frontend', 'Catalog')?><span class="uk-margin-xsmall-left" uk-icon="icon: chevron-down; ratio: .75;"></span></a>
                  <div class="uk-navbar-dropdown uk-margin-remove uk-padding-remove-vertical" uk-drop="pos: bottom-justify;delay-show: 125;delay-hide: 50;duration: 75;boundary: .tm-navbar-container;boundary-align: true;pos: bottom-justify;flip: x">
                    <div class="uk-container">
                      <ul class="uk-navbar-dropdown-grid uk-child-width-1-5" uk-grid>
                        <?php if ($this->categories): ?>
                          <?php foreach ($this->categories as $data): ?>
                            <li>
                              <div class="uk-margin-top uk-margin-bottom">
                                <a class="uk-link-reset" href="<?=Url::to(['/category/view', 'slug'=>$data['slug']])?>">
                                  <img class="uk-display-block uk-margin-auto uk-margin-bottom" src="<?=$data['icon']?>" width="80" height="80">
                                  <div class="uk-text-bolder"><?=$data['title']?></div>
                                </a>
                                <?php if ($data['cats']): $array_data = Json::decode($data['cats']);?>
                                  <ul class="uk-nav uk-nav-default">
                                  <?php foreach ($array_data as $child_data): ?>
                                      <li><a href="<?=Url::to(['/category/view', 'slug'=>$child_data['slug']])?>"><?=$child_data['title']?></a></li>
                                  <?php endforeach; ?>
                                  </ul>
                                <?php endif;?>
                              </div>
                            </li>
                          <?php endforeach;?>
                        <?php endif;?>
                      </ul>
                    </div>
                  </div>
                </li>
                <li>
                  <?php if ($this->brands): ?>
                    <a href="<?=Url::to(['/brand/index'])?>"><?=Yii::t('frontend', 'Brands')?><span class="uk-margin-xsmall-left" uk-icon="icon: chevron-down; ratio: .75;"></span></a>
                    <div class="uk-navbar-dropdown uk-margin-remove uk-padding-remove-vertical" uk-drop="pos: bottom-justify;delay-show: 125;delay-hide: 50;duration: 75;boundary: .tm-navbar-container;boundary-align: true;pos: bottom-justify;flip: x">
                      <div class="uk-container uk-container-small uk-margin-top uk-margin-bottom">
                        <ul class="uk-grid-small uk-child-width-1-6" uk-grid>
                    <?php foreach ($this->brands as $data): ?>
                      <li>
                        <div class="tm-ratio tm-ratio-4-3">
                          <a class="uk-link-muted uk-text-center uk-display-block uk-padding-small uk-box-shadow-hover-large tm-media-box" href="<?=Url::to(['/brand/view', 'slug'=>$data['slug']])?>">
                            <figure class="tm-media-box-wrap"><img src="<?=$data['logo']?>"></figure>
                          </a>
                        </div>
                      </li>
                      <?php endforeach;?>
                  <?php endif;?>
                      </ul>
                    </div>
                  </div>
                </li>
                <?php if ($this->menupage): ?>
                  <?php foreach ($this->menupage as $data): ?>
                    <li><a href="<?=Url::to(['/page/view', 'slug'=>$data['slug']])?>"><?=$data['title']?></a></li>
                  <?php endforeach;?>
                <?php endif;?>

                <li><a href="<?=Url::to(['/contact/index'])?>"><?=Yii::t('frontend', 'Contacts')?></a></li>
              </ul>
            </nav>
          </div>
          <div class="uk-navbar-right">
            <a class="uk-navbar-toggle tm-navbar-button" href="#" uk-search-icon></a>
            <div class="uk-navbar-dropdown uk-padding-small uk-margin-remove" uk-drop="mode: click;cls-drop: uk-navbar-dropdown;boundary: .tm-navbar-container;boundary-align: true;pos: bottom-justify;flip: x">
              <div class="uk-container"><div class="uk-grid-small uk-flex-middle" uk-grid>
                <div class="uk-width-expand">
                  <?php echo Html::beginForm(Url::to(['/product/search']), 'get', ['class' => 'uk-search uk-search-navbar uk-width-1-1']); ?>

                    <input class="uk-search-input" type="search" placeholder="<?=Yii::t('frontend', 'Search')?>" name="q" autofocus>

                  <?php echo Html::endForm(); ?>
                </div>
                <div class="uk-width-auto">
                  <a class="uk-navbar-dropdown-close" href="#" uk-close></a>
                </div>
              </div>
            </div>
          </div>

          <a class="uk-navbar-item uk-link-muted tm-navbar-button" href="<?=Url::to(['/contact/index'])?>" uk-toggle="target: #cart-offcanvas" onclick="return false">
            <span uk-icon="cart"></span>
            <span class="uk-badge" id="cart_badge"></span>
          </a>
        </div>
      </div>
    </div></header>
    <main>
        <?php echo $content ?>
    </main>
    <footer>
  <section class="uk-section uk-section-secondary uk-section-small uk-light">
    <div class="uk-container">
      <div class="uk-grid-medium uk-child-width-1-1 uk-child-width-1-4@m" uk-grid>
        <div><a class="uk-logo" href="<?=Url::to(['/site/index'])?>"><img src="<?=$this->settings['logo1']?>" width="90" height="32" alt="Logo"></a>
          <p class="uk-text-small"></p>
          <ul class="uk-iconnav">
            <li><a href="<?=$this->settings['fb_link']?>" title="Facebook" uk-icon="facebook"></a></li>
            <li><a href="<?=$this->settings['tw_link']?>" title="Twitter" uk-icon="twitter"></a></li>
            <li><a href="<?=$this->settings['you_link']?>" title="YouTube" uk-icon="youtube"></a></li>
            <li><a href="<?=$this->settings['ints_link']?>" title="Instagram" uk-icon="instagram"></a></li>
          </ul>
        </div>
        <div>
          <nav class="uk-grid-small uk-child-width-1-2" uk-grid>
            <div>
              <ul class="uk-nav uk-nav-default">
                <li><a href="<?=Url::to(['/category/index'])?>"><?=Yii::t('frontend', 'Catalog')?></a></li>
                <li><a href="<?=Url::to(['/brand/index'])?>"><?=Yii::t('frontend', 'Brands')?></a></li>
                <li><a href="<?=Url::to(['/page/view', 'slug'=>'delivery'])?>"><?=Yii::t('frontend', 'Delivery')?></a></li>
                <li><a href="<?=Url::to(['/page/view', 'slug'=>'payment'])?>"><?=Yii::t('frontend', 'Payment')?></a></li>
              </ul>
            </div>
            <div>
              <ul class="uk-nav uk-nav-default">
                <li><a href="<?=Url::to(['/page/view', 'slug'=>'about'])?>"><?=Yii::t('frontend', 'About')?></a></li>
                <li><a href="<?=Url::to(['/contact/index'])?>"><?=Yii::t('frontend', 'Contacts')?></a></li>
                <li><a href="<?=Url::to(['/news/index'])?>"><?=Yii::t('frontend', 'News')?></a></li>
              </ul>
            </div>
          </nav>
        </div>
        <div>
          <ul class="uk-list uk-text-small">
            <li><a class="uk-link-muted" href="tel:<?=$this->settings['phone_number']?>"><span class="uk-margin-small-right" uk-icon="receiver"></span><span class="tm-pseudo"><?=$this->settings['phone_number']?></span></a></li>
            <li><a class="uk-link-muted" href="mailto:<?=$this->settings['email_main']?>"><span class="uk-margin-small-right" uk-icon="mail"></span><span class="tm-pseudo"><?=$this->settings['email_main']?></span></a></li>
            <li>
              <div class="uk-text-muted"><span class="uk-margin-small-right" uk-icon="location"></span><span><?=$this->settings['address']?></span>
              </div>
            </li>
            <li>
              <div class="uk-text-muted"><span class="uk-margin-small-right" uk-icon="clock"></span><span><?=$this->settings['schedule']?></span>
              </div>
            </li>
          </ul>
        </div>
        <div>


        </div>
      </div>
    </div>
  </section>
</footer>
<div id="cart-offcanvas" uk-offcanvas="overlay: true; flip: true">
  <aside class="uk-offcanvas-bar uk-padding-remove">
          <div class="uk-card uk-card-default uk-card-small uk-height-1-1 uk-flex uk-flex-column tm-shadow-remove">
            <header class="uk-card-header uk-flex uk-flex-middle">
              <div class="uk-grid-small uk-flex-1" uk-grid>
                <div class="uk-width-expand">
                  <div class="uk-h3"><?=Yii::t('frontend', 'Cart')?></div>
                </div>
                <button class="uk-offcanvas-close" type="button" uk-close></button>
              </div>
            </header>
            <div class="uk-card-body uk-overflow-auto">
              <ul class="uk-list uk-list-divider">

              </ul>
            </div>
            <footer class="uk-card-footer" style="display:none" id="cart_footer">
              <div class="uk-grid-small" uk-grid>
                <div class="uk-width-expand uk-text-muted uk-h4"><?=Yii::t('frontend', 'cart_total')?></div>
                <div class="uk-h4 uk-text-bolder" id="cart-tot"></div>
              </div>
              <div class="uk-grid-small uk-child-width-1-1 uk-child-width-1-2@m uk-margin-small" uk-grid>
                <div><a class="uk-button uk-button-default uk-margin-small uk-width-1-1" href="<?=Url::to(['/cart/index'])?>"><?=Yii::t('frontend', 'view_cart')?></a></div>
                <div><a class="uk-button uk-button-primary uk-margin-small uk-width-1-1" href="<?=Url::to(['/order/checkout'])?>"><?=Yii::t('frontend', 'view_checkout')?></a></div>
              </div>
            </footer>
          </div>
        </aside>
</div>
</div>
<?php $this->endContent() ?>
