<?php
/**
 * @var yii\web\View $this
 */
 use yii\helpers\Url;
 use yii\helpers\Json;

$this->settings = $settings;
$this->categories = $categories;
$this->brands = $brands;
$this->menupage = $menupage;
?>
<section class="uk-section uk-section-small">
          <div class="uk-container">
            <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
              <div class="uk-text-center">
                <h1 class="uk-margin-small-top uk-margin-remove-bottom"><?=Yii::t('frontend', 'Contacts')?></h1>
              </div>
              <div>
                <div class="uk-grid-medium" uk-grid>
                  <section class="uk-width-1-1 uk-width-expand@m">
                    <article class="uk-card uk-card-default uk-card-small uk-card-body uk-article tm-ignore-container">
                      <div class="tm-wrapper">
                        <figure class="tm-ratio tm-ratio-16-9">
                            <?=$settings['maps_iframe']?>
                        </figure>
                      </div>
                      <div class="uk-child-width-1-1 uk-child-width-1-2@s uk-margin-top" uk-grid>
                        <section>
                          <h3><?=Yii::t('frontend', 'Feedback')?></h3>
                          <ul class="uk-list">
                            <li><a class="uk-link-heading" href="#"><span class="uk-margin-small-right" uk-icon="receiver"></span><span class="tm-pseudo"><?=$settings['phone_number']?></span></a></li>
                            <li><a class="uk-link-heading" href="#"><span class="uk-margin-small-right" uk-icon="mail"></span><span class="tm-pseudo"><?=$settings['email_main']?></span></a></li>
                            <li>
                              <div><span class="uk-margin-small-right" uk-icon="location"></span><span><?=$settings['address']?></span>
                              </div>
                            </li>
                            <li>
                              <div><span class="uk-margin-small-right" uk-icon="clock"></span><span><?=$this->settings['schedule']?></span>
                              </div>
                            </li>
                          </ul>
                        </section>
                        <section>
                          <h3><?=Yii::t('frontend', 'Support')?></h3>
                          <dl class="uk-description-list">
                            <dt></dt>
                            <dd><a class="uk-link-muted" href="#"><?=$this->settings['email_support']?></a></dd>

                          </dl>
                        </section>
                        </div>
                    </article>
                  </section>
                  <aside class="uk-width-1-4 uk-visible@m tm-aside-column">
                    <section class="uk-card uk-card-default uk-card-small" uk-sticky="offset: 90; bottom: true;">
                      <nav>
                        <ul class="uk-nav uk-nav-default tm-nav">

                          <li class="uk-active"><a href="<?=Url::to(['/contact/index'])?>"><?=Yii::t('frontend', 'Contacts')?></a></li>
                          <li><a href="<?=Url::to(['/news/index'])?>"><?=Yii::t('frontend', 'News')?></a></li>
                          <li><a href="<?=Url::to(['/page/view', 'slug'=>'delivery'])?>"><?=Yii::t('frontend', 'Delivery')?></a></li>
                        </ul>
                      </nav>
                    </section>
                  </aside>
                </div>
              </div>
            </div>
          </div>
        </section>
