<?php
/**
 * @var yii\web\View $this
 */
 use yii\helpers\Url;
 use yii\helpers\Json;

$this->settings = $settings;
$this->categories = $categories;
$this->brands = $brands;
$this->menupage = $menupage;
?>
<section class="uk-section uk-section-small">
          <div class="uk-container">
            <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
              <div class="uk-text-center">
                <h1 class="uk-margin-small-top uk-margin-remove-bottom"><?=Yii::t('frontend', 'Catalog')?></h1>
              </div>
              <div>
                <div class="uk-grid-medium" uk-grid>
                  <aside class="uk-width-1-4 uk-visible@m tm-aside-column">
                    <nav class="uk-card uk-card-default uk-card-body uk-card-small" uk-sticky="bottom: true; offset: 90">
                      <ul class="uk-nav uk-nav-default" uk-scrollspy-nav="closest: li; scroll: true; offset: 90">
                        <?php foreach ($this->categories as $data): ?>
                        <li><a href="#<?=$data['slug']?>"><?=$data['title']?></a></li>
                      <?php endforeach; ?>

                      </ul>
                    </nav>
                  </aside>
                  <div class="uk-width-1-1 uk-width-expand@m">
                    <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
                    <?php foreach ($this->categories as $data): ?>
                      <section id="<?=$data['slug']?>">
                        <div class="uk-card uk-card-default uk-card-small tm-ignore-container">
                          <header class="uk-card-header">
                            <div class="uk-grid-small uk-flex-middle" uk-grid><a href="<?=Url::to(['/category/view', 'slug'=>$data['slug']])?>">
                              <img src="<?=$data['icon']?>" width="50" height="50"></a>
                              <div class="uk-width-expand">
                                <h2 class="uk-h4 uk-margin-remove"><a class="uk-link-heading" href="<?=Url::to(['/category/view', 'slug'=>$data['slug']])?>"><?=$data['title']?></a></h2>

                              </div>
                            </div>
                          </header>
                          <div class="uk-card-body">
                            <ul class="uk-list">
                              <?php if ($data['cats']): $array_data = Json::decode($data['cats']);?>
                                <?php foreach ($array_data as $child_data): ?>
                              <li><a href="<?=Url::to(['/category/view', 'slug'=>$child_data['slug']])?>"><?=$child_data['title']?></a></li>
                              <?php endforeach; ?>
                              <?php endif; ?>

                            </ul>
                          </div>
                        </div>
                      </section>
                      <?php endforeach; ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
