<?php
/**
 * @var yii\web\View $this
 */
 use yii\helpers\Url;
 use yii\helpers\Json;


$this->settings = $settings;
$this->categories = $categories;
$this->brands = $brands;
$this->menupage = $menupage;
?>
<section class="uk-section uk-section-small">
  <div class="uk-container">
    <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
      <div class="uk-text-center">
        <ul class="uk-breadcrumb uk-flex-center uk-margin-remove">
          <li><a href="<?=Url::to(['/site/index'])?>"><?=Yii::t('frontend', 'to_home')?></a></li>
          <li><a href="<?=Url::to(['/category/index'])?>"><?=Yii::t('frontend', 'Catalog')?></a></li>
          <li><a href="<?=Url::to(['/category/view', 'slug'=>$product['category_slug']])?>"><?=$product['category_title']?></a></li>

          <li><span><?=$product['title']?></span></li>
        </ul>
        <h1 class="uk-margin-small-top uk-margin-remove-bottom"><?=$product['title']?></h1>
      </div>
      <div>
        <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
          <div>
            <div class="uk-card uk-card-default uk-card-small tm-ignore-container">
              <div class="uk-grid-small uk-grid-collapse uk-grid-match" uk-grid>
                <div class="uk-width-1-1 uk-width-expand@m">
                  <div class="uk-grid-collapse uk-child-width-1-1" uk-slideshow="finite: true; ratio: 4:3;" uk-grid>
                    <div>
                      <?php if($images):?>
                      <ul class="uk-slideshow-items" uk-lightbox>
                        <?php foreach ($images as $data): ?>
                        <li><a class="uk-card-body tm-media-box tm-media-box-zoom" href="<?=$data['image']?>">
                            <figure class="tm-media-box-wrap"><img src="<?=$this->getListImage($data)?>"></figure></a>
                        </li>
                        <?php endforeach; ?>

                      </ul>
                      <?php endif; ?>
                    </div>
                    <div>
                      <div class="uk-card-body uk-flex uk-flex-center">
                        <div class="uk-width-1-2 uk-visible@s">
                          <div uk-slider="finite: true">
                            <div class="uk-position-relative">
                              <div class="uk-slider-container">
                                <?php if($images):?>
                                <ul class="tm-slider-items uk-slider-items uk-child-width-1-4 uk-grid uk-grid-small">
                                  <?php foreach ($images as $data): ?>
                                  <li uk-slideshow-item="<?=$data['id']?>">
                                    <div class="tm-ratio tm-ratio-1-1"><a class="tm-media-box tm-media-box-frame">
                                        <figure class="tm-media-box-wrap"><img src="<?=$this->getListImage($data)?>"></figure></a></div>
                                  </li>
                                  <?php endforeach; ?>
                                </ul>
                                <?php endif; ?>
                                <div><a class="uk-position-center-left-out uk-position-small" href="#" uk-slider-item="previous" uk-slidenav-previous></a><a class="uk-position-center-right-out uk-position-small" href="#" uk-slider-item="next" uk-slidenav-next></a></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <ul class="uk-slideshow-nav uk-dotnav uk-hidden@s"></ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="uk-width-1-1 uk-width-1-3@m tm-product-info">
                  <div class="uk-card-body">
                    <div><a href="<?=Url::to(['/brand/view', 'slug'=>$product['brand_slug']])?>" title="<?=$product['brand_title']?>"><img src="<?=$product['brand_logo']?>" alt="<?=$product['brand_title']?>" style="height: 40px;"></a></div>
                    <div class="uk-margin">
                      <div class="uk-grid-small" uk-grid>
                        <div>
                          <?php if ($product['top_selling']): ?>
                          <span class="uk-label uk-label-warning uk-margin-xsmall-right"><?=Yii::t('frontend', 'topbuy_product')?></span>
                          <?php endif;?>
                          <?php if ($product['new_product']): ?>
                          <span class="uk-label uk-label-success uk-margin-xsmall-right"><?=Yii::t('frontend', 'new_product')?></span>
                          <?php endif;?>
                        </div>
                      </div>
                    </div>

                    <div class="uk-margin">
                      <div class="uk-padding-small uk-background-primary-lighten uk-border-rounded">
                        <div class="uk-grid-small uk-child-width-1-1" uk-grid>
                          <div>

                            <div class="tm-product-price"><?=$this->numFormat($product['price'])?></div>
                          </div>
                          <div>
                            <div class="uk-grid-small" uk-grid>

                              <div>
                                <button class="uk-button uk-button-primary tm-product-add-button tm-shine js-add-to-cart" data-id="<?=$product['id']?>"><?=Yii::t('frontend', 'add_to_cart')?></button>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="uk-margin">
                      <div class="uk-padding-small uk-background-muted uk-border-rounded">
                        <div class="uk-grid-small uk-child-width-1-1 uk-text-small" uk-grid>
                          <div>
                            <div class="uk-grid-collapse" uk-grid><span class="uk-margin-xsmall-right" uk-icon="cart"></span>
                              <div>
                                <div class="uk-text-bolder"><?=Yii::t('frontend', 'left_delivery_1')?></div>
                                <div class="uk-text-xsmall uk-text-muted"><?=Yii::t('frontend', 'left_delivery_2')?></div>
                              </div>
                            </div>
                          </div>
                          <div>
                            <div class="uk-grid-collapse" uk-grid><span class="uk-margin-xsmall-right" uk-icon="location"></span>
                              <div>
                                <div class="uk-text-bolder"><?=Yii::t('frontend', 'left_delivery_3')?></div>
                                <div class="uk-text-xsmall uk-text-muted"><?=Yii::t('frontend', 'left_delivery_4')?></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="uk-width-1-1 tm-product-description" id="description">
                  <header>
                    <nav class="tm-product-nav" uk-sticky="offset: 60; bottom: #description; cls-active: tm-product-nav-fixed;">
                      <ul class="uk-subnav uk-subnav-pill js-product-switcher" uk-switcher="connect: .js-tabs">
                        <li><a class="js-scroll-to-description" href="#description"><?=Yii::t('frontend', 'product_ob')?></a></li>
                      </ul>
                    </nav>
                  </header>
                  <div class="uk-card-body">
                    <div class="uk-switcher js-product-switcher js-tabs">
                      <section>
                        <article class="uk-article">
                          <div class="uk-article-body">
                            <p>
                              <?=$product['description']?>
                            </p>


                          </div>
                        </article>
                      </section>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <?php if ($realted):?>
          <!-- Related items-->
          <section>
            <div uk-slider="finite: true">
              <div class="uk-grid-small uk-flex-middle uk-margin-bottom" uk-grid>
                <h2 class="uk-width-expand uk-text-center uk-text-left@s"><?=Yii::t('frontend', 'related_product')?></h2>
                <div class="uk-visible@s"><a class="tm-slidenav" href="#" uk-slider-item="previous" uk-slidenav-previous></a><a class="tm-slidenav" href="#" uk-slider-item="next" uk-slidenav-next></a></div>
              </div>
              <div>
                <div class="uk-card uk-card-default uk-card-small tm-ignore-container">
                  <div class="uk-position-relative">
                    <div class="uk-slider-container">
                      <div class="uk-slider-items uk-grid-collapse uk-child-width-1-3 uk-child-width-1-4@m tm-products-grid">
                        <?php foreach ($realted as $data): ?>
                        <article class="tm-product-card">
                          <div class="tm-product-card-media">
                            <div class="tm-ratio tm-ratio-4-3"><a class="tm-media-box" href="<?=Url::to(['/product/view', 'slug'=>$data['slug']])?>">
                                <div class="tm-product-card-labels">
                                  <?php if ($data['top_selling']): ?>
                                    <span class="uk-label uk-label-warning"><?=Yii::t('frontend', 'topbuy_product')?></span>
                                  <?php endif;?>
                                  <?php if ($data['new_product']): ?>
                                  <span class="uk-label uk-label-success"><?=Yii::t('frontend', 'new_product')?></span>
                                  <?php endif;?>
                                </div>
                                <figure class="tm-media-box-wrap"><img src="<?=$this->getListImage($data)?>"/>
                                </figure></a></div>
                          </div>
                          <div class="tm-product-card-body">
                            <div class="tm-product-card-info">
                              <div class="uk-text-meta uk-margin-xsmall-bottom"><?=$data['category_title']?></div>
                              <h3 class="tm-product-card-title"><a class="uk-link-heading" href="<?=Url::to(['/product/view', 'slug'=>$data['slug']])?>"><?=$data['title']?></a></h3>

                            </div>
                            <div class="tm-product-card-shop">
                              <div class="tm-product-card-prices">

                                <div class="tm-product-card-price"><?=$this->numFormat($data['price'])?></div>
                              </div>
                              <div class="tm-product-card-add">

                                <button class="uk-button uk-button-primary tm-product-card-add-button tm-shine js-add-to-cart" data-id="<?=$data['id']?>"><span class="tm-product-card-add-button-icon" uk-icon="cart"></span><span class="tm-product-card-add-button-text"><?=Yii::t('frontend', 'add_to_cart')?></span>
                                </button>
                              </div>
                            </div>
                          </div>
                        </article>
                        <?php endforeach;?>
                      </div>
                    </div>
                  </div>
                </div>
                <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin-top uk-hidden@s"></ul>
              </div>
            </div>
          </section>
          <?php endif; ?>
        </div>

      </div>
    </div>
  </div>
</section>
