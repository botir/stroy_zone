<?php
/**
 * @var yii\web\View $this
 */
 use yii\helpers\Url;

$this->title = Yii::$app->name;
$this->settings = $settings;
$this->categories = $categories;
$this->brands = $brands;
$this->menupage = $menupage;
?>
<section class="uk-section uk-section-small">
  <div class="uk-container">
    <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
      <section class="uk-text-center"><a class="uk-link-muted uk-text-small" href="<?=Url::to(['/cart/index'])?>"><span class="uk-margin-xsmall-right" uk-icon="icon: arrow-left; ratio: .75;"></span><?=Yii::t('frontend', 'back_cart')?></a>
        <h1 class="uk-margin-small-top uk-margin-remove-bottom"><?=Yii::t('frontend', 'submit_checkout')?></h1>
      </section>
      <section>
        <div class="uk-grid-medium" uk-grid>
            <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
              <section>
                <h2 class="tm-checkout-title">Contact Information</h2>
                <div class="uk-card uk-card-default uk-card-small uk-card-body tm-ignore-container">
                  <div class="uk-grid-small uk-child-width-1-1 uk-child-width-1-2@s uk-padding-small uk-background-primary-lighten uk-border-rounded" uk-grid>
                    <div class="uk-width-1-1">
                        <h3>Спасибо, ваша заявка принята! Мы свяжемся с вами в ближайшее время.</h3>
                    </div>
                  </div>
                </div>
              </section>
            </div>
        </div>
      </section>
    </div>
  </div>
</section>
