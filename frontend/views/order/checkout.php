<?php
/**
 * @var yii\web\View $this
 */
 use yii\helpers\Url;
 use yii\helpers\Json;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->settings = $settings;
$this->categories = $categories;
$this->brands = $brands;
$this->menupage = $menupage;
?>
<section class="uk-section uk-section-small">
  <div class="uk-container">
    <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
      <section class="uk-text-center"><a class="uk-link-muted uk-text-small" href="<?=Url::to(['/cart/index'])?>"><span class="uk-margin-xsmall-right" uk-icon="icon: arrow-left; ratio: .75;"></span><?=Yii::t('frontend', 'back_cart')?></a>
        <h1 class="uk-margin-small-top uk-margin-remove-bottom"><?=Yii::t('frontend', 'submit_checkout')?></h1>
      </section>
      <section>
        <div class="uk-grid-medium" uk-grid>
          <?php $form = ActiveForm::begin([
                'id' => 'order-form',
                'validateOnSubmit' => true,
                'validateOnChange' => false,
                'validateOnType' => false,
                'options'=>['class'=>'uk-form-stacked uk-width-1-1 tm-checkout uk-width-expand@m']
          ]);
              ?>
            <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
              <section>
                <h2 class="tm-checkout-title"><?=Yii::t('frontend', 'contact_information')?></h2>
                <div class="uk-card uk-card-default uk-card-small uk-card-body tm-ignore-container">
                  <div class="uk-grid-small uk-child-width-1-1 uk-child-width-1-2@s" uk-grid>
                    <div>
                      <label>
                        <?php echo $form->field($model, 'full_name',[
                          'options' => ['class' => 'input-field'],
                          'errorOptions'  => [
                            'tag' => 'p',
                          ],
                          'template' => '<div class="uk-form-label uk-form-label-required">ФИО</div>{input}<div class="ui error message">{error}</div>'])
                        ->textInput([
                            'placeholder'=> $model->getAttributeLabel('full_name'),
                            'maxlength' => true,
                            'class' => 'uk-input'
                        ]);
                    ?>
                      </label>
                    </div>
                    <div>
                      <label>
                        <?php echo $form->field($model, 'phone_number',[
                          'options' => ['class' => 'input-field'],
                          'errorOptions'  => [
                            'tag' => 'p',
                          ],
                          'template' => '<div class="uk-form-label uk-form-label-required">Номер телефона</div>{input}<div class="ui error message">{error}</div>'])
                        ->textInput([
                            'placeholder'=> $model->getAttributeLabel('phone_number'),
                            'maxlength' => true,
                            'class' => 'uk-input'
                        ]);
                    ?>

                      </label>
                    </div>
                    <div class="uk-width-1-1">
                      <label>
                        <?php echo $form->field($model, 'comments',[
                          'options' => ['class' => 'input-field'],
                          'errorOptions'  => [
                            'tag' => 'p',
                          ],
                          'template' => '<div class="uk-form-label uk-form-label-required">Комментарии</div>{input}<div class="ui error message">{error}</div>'])
                        ->textArea([
                            'rows'=>5,
                            'placeholder'=> $model->getAttributeLabel('comments'),
                            'maxlength' => true,
                            'class' => 'uk-textarea'
                        ]);
                    ?>

                      </label>
                    </div>

                  </div>
                </div>
              </section>
              <section>
                <h2 class="tm-checkout-title"><?=Yii::t('frontend', 'shipping_information')?></h2>
                <div class="uk-card uk-card-default uk-card-small uk-card-body tm-ignore-container">
                  <div class="uk-grid-small uk-child-width-1-1 uk-child-width-1-2@s" uk-grid>
                    <div>
                      <label>
                        <?php echo $form->field($model, 'region_id',[
                          'options' => ['class' => 'input-field'],
                          'errorOptions'  => [
                            'tag' => 'p',
                          ],
                          'template' => '<div class="uk-form-label uk-form-label-required">Выберите область или город</div>{input}<div class="ui error message">{error}</div>'])
                        ->dropDownList(\common\models\GeoRegion::getListData(),
                        [
                            'placeholder'=> $model->getAttributeLabel('region_id'),
                            'class' => 'uk-select',
                            'prompt'=>''
                        ]);
                    ?>
                      </label>
                    </div>
                    <div>
                      <label>
                        <?php echo $form->field($model, 'district_id',[
                          'options' => ['class' => 'input-field'],
                          'errorOptions'  => [
                            'tag' => 'p',
                          ],
                          'template' => '<div class="uk-form-label uk-form-label-required">Выберите район</div>{input}<div class="ui error message">{error}</div>'])
                        ->dropDownList(\common\models\GeoRegion::getListData(),
                        [
                            'placeholder'=> $model->getAttributeLabel('district_id'),
                            'class' => 'uk-select',
                            'prompt'=>''
                        ]);
                    ?>

                      </label>
                    </div>

                  </div>
                </div>
              </section>
              <button class="uk-button uk-button-primary uk-margin-small uk-width-1-1"><?=Yii::t('frontend', 'btn_checkout')?></button>
            </div>
          <?php ActiveForm::end(); ?>
          <div class="uk-width-1-1 uk-width-1-4@m tm-aside-column">
            <div class="uk-card uk-card-default uk-card-small tm-ignore-container" uk-sticky="offset: 30; bottom: true; media: @m;">
              <section class="uk-card-body">
                <h4><?=Yii::t('frontend', 'product_information')?></h4>
                <?php foreach ($result['products'] as $data):?>

                <div class="uk-grid-small" uk-grid>
                  <div class="uk-width-expand">
                    <div class="uk-text-small"><?=$data['title']?></div>
                    <div class="uk-text-meta"><?=$data['quantity']?> × <?=$data['price']?></div>
                  </div>
                  <div class="uk-text-right">
                    <div><?=$data['total_price']?></div>
                  </div>
                </div>
              <?php endforeach;?>
              </section>
              <section class="uk-card-body">
                <div class="uk-grid-small uk-flex-middle" uk-grid>
                  <div class="uk-width-expand">
                    <div class="uk-text-muted"><?=Yii::t('frontend', 'checkout_total')?></div>
                  </div>
                  <div class="uk-text-right">
                    <div class="uk-text-lead uk-text-bolder"><?=$result['total']?></div>
                  </div>
                </div>

              </section>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</section>
