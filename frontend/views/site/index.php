<?php
/**
 * @var yii\web\View $this
 */
 use yii\helpers\Url;

$this->settings = $settings;
$this->categories = $categories;
$this->brands = $brands;
$this->menupage = $menupage;
?>
<?php if ($slider):?>
<section class="uk-position-relative uk-visible-toggle uk-light" uk-slideshow="min-height: 300; max-height: 600;">
  <ul class="uk-slideshow-items">
    <?php foreach ($slider as $data): ?>
    <li style="background-color: <?=$data['bg_color']?>"><a href="<?=$data['link_to']?>">
        <figure class="uk-container uk-height-1-1"><img src="<?=$data['img']?>" alt="New Macbook" width="1200" height="600" uk-cover></figure></a>
    </li>
    <?php endforeach;?>
  </ul><a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slideshow-item="previous" uk-slidenav-previous></a><a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slideshow-item="next" uk-slidenav-next></a>
  <div class="uk-position-bottom-center uk-position-small">
    <ul class="uk-slideshow-nav uk-dotnav"></ul>
  </div>
</section>
<?php endif; ?>

<?php if ($main_categories):?>
<section class="uk-section uk-section-default uk-section-small">
  <div class="uk-container">
    <div class="uk-grid-small uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-6@m" uk-grid>

      <?php foreach ($main_categories as $data): ?>
        <div><a class="uk-link-muted uk-text-center uk-display-block uk-padding-small uk-box-shadow-hover-large" href="<?=Url::to(['/category/view', 'slug'=>$data['slug']])?>">
            <div class="tm-ratio tm-ratio-4-3">
              <div class="tm-media-box">
                <figure class="tm-media-box-wrap"><img class="item-brand" src="<?=$data['icon']?>"></figure>
              </div>
            </div>
            <div class="uk-margin-small-top">
              <div class="uk-text-truncate"><?=$data['title']?></div>
              <div class="uk-text-meta uk-text-xsmall uk-text-truncate"></div>
            </div></a>
          </div>
        <?php endforeach;?>
    </div>
    <div class="uk-margin uk-text-center"><a class="uk-link-muted uk-text-uppercase tm-link-to-all" href="<?=Url::to(['/category/index'])?>"><span><?=Yii::t('frontend', 'all_catalog')?></span><span uk-icon="icon: chevron-right; ratio: .75;"></span></a>
    </div>
  </div>
</section>
<?php endif; ?>


<?php if ($trends):?>
<section class="uk-section uk-section-small">
   <div class="uk-container">
     <h2 class="uk-text-center"><?=Yii::t('frontend', 'popular_product')?></h2>
     <div class="uk-card uk-card-default tm-ignore-container">
       <div class="uk-grid-collapse uk-child-width-1-3 uk-child-width-1-4@m tm-products-grid" uk-grid>
         <?php foreach ($trends as $data): ?>
           <article class="tm-product-card">
                <div class="tm-product-card-media">
                  <div class="tm-ratio tm-ratio-4-3"><a class="tm-media-box" href="<?=Url::to(['/product/view', 'slug'=>$data['slug']])?>">
                      <div class="tm-product-card-labels">
                        <?php if ($data['top_selling']): ?>
                          <span class="uk-label uk-label-warning"><?=Yii::t('frontend', 'topbuy_product')?></span>
                        <?php endif;?>
                        <?php if ($data['new_product']): ?>
                        <span class="uk-label uk-label-success"><?=Yii::t('frontend', 'new_product')?></span>
                        <?php endif;?>
                      </div>
                      <figure class="tm-media-box-wrap"><img src="<?=$this->getListImage($data)?>"/>
                      </figure></a></div>
                </div>
                <div class="tm-product-card-body">
                  <div class="tm-product-card-info">
                    <div class="uk-text-meta uk-margin-xsmall-bottom"><?=$data['category_title']?></div>
                    <h3 class="tm-product-card-title"><a class="uk-link-heading" href="<?=Url::to(['/product/view', 'slug'=>$data['slug']])?>"><?=$data['title']?></a></h3>
                  </div>
                  <div class="tm-product-card-shop">
                    <div class="tm-product-card-prices">
                      <div class="tm-product-card-price"><?=$this->numFormat($data['price'])?></div>
                    </div>
                    <div class="tm-product-card-add">
                      <button class="uk-button uk-button-primary tm-product-card-add-button tm-shine js-add-to-cart" data-id="<?=$data['id']?>"><span class="tm-product-card-add-button-icon" uk-icon="cart"></span>
                        <span class="tm-product-card-add-button-text"><?=Yii::t('frontend', 'add_to_cart')?></span>
                      </button>
                    </div>
                  </div>
                </div>
            </article>
         <?php endforeach;?>

       </div>
     </div>
   </div>
</section>
<?php endif; ?>
<?php if ($top_brends):?>
  <section class="uk-section uk-section-default uk-section-small">
 <div class="uk-container">
   <h2 class="uk-text-center"><?=Yii::t('frontend', 'popular_brands')?></h2>
   <div class="uk-margin-medium-top" uk-slider="finite: true">
     <div class="uk-position-relative">
       <div class="uk-grid-small uk-flex-middle" uk-grid>
         <div class="uk-visible@m"><a href="#" uk-slidenav-previous uk-slider-item="previous"></a></div>
         <div class="uk-width-expand uk-slider-container">
           <ul class="uk-slider-items uk-child-width-1-3 uk-child-width-1-6@s uk-grid uk-grid-large">
              <?php foreach ($top_brends as $data): ?>
             <li>
               <div class="tm-ratio tm-ratio-16-9"><a class="uk-link-muted tm-media-box tm-grayscale" href="<?=Url::to(['/brand/view', 'slug'=>$data['slug']])?>" title="<?=$data['title']?>">
                   <figure class="tm-media-box-wrap"><img src="<?=$data['logo']?>" alt="Apple"></figure></a></div>
             </li>
             <?php endforeach;?>
           </ul>
         </div>
         <div class="uk-visible@m"><a href="#" uk-slider-item="next" uk-slidenav-next></a></div>
       </div>
     </div>
     <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin-medium-top uk-hidden@m"></ul>
   </div>
   <div class="uk-margin uk-text-center"><a class="uk-link-muted uk-text-uppercase tm-link-to-all" href="<?=Url::to(['/brand/index'])?>"><span><?=Yii::t('frontend', 'all_brand')?></span><span uk-icon="icon: chevron-right; ratio: .75;"></span></a>
   </div>
 </div>
</section>
<?php endif;?>
<?php if ($last_news):?>
  <section class="uk-section uk-section-small">
    <div class="uk-container">
      <h2 class="uk-text-center"><?=Yii::t('frontend', 'last_news')?></h2>
      <div class="uk-grid-medium uk-grid-match uk-child-width-1-1 uk-child-width-1-2@s" uk-grid>
        <?php foreach ($last_news as $data): ?>
        <div><a href="<?=Url::to(['/news/view', 'slug'=>$data['slug']])?>">
            <article class="uk-card uk-card-default uk-card-small uk-article uk-overflow-hidden uk-box-shadow-hover-large uk-height-1-1 tm-ignore-container">
              <div class="tm-ratio tm-ratio-16-9">
                <figure class="tm-media-box uk-cover-container uk-margin-remove"><img src="<?=$this->getListImage($data)?>" uk-cover="uk-cover"/></figure>
              </div>

              <div class="uk-card-body">
                <div class="uk-article-body">
                  <div class="uk-article-meta uk-margin-xsmall-bottom">

                  </div>
                  <div>
                    <h3 class="uk-margin-remove"><?=$data['title']?></h3>
                  </div>
                  <div class="uk-margin-small-top"><p><?=$data['description']?></p></div>
                </div>
              </div>

            </article></a>
        </div>
        <?php endforeach;?>
      </div>
    </div>
  </section>
<?php endif;?>
