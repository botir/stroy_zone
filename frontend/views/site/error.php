<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var string $name
 * @var string $message
 * @var Exception $exception
 */
 $this->settings = $settings;
 $this->categories = $categories;
 $this->brands = $brands;
 $this->menupage = $menupage;
$this->title = $name;
?>

<section class="uk-section uk-section-small">
  <div class="uk-container">
    <div class="uk-text-center">
      <h1 class="uk-heading-hero">404</h1>
      <div class="uk-text-lead"></span> <?php echo nl2br(Html::encode($message)) ?></div>
      <div class="uk-margin-top"><?php echo Yii::t('frontend', 'Oops! There was an error...') ?><br><a href="/">Go back to Homepage</a></div>
    </div>
  </div>
</section>
