<?php
/**
 * @var yii\web\View $this
 */
 use yii\helpers\Url;
 use yii\helpers\Json;

$this->settings = $settings;
$this->categories = $categories;
$this->brands = $brands;
$this->menupage = $menupage;
?>
<section class="uk-section uk-section-small">
  <div class="uk-container">
    <div class="uk-grid-medium uk-child-width-1-1" uk-grid>

      <div class="uk-text-center">
        <ul class="uk-breadcrumb uk-flex-center uk-margin-remove">
          <li><a href="<?=Url::to(['/site/index'])?>"><?=Yii::t('frontend', 'to_home')?></a></li>
          <li><a href="<?=Url::to(['/brand/index'])?>"><?=Yii::t('frontend', 'Brands')?></a></li>
          <li><span><?=$brand['title']?></span></li>
        </ul>
        <h1 class="uk-margin-small-top uk-margin-remove-bottom"><?=$brand['title']?></h1>
        <div class="uk-text-meta uk-margin-xsmall-top"><?=$count_products?> <?=Yii::t('frontend', 'count_product')?></div>
      </div>
      <div>
        <div class="uk-grid-medium" uk-grid>
          <aside class="uk-width-1-4 tm-aside-column tm-filters" id="filters" uk-offcanvas="overlay: true; container: false;">
              <div class="uk-offcanvas-bar uk-padding-remove">
                <div class="uk-card uk-card-default uk-card-small uk-flex uk-flex-column uk-height-1-1">
                  <header class="uk-card-header uk-flex uk-flex-middle">
                    <div class="uk-grid-small uk-flex-1" uk-grid>
                      <div class="uk-width-expand">
                        <div class="uk-h3">Фильтры</div>
                      </div>
                      <button class="uk-offcanvas-close" type="button" uk-close></button>
                    </div>
                  </header>
                  <div class="uk-margin-remove uk-flex-1 uk-overflow-auto" uk-accordion="multiple: true; targets: &gt; .js-accordion-section" style="flex-basis: auto">

                    <?php if ($brand_categories):?>
                    <section class="uk-card-small uk-card-body">
                      <h4 class="uk-margin-small-bottom">Категории</h4>
                      <ul class="uk-nav uk-nav-default">
                        <?php foreach ($brand_categories as $data): ?>
                        <li><a href="<?=Url::to(['/category/view', 'slug'=>$data['slug']])?>"><?=$data['title']?></a></li>
                        <?php endforeach; ?>
                      </ul>
                    </section>
                    <?php endif; ?>
                    <form method="get">
                    <section class="uk-card-body uk-open js-accordion-section">
                      <h4 class="uk-accordion-title uk-margin-remove">Цены</h4>
                      <div class="uk-accordion-content">
                        <div class="uk-grid-small uk-child-width-1-2 uk-text-small" uk-grid>
                          <div>
                            <div class="uk-inline"><span class="uk-form-icon uk-text-xsmall">от</span>
                              <input class="uk-input" name="pfrom" type="text" value="<?=$pfrom?>">
                            </div>
                          </div>
                          <div>
                            <div class="uk-inline"><span class="uk-form-icon uk-text-xsmall">до</span>
                              <input class="uk-input" name="pto" type="text" value="<?=$pto?>">
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>


                    <div class="uk-card-body">
                      <button type="submit" class="uk-button uk-button-default uk-width-1-1">
                        фильтр
                      </button>
                    </div>
                  </form>
                  </div>
                </div>
              </div>
            </aside>
            <div class="uk-width-expand">
              <?php if ($products):?>
                <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
                  <div>
                    <div class="uk-card uk-card-default uk-card-small tm-ignore-container">
                      <div class="uk-grid-collapse uk-child-width-1-1" id="products" uk-grid>
                        <div class="uk-card-header">
                          <div class="uk-grid-small uk-flex-middle" uk-grid>
                            <div class="uk-width-1-1 uk-width-expand@s uk-flex uk-flex-center uk-flex-left@s uk-text-small"><span class="uk-margin-small-right uk-text-muted"><?=Yii::t('frontend', 'order_list')?></span>
                              <ul class="uk-subnav uk-margin-remove">

                                <li><a class="uk-text-lowercase" href="<?=Url::current(['order' => 'price'])?>"><?=Yii::t('frontend', 'order_price')?></a></li>
                                <li><a class="uk-text-lowercase" href="<?=Url::current(['order' => 'new'])?>"><?=Yii::t('frontend', 'order_new')?></a></li>
                              </ul>
                            </div>
                            <div class="uk-width-1-1 uk-width-auto@s uk-flex uk-flex-center uk-flex-middle">
                              <button class="uk-button uk-button-default uk-button-small uk-hidden@m" uk-toggle="target: #filters"><span class="uk-margin-xsmall-right" uk-icon="icon: settings; ratio: .75;"></span>Filters
                              </button>
                              <div class="tm-change-view uk-margin-small-left">
                                <ul class="uk-subnav uk-iconnav js-change-view" uk-switcher>
                                  <li><a class="uk-active" data-view="grid" uk-icon="grid" uk-tooltip="Сетка"></a></li>
                                  <li><a data-view="list" uk-icon="list" uk-tooltip="Списка"></a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div>
                          <div class="uk-grid-collapse uk-child-width-1-3 tm-products-grid js-products-grid" uk-grid>

                              <?php foreach ($products as $data): ?>
                            <article class="tm-product-card">
                              <div class="tm-product-card-media">
                                <div class="tm-ratio tm-ratio-4-3"><a class="tm-media-box" href="<?=Url::to(['/product/view', 'slug'=>$data['slug']])?>">
                                    <div class="tm-product-card-labels">
                                      <?php if ($data['top_selling']): ?>
                                        <span class="uk-label uk-label-warning"><?=Yii::t('frontend', 'topbuy_product')?></span>
                                      <?php endif;?>
                                      <?php if ($data['new_product']): ?>
                                      <span class="uk-label uk-label-success"><?=Yii::t('frontend', 'new_product')?></span>
                                      <?php endif;?>
                                    </div>
                                    <figure class="tm-media-box-wrap"><img src="<?=$this->getListImage($data)?>" />
                                    </figure></a></div>
                              </div>
                              <div class="tm-product-card-body">
                                <div class="tm-product-card-info">
                                  <div class="uk-text-meta uk-margin-xsmall-bottom"><?=$data['category_title']?></div>
                                  <h3 class="tm-product-card-title"><a class="uk-link-heading" href="<?=Url::to(['/product/view', 'slug'=>$data['slug']])?>"><?=$data['title']?></a></h3>

                                </div>
                                <div class="tm-product-card-shop">
                                  <div class="tm-product-card-prices">
                                    <div class="tm-product-card-price"><?=$this->numFormat($data['price'])?></div>
                                  </div>
                                  <div class="tm-product-card-add">

                                    <button class="uk-button uk-button-primary tm-product-card-add-button tm-shine js-add-to-cart" data-id="<?=$data['id']?>"><span class="tm-product-card-add-button-icon" uk-icon="cart"></span><span class="tm-product-card-add-button-text"><?=Yii::t('frontend', 'add_cart')?></span>
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </article>
                            <?php endforeach; ?>


                          </div>
                        </div>
                        <div>

                        </div>
                      </div>
                    </div>
                  </div>
                  <div>
                    <?php echo \frontend\components\CustomPager::widget([
                      'pagination'=>$pagination,
                      'pageLabel'     => False,
                      'nextPageLabel' => '<span uk-pagination-next></span>',
                      'prevPageLabel' => '',
                      'options'=>[
                      	'class'=>'uk-pagination uk-flex-center'
                      ]
                  ]);?>

                  </div>
                </div>
                  <?php else:?>
                    <section class="uk-section uk-section-small">
                      <div class="uk-container">
                        <div class="uk-text-center">

                          <div class="uk-text-lead">Продуктов нет</div>

                        </div>
                      </div>
                    </section>
                    <?php endif; ?>
              </div>


        </div>
      </div>

    </div>
  </div>
</section>
