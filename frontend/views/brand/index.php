<?php
/**
 * @var yii\web\View $this
 */
 use yii\helpers\Url;
 use yii\helpers\Json;

$this->settings = $settings;
$this->categories = $categories;
$this->brands = $brands;
$this->menupage = $menupage;
?>
<section class="uk-section uk-section-small">
  <div class="uk-container">
    <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
      <div class="uk-text-center">
        <ul class="uk-breadcrumb uk-flex-center uk-margin-remove">
          <li><a href="<?=Url::to(['/site/index'])?>"><?=Yii::t('frontend', 'to_home')?></a></li>
          <li><span><?=Yii::t('frontend', 'Brands')?></span></li>
        </ul>
        <h1 class="uk-margin-small-top uk-margin-remove-bottom"><?=Yii::t('frontend', 'Brands')?></h1>
      </div>
      <div>
        <div class="uk-card uk-card-default tm-ignore-container">
          <?php if ($letters): ?>

          <header class="uk-card-header uk-background-default">
            <nav>
              <ul class="uk-subnav uk-flex-center uk-margin-remove">
                <?php foreach ($letters as $key=>$data): ?>

                <li><a href="#<?=$key?>"><?=$key?></a></li>

                <?php endforeach; ?>
              </ul>
            </nav>
          </header>
          <?php foreach ($letters as $key=>$data): ?>
            <section class="uk-card-body" id="<?=$key?>">
              <div uk-grid>
                <div class="uk-width-1-1 uk-width-1-6@m">
                  <h2 class="uk-text-center"><?=$key?></h2>
                </div>
                <div class="uk-width-1-1 uk-width-expand@m">
                  <ul class="uk-grid-small uk-child-width-1-2 uk-child-width-1-4@s uk-child-width-1-5@m" uk-grid>
              <?php foreach ($data as $brand): ?>
                <li><a class="uk-link-muted uk-text-center uk-display-block uk-padding-small uk-box-shadow-hover-large" href="<?=Url::to(['/brand/view', 'slug'=>$brand['slug']])?>">
                    <div class="tm-ratio tm-ratio-4-3">
                      <div class="tm-media-box">
                        <figure class="tm-media-box-wrap"><img class="item-brand" src="<?=$brand['logo']?>"></figure>
                      </div>
                    </div>
                    <div class="uk-margin-small-top uk-text-truncate"><?=$brand['title']?></div></a></li>
              <?php endforeach; ?>
              </ul>
              </div>
            </div>
            </section>
          <?php endforeach; ?>

          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
