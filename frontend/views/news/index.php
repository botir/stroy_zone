<?php
/**
 * @var yii\web\View $this
 */
 use yii\helpers\Url;
 use yii\helpers\Json;

$this->settings = $settings;
$this->categories = $categories;
$this->brands = $brands;
$this->menupage = $menupage;
?>
<section class="uk-section uk-section-small">
  <div class="uk-container">
    <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
      <div class="uk-text-center">
        <ul class="uk-breadcrumb uk-flex-center uk-margin-remove">
          <li><a href="<?=Url::to(['/site/index'])?>"><?=Yii::t('frontend', 'to_home')?></a></li>
          <li><span><?=Yii::t('frontend', 'News')?></span></li>
        </ul>
        <h1 class="uk-margin-small-top uk-margin-remove-bottom"><?=Yii::t('frontend', 'News')?></h1>
      </div>
      <div>
        <div class="uk-grid-medium" uk-grid>
          <section class="uk-width-1-1 uk-width-expand@m">
            <section class="uk-card uk-card-default uk-card-small uk-card-body tm-ignore-container">
              <?php if ($news): ?>
              <ul class="uk-list uk-list-large uk-list-divider">

                <?php foreach ($news as $data): ?>
                <li>
                  <article class="uk-article">
                    <div class="uk-article-body">
                      <div class="uk-article-meta uk-margin-xsmall-bottom">
                        <time><?=$this->getPublishDate($data['created_at'])?></time>
                      </div>
                      <div>
                        <h3><a class="uk-link-heading" href="<?=Url::to(['/news/view', 'slug'=>$data['slug']])?>"><?=$data['title']?></a></h3>
                      </div>
                      <div class="uk-margin-small-top"><p><?=$data['description']?></p></div>
                    </div>
                  </article>
                </li>
                <?php endforeach; ?>
              </ul>
              <?php endif; ?>
            </section>
          </section>
          <aside class="uk-width-1-4 uk-visible@m tm-aside-column">
            <section class="uk-card uk-card-default uk-card-small" uk-sticky="offset: 90; bottom: true;">
              <nav>
                <ul class="uk-nav uk-nav-default tm-nav">
                  <li><a href="<?=Url::to(['/contact/index'])?>"><?=Yii::t('frontend', 'Contacts')?></a></li>
                  <li class="uk-active"><a href="<?=Url::to(['/news/index'])?>"><?=Yii::t('frontend', 'News')?></a></li>
                  <li><a href="<?=Url::to(['/page/view', 'slug'=>'delivery'])?>"><?=Yii::t('frontend', 'Delivery')?></a></li>
                </ul>
              </nav>
            </section>
          </aside>
        </div>
      </div>
    </div>
  </div>
</section>
