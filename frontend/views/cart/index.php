<?php
/**
 * @var yii\web\View $this
 */
 use yii\helpers\Url;
 use yii\helpers\Json;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->settings = $settings;
$this->categories = $categories;
$this->brands = $brands;
$this->menupage = $menupage;
?>
<section class="uk-section uk-section-small">
  <div class="uk-container">
    <div class="uk-grid-medium uk-child-width-1-1" uk-grid>
      <div class="uk-text-center">
        <ul class="uk-breadcrumb uk-flex-center uk-margin-remove">
          <li><a href="<?=Url::to(['/site/index'])?>"><?=Yii::t('frontend', 'to_home')?></a></li>
          <li><span><?=Yii::t('frontend', 'my_cart')?></span></li>
        </ul>
        <h1 class="uk-margin-small-top uk-margin-remove-bottom"><?=Yii::t('frontend', 'my_cart')?></h1>
      </div>
      <div>
        <div class="uk-grid-medium" uk-grid>
          <div class="uk-width-1-1 uk-width-expand@m">
            <div class="uk-card uk-card-default uk-card-small tm-ignore-container">
              <header class="uk-card-header uk-text-uppercase uk-text-muted uk-text-center uk-text-small uk-visible@m">
                <div class="uk-grid-small uk-child-width-1-2" uk-grid>
                  <div><?=Yii::t('frontend', 'cart_item_name')?></div>
                  <div>
                    <div class="uk-grid-small uk-child-width-expand" uk-grid>
                      <div><?=Yii::t('frontend', 'cart_item_price')?></div>
                      <div class="tm-quantity-column"><?=Yii::t('frontend', 'cart_item_quant')?></div>
                      <div><?=Yii::t('frontend', 'cart_item_total')?></div>
                      <div class="uk-width-auto">
                        <div style="width: 20px;"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </header>
              <?php if ($result && $result['products'] && count($result['products']) > 0):?>
              <div class="uk-card-body">
                <div class="uk-grid-small uk-child-width-1-1 uk-child-width-1-2@m uk-flex-middle" uk-grid>

                      <?php foreach ($result['products'] as $data): ?>
                        <div>
                          <div class="uk-grid-small" uk-grid>
                            <div class="uk-width-1-3">
                              <div class="tm-ratio tm-ratio-4-3"><a class="tm-media-box" href="<?=$data['url']?>">
                                  <figure class="tm-media-box-wrap"><img src="<?=$data['image']?>" />
                                  </figure></a></div>
                            </div>
                            <div class="uk-width-expand">
                              <div class="uk-text-meta"><?=$data['category_title']?></div><a class="uk-link-heading" href="<?=$data['url']?>"><?=$data['title']?></a>
                            </div>
                          </div>
                        </div>


                  <!-- Product cell-->

                  <!-- Other cells-->
                  <div>
                    <div class="uk-grid-small uk-child-width-1-1 uk-child-width-expand@s uk-text-center" uk-grid>
                      <div>
                        <div class="uk-text-muted uk-hidden@m">Price</div>
                        <div><?=$this->numFormat($data['price'])?></div>
                      </div>
                      <div class="tm-cart-quantity-column"><a onclick="increment(-1, 'product-<?=$data['id']?>', <?=$data['price_num']?>, 'сум')" uk-icon="icon: minus; ratio: .75"></a>
                        <input class="uk-input tm-quantity-input" id="product-<?=$data['id']?>" type="text" maxlength="3" value="<?=$data['quantity']?>" data-price="<?=$data['price_num']?>" /><a onclick="increment(+1, 'product-<?=$data['id']?>', <?=$data['price_num']?>, 'сум')" uk-icon="icon: plus; ratio: .75"></a>
                      </div>
                      <div>
                        <div class="uk-text-muted uk-hidden@m">Sum</div>
                        <div id="total-product-<?=$data['id']?>"><?=$data['total_price']?></div>
                      </div>
                      <div class="uk-width-auto@s"><a class="uk-text-danger" uk-tooltip="Remove" onclick="removeCart(<?=$data['id']?>, 2);"><span uk-icon="close"></span></a></div>
                    </div>
                  </div>

                <?php endforeach; ?>

                </div>
              </div>
              <?php endif;?>
            </div>
          </div>
          <div class="uk-width-1-1 tm-aside-column uk-width-1-4@m">
            <div class="uk-card uk-card-default uk-card-small tm-ignore-container" uk-sticky="offset: 30; bottom: true; media: @m;">
              <div class="uk-card-body">
                <div class="uk-grid-small uk-flex-middle" uk-grid>
                  <div class="uk-width-expand uk-text-muted"><?=Yii::t('frontend', 'checkout_total')?></div>
                  <div class="uk-text-lead uk-text-bolder" id="total-price"><?=$result['total']?></div>
                </div><a class="uk-button uk-button-primary uk-margin-small uk-width-1-1" href="<?=Url::to(['/order/checkout'])?>"><?=Yii::t('frontend', 'view_checkout')?></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
