<?php
namespace common\components;

use Yii;
use yii\helpers\Url;
use yii\helpers\Json;

class View extends \yii\web\View
{    
    public function getListImage($data){
        return $data['base_url'].'/thumbnails/_medium/'.$data['image_name'].'_medium.'.$data['image_ext'];
    }

    public static function numFormat($price)
    {
        $format = number_format($price, 0, ' ', ' ');
        return $format. ' сум';
    }

    public function getPublishDate($date,$type=1)
    {
      $time = strtotime($date);
      $now = date('Y-m-d');
      if ($now==date('Y-m-d',$time)){
          return date("H:i",$time);
      }else{
        return date("H:i / d.m.Y",$time);
      }
    }



}
