<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "geo_region".
 *
 * @property int $id
 * @property string $name_uz
 * @property string $name_ru
 * @property int $country_id
 *
 * @property GeoCountry $country
 * @property GeoDistrict[] $geoDistricts
 */
class GeoRegion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'geo_region';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_uz', 'name_ru', 'country_id'], 'required'],
            [['country_id'], 'default', 'value' => null],
            [['country_id'], 'integer'],
            [['name_uz', 'name_ru'], 'string', 'max' => 100],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => GeoCountry::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_uz' => 'Name Uz',
            'name_ru' => 'Name Ru',
            'country_id' => 'Country ID',
        ];
    }

    /**
     * Gets query for [[Country]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(GeoCountry::className(), ['id' => 'country_id']);
    }

    /**
     * Gets query for [[GeoDistricts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGeoDistricts()
    {
        return $this->hasMany(GeoDistrict::className(), ['region_id' => 'id']);
    }

    public static  function getListData()
    {
        $list = self::find()->where(['country_id'=>239])->all();
        $result = [];
        foreach ($list as $data) {
            $result[$data->id] = $data->name_ru;
        }
        return $result;
    }
}
