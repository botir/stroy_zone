<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "geo_district".
 *
 * @property int $id
 * @property string $name_uz
 * @property string $name_ru
 * @property int $country_id
 * @property int $region_id
 *
 * @property GeoCountry $country
 * @property GeoRegion $region
 */
class GeoDistrict extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'geo_district';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_uz', 'name_ru', 'country_id', 'region_id'], 'required'],
            [['country_id', 'region_id'], 'default', 'value' => null],
            [['country_id', 'region_id'], 'integer'],
            [['name_uz', 'name_ru'], 'string', 'max' => 100],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => GeoCountry::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => GeoRegion::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_uz' => 'Name Uz',
            'name_ru' => 'Name Ru',
            'country_id' => 'Country ID',
            'region_id' => 'Region ID',
        ];
    }

    /**
     * Gets query for [[Country]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(GeoCountry::className(), ['id' => 'country_id']);
    }

    /**
     * Gets query for [[Region]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(GeoRegion::className(), ['id' => 'region_id']);
    }

    public static  function getListDataFromRegion($region_id)
    {
        $list = self::find()->where(['region_id'=>$region_id])->all();
        $result = [];
        foreach ($list as $data) {
            $result[$data->id] = $data->name_ru;

        }
        return $result;
    }
}
