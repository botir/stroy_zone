<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "geo_country".
 *
 * @property int $id
 * @property string|null $iso_code
 * @property string|null $iso_code3
 * @property string $name_uz
 * @property string $name_ru
 * @property float|null $lat
 * @property float|null $lng
 *
 * @property GeoDistrict[] $geoDistricts
 * @property GeoRegion[] $geoRegions
 */
class GeoCountry extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'geo_country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_uz', 'name_ru'], 'required'],
            [['lat', 'lng'], 'number'],
            [['iso_code'], 'string', 'max' => 2],
            [['iso_code3'], 'string', 'max' => 3],
            [['name_uz', 'name_ru'], 'string', 'max' => 100],
            [['iso_code3'], 'unique'],
            [['iso_code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'iso_code' => 'Iso Code',
            'iso_code3' => 'Iso Code3',
            'name_uz' => 'Name Uz',
            'name_ru' => 'Name Ru',
            'lat' => 'Lat',
            'lng' => 'Lng',
        ];
    }

    /**
     * Gets query for [[GeoDistricts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGeoDistricts()
    {
        return $this->hasMany(GeoDistrict::className(), ['country_id' => 'id']);
    }

    /**
     * Gets query for [[GeoRegions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGeoRegions()
    {
        return $this->hasMany(GeoRegion::className(), ['country_id' => 'id']);
    }
}
