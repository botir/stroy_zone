<?php

namespace common\models;

use Yii;
use trntv\filekit\behaviors\UploadBehavior;
/**
 * This is the model class for table "system_settings".
 *
 * @property int $id
 * @property string|null $store_name
 * @property string|null $phone_number
 * @property string $email_main
 * @property string $email_support
 * @property string|null $address
 * @property string|null $landmark
 * @property string|null $schedule
 * @property string|null $maps_address
 * @property string|null $maps_lat
 * @property string|null $maps_lon
 * @property string|null $fb_link
 * @property string|null $ints_link
 * @property string|null $tw_link
 * @property string|null $you_link
 * @property string|null $logo1_url
 * @property string|null $logo1_path
 * @property string|null $logo2_url
 * @property string|null $logo2_path
 * @property string|null $maps_iframe
 */
class SystemSettings extends \yii\db\ActiveRecord
{
    public $thumbnail1;
    public $thumbnail2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'system_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email_main', 'email_support'], 'required'],
            [['store_name'], 'string', 'max' => 100],
            [['phone_number'], 'string', 'max' => 30],
            [['thumbnail1', 'thumbnail2', 'maps_iframe'], 'safe'],
            [['email_main', 'email_support', 'address', 'landmark', 'schedule', 'maps_address'], 'string', 'max' => 255],
            [['maps_lat', 'maps_lon', 'fb_link', 'ints_link', 'tw_link', 'you_link'], 'string', 'max' => 50],
            [['logo1_url', 'logo1_path', 'logo2_url', 'logo2_path'], 'string', 'max' => 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'store_name' => 'Название магазина',
            'phone_number' => 'Номер телефона',
            'email_main' => 'E-mail',
            'email_support' => 'Email тех поддержка',
            'address' => 'Адрес',
            'landmark' => 'Ориентир',
            'schedule' => 'Режим работы',
            'maps_address' => 'Локация',
            'maps_lat' => 'Координаты долгота',
            'maps_lon' => 'Координаты широта',
            'fb_link' => 'Ссылка фейсбук',
            'ints_link' => 'Ссылка инстаграм',
            'tw_link' => 'Ссылка твиттера',
            'you_link' => 'Ссылка ютуб',
            'logo1_url' => 'Logo1 Url',
            'logo1_path' => 'Logo1 Path',
            'logo2_url' => 'Logo2 Url',
            'logo2_path' => 'Logo2 Path',
            'thumbnail1' => 'Логотип 1',
            'thumbnail2' => 'Логотип 2',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::class,
                'attribute' => 'thumbnail1',
                'pathAttribute' => 'logo1_path',
                'baseUrlAttribute' => 'logo1_url',
            ],
            [
                'class' => UploadBehavior::class,
                'attribute' => 'thumbnail2',
                'pathAttribute' => 'logo2_path',
                'baseUrlAttribute' => 'logo2_url',
            ],
        ];
    }

}
