<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use common\models\behaviors\CategoryBehavior;
use yii\helpers\Json;
use trntv\filekit\behaviors\UploadBehavior;

/**
 * This is the model class for table "catalog_category".
 *
 * @property int $id
 * @property string $content_txt
 * @property string|null $slug
 * @property int|null $parent_id
 * @property int|null $sort_order
 * @property int $status
 * @property int $is_main
 * @property string|null $background_image
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CatalogCategory[] $catalogCategories
 * @property CatalogProduct[] $catalogProducts
 * @property CatalogCategory $parent
 */
class CatalogCategory extends \yii\db\ActiveRecord
{

    public $icon;
    public $title_uz;
    public $title_ru;
    public $description_uz;
    public $description_ru;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'catalog_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ru', 'description_ru'], 'required'],
            [['content_txt', 'created_at', 'updated_at'], 'safe'],
            [['sort_order'], 'default', 'value' => 1],
            [['status'], 'default', 'value' => 2],
            [['parent_id', 'sort_order', 'status'], 'integer'],
            [['slug'], 'string', 'max' => 200],
            [['icon', 'is_main'], 'safe'],
            [['is_main'], 'boolean'],
            [['title_uz', 'title_ru', 'description_uz', 'description_ru'], 'string', 'max' => 200],
            [['background_image'], 'string', 'max' => 100],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatalogCategory::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['icon_path', 'icon_url'], 'string', 'max' => 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content_txt' => 'Content Txt',
            'title_uz' => 'Название',
            'title_ru' => 'Название',
            'description_uz' => 'Описание',
            'description_ru' => 'Описание',
            'slug' => 'ЧПУ',
            'parent_id' => 'Родител',
            'sort_order' => 'Сортировка',
            'status' => 'Статус',
            'background_image' => 'Фото',
            'created_at' => 'Создано',
            'updated_at' => 'Последнее обновление',
            'is_main' => 'На главный экран',
            'icon' => 'Фото',
        ];
    }

    /**
     * Gets query for [[CatalogCategories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogCategories()
    {
        return $this->hasMany(CatalogCategory::className(), ['parent_id' => 'id']);
    }

    /**
     * Gets query for [[CatalogProducts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogProducts()
    {
        return $this->hasMany(CatalogProduct::className(), ['category_id' => 'id']);
    }

    /**
     * Gets query for [[Parent]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(CatalogCategory::className(), ['id' => 'parent_id']);
    }
    /**
 * @inheritdoc
 */
  public function behaviors()
  {
      return [
          [
              'class'=>TimestampBehavior::className(),
              'createdAtAttribute' => 'created_at',
              'updatedAtAttribute' => 'updated_at',
              'value' => date('Y-m-d H:i:s'),
          ],
          [
              'class' => UploadBehavior::class,
              'attribute' => 'icon',
              'pathAttribute' => 'icon_path',
              'baseUrlAttribute' => 'icon_url',
          ],
          [
              'class'=>CategoryBehavior::className()
          ],
          [
              'class' => SluggableBehavior::class,
              'attribute' => 'title_ru',
              'immutable' => true,
          ]
      ];
  }

    public function getListData()
    {
        $categories = self::find()->where(['status' => 2])->all();
        $list = [];

        foreach ($categories as $category) {
          if ($category->id == $this->id){
              continue;
          }

          $list[$category->id] = $category->title_ru;
        }

        return $list;
    }

    public static function getTreeCategories()
    {

        $categories = \Yii::$app->db->createCommand(
          "select catalog_category.id, catalog_category.content_txt , childs
from catalog_category
LEFT JOIN ( SELECT catalog_category.parent_id,
            array_to_json(array_agg(row_to_json(catalog_category.*))) AS childs
           FROM catalog_category
          GROUP BY catalog_category.parent_id) childs ON catalog_category.id = childs.parent_id
where catalog_category.parent_id is null
limit 50"
          )->queryAll();
        $list = [];

        foreach ($categories as $category) {
          $array_data = Json::decode($category['content_txt']);
          $list[$category['id']] = $array_data['title_ru'];
          $childs =  Json::decode($category['childs']);
          if ($childs && is_array($childs)){
            foreach ($childs as $child) {
              $list[$child['id']] = '--- '.$child['content_txt']['title_ru'];
            }
          }

        }

        return $list;
    }

    public static function getMainList()
    {
      $result = Yii::$app->db->createCommand('SELECT id, parent_id, slug,
        content_txt->>\'title_ru\' as title, content_txt->>\'description_ru\' as description, coalesce(icon_url, \'\')||coalesce(icon_path, \'\') AS "icon"
      FROM "catalog_category" where is_main is true order by created_at asc limit 6')->queryAll();

    return $result;
    }

    public static function getOneCategory($slug)
    {
      $result = Yii::$app->db->createCommand('SELECT id, parent_id, slug,
        content_txt->>\'title_ru\' as title, content_txt->>\'description_ru\' as description, coalesce(icon_url, \'\')||coalesce(icon_path, \'\') AS "icon"
      FROM "catalog_category" where slug=:slug order by created_at asc limit 1', [':slug'=>$slug])->queryOne();

    return $result;
    }

    public static function getparentChilds($parent_id)
    {
      $result = Yii::$app->db->createCommand('SELECT id, parent_id, slug,
        content_txt->>\'title_ru\' as title, content_txt->>\'description_ru\' as description, coalesce(icon_url, \'\')||coalesce(icon_path, \'\') AS "icon"
      FROM "catalog_category" where parent_id=:parent_id order by created_at asc limit 20', [':parent_id'=>$parent_id])->queryAll();

    return $result;
    }

}
