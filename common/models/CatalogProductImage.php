<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "catalog_product_image".
 *
 * @property int $id
 * @property int $product_id
 * @property string $file_name
 * @property string $file_path
 * @property string $base_url
 * @property string|null $file_type
 * @property int|null $file_size
 * @property int|null $sort_order
 */
class CatalogProductImage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'catalog_product_image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'file_name', 'file_path', 'base_url'], 'required'],
            [['product_id', 'file_size', 'sort_order'], 'default', 'value' => null],
            [['product_id', 'file_size', 'sort_order'], 'integer'],
            [['file_name', 'file_path', 'base_url'], 'string', 'max' => 1024],
            [['file_type'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'file_name' => 'File Name',
            'file_path' => 'File Path',
            'base_url' => 'Base Url',
            'file_type' => 'File Type',
            'file_size' => 'File Size',
            'sort_order' => 'Sort Order',
        ];
    }
}
