<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use trntv\filekit\behaviors\UploadBehavior;

/**
 * This is the model class for table "catalog_brand".
 *
 * @property int $id
 * @property string $title
 * @property string|null $slug
 * @property string|null $logo_url
 * @property string|null $logo_path
 *
 * @property CatalogProduct[] $catalogProducts
 */
class CatalogBrand extends \yii\db\ActiveRecord
{
     public $thumbnail;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'catalog_brand';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['slug'], 'unique'],
            [['title', 'slug'], 'string', 'max' => 200],
            [['logo_url', 'logo_path'], 'string', 'max' => 1024],
            [['thumbnail'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'slug' => 'ЧПУ',
            'logo_url' => 'Logo Url',
            'logo_path' => 'Logo Path',
            'thumbnail' => 'Логотип',
        ];
    }

    /**
     * Gets query for [[CatalogProducts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogProducts()
    {
        return $this->hasMany(CatalogProduct::className(), ['brand_id' => 'id']);
    }
    /**
   * @inheritdoc
   */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::class,
                'attribute' => 'title',
                'immutable' => true,
            ],
            [
                'class' => UploadBehavior::class,
                'attribute' => 'thumbnail',
                'pathAttribute' => 'logo_path',
                'baseUrlAttribute' => 'logo_url',
            ],
        ];
    }
    public static function getListData()
    {
        $brands = self::find()->all();
        $list = [];

        foreach ($brands as $brand) {

          $list[$brand->id] = $brand->title;
        }

        return $list;
    }

    public static function getTopList()
    {
        $brands = Yii::$app->db->createCommand("select title, slug,  coalesce(logo_url, '')||coalesce(logo_path, '') AS logo
from catalog_brand
inner join  (select brand_id, count(brand_id) as cnt from catalog_product group by brand_id order by cnt desc limit 10) as top_brands on catalog_brand.id = top_brands.brand_id
order by cnt desc
limit 10")->queryAll();

        return $brands;
    }

    public static function getAllList()
    {
        $brands = Yii::$app->db->createCommand("select title, slug,  coalesce(logo_url, '')||coalesce(logo_path, '') AS logo
from catalog_brand
inner join  (select brand_id, count(brand_id) as cnt from catalog_product group by brand_id order by cnt desc limit 10) as top_brands on catalog_brand.id = top_brands.brand_id
order by cnt desc
limit 100")->queryAll();

        return $brands;
    }

    public static function getOneBrand($slug)
    {
      $result = Yii::$app->db->createCommand('select id, title, slug,  coalesce(logo_url, \'\')||coalesce(logo_path, \'\') AS logo from catalog_brand
     where slug=:slug limit 1', [':slug'=>$slug])->queryOne();

    return $result;
    }

}
