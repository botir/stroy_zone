<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "order_order".
 *
 * @property int $id
 * @property string $user_full_name
 * @property string $user_phone
 * @property string|null $user_email
 * @property int $order_code
 * @property string|null $comments
 * @property float $total_amount
 * @property int|null $region_id
 * @property int|null $district_id
 * @property string|null $shipping_address
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property GeoDistrict $district
 * @property OrderLine[] $orderLines
 * @property GeoRegion $region
 */
class OrderOrder extends \yii\db\ActiveRecord
{

  const STATUS_NEW = 1;
  const STATUS_ACCEPTED = 2;
  const STATUS_WAIT_PAYMENT = 3;
  const STATUS_COMPLATED = 4;
  const STATUS_CANCELLED = -1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_full_name', 'user_phone', 'order_code', 'total_amount'], 'required'],
            [['order_code', 'region_id', 'district_id', 'status'], 'default', 'value' => null],
            [['status'], 'default', 'value' => 1],
            [['order_code', 'region_id', 'district_id', 'status'], 'integer'],
            [['total_amount'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_full_name', 'user_email', 'shipping_address'], 'string', 'max' => 254],
            [['user_phone'], 'string', 'max' => 50],
            [['comments'], 'string', 'max' => 1024],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => GeoDistrict::className(), 'targetAttribute' => ['district_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => GeoRegion::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_full_name' => 'ФИО',
            'user_phone' => 'Номер телефона',
            'user_email' => 'E-mail',
            'order_code' => 'Код заказа',
            'comments' => 'Комментарии',
            'total_amount' => 'Итого',
            'region_id' => 'Область',
            'district_id' => 'Регион',
            'shipping_address' => 'Адрес',
            'status' => 'Статус',
            'created_at' => 'Создано',
            'updated_at' => 'Последнее обновление',
        ];
    }

    /**
     * Gets query for [[District]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(GeoDistrict::className(), ['id' => 'district_id']);
    }

    /**
     * Gets query for [[OrderLines]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrderLines()
    {
        return $this->hasMany(OrderLine::className(), ['order_id' => 'id']);
    }

    /**
     * Gets query for [[Region]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(GeoRegion::className(), ['id' => 'region_id']);
    }

    public function getStatusList()
    {
        return [
            self::STATUS_NEW      => 'Новый',
            self::STATUS_ACCEPTED  => 'Принята',
            self::STATUS_WAIT_PAYMENT  => 'Ожидании оплаты',
            self::STATUS_COMPLATED  => 'Исполнен',
            self::STATUS_CANCELLED => 'Отменён',
        ];
    }

    public function getStatus()
    {
        $data = self::getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : 'Неизвестный';
    }


    /**
 * @inheritdoc
 */
  public function behaviors()
  {
      return [
          [
              'class'=>TimestampBehavior::className(),
              'createdAtAttribute' => 'created_at',
              'updatedAtAttribute' => 'updated_at',
              'value' => date('Y-m-d H:i:s'),
          ]

      ];
  }
}
