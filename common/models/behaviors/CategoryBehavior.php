<?php
namespace common\models\behaviors;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\di\Instance;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class CategoryBehavior extends Behavior
{
    /**
     * @var ActiveRecord
     */
    public $owner;


    public function events()
    {
        $events = [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFindSingle',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdateSingle',
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsertSingle',
        ];
        return $events;
    }

    /**
     * @return void
     */
    public function afterFindSingle()
    {

        $owner_id = $this->owner->getAttribute('id');
        $content = $this->owner->content_txt;

        if (isset($content['title_ru']))
            $this->owner->title_uz = strval($content['title_ru']);
        if (isset($content['title_ru']))
            $this->owner->title_ru = strval($content['title_uz']);
        if (isset($content['description_ru']))
            $this->owner->description_uz = strval($content['description_ru']);
        if (isset($content['description_ru']))
            $this->owner->description_ru = strval($content['description_ru']);
    }

    public function beforeInsertSingle()
    {
      $more_option = [
            'title_uz' => $this->owner->title_ru,
            'title_ru' => $this->owner->title_ru,
            'description_uz' => $this->owner->description_ru,
            'description_ru' => $this->owner->description_ru
      ];
      $this->owner->content_txt =  $more_option;
    }

    public function beforeUpdateSingle()
    {
      $more_option = [
            'title_uz' => $this->owner->title_ru,
            'title_ru' => $this->owner->title_ru,
            'description_uz' => $this->owner->description_ru,
            'description_ru' => $this->owner->description_ru
      ];
      $this->owner->content_txt =  $more_option;
    }

}
