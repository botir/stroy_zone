<?php

namespace common\models;

use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "content_news".
 *
 * @property int $id
 * @property string $slug
 * @property string $title_ru
 * @property string $title_uz
 * @property string $description_uz
 * @property string $description_ru
 * @property string $img_url
 * @property string $img_path
 * @property string $content_ru
 * @property string $content_uz
 * @property bool $is_active
 * @property string $created_at
 */
class ContentNews extends \yii\db\ActiveRecord
{

    public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'content_news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ru', 'description_ru', 'image', 'content_ru', ], 'required'],
            [['content_ru', 'content_uz'], 'string'],
            [['is_active'], 'boolean'],
            [['created_at'], 'safe'],
            [['image'], 'safe'],
            [['slug', 'title_ru', 'title_uz'], 'string', 'max' => 255],
            [['description_uz', 'description_ru', 'img_url', 'img_path'], 'string', 'max' => 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'ЧПУ',
            'title_ru' => 'Название',
            'title_uz' => 'Название',
            'description_uz' => 'Описание',
            'description_ru' => 'Описание',
            'img_url' => 'Img Url',
            'img_path' => 'Img Path',
            'content_ru' => 'Контент',
            'content_uz' => 'Контент',
            'is_active' => 'Активно',
            'created_at' => 'Создано',
            'image' => 'Фото',
        ];
    }

    /**
 * @inheritdoc
 */
  public function behaviors()
  {
      return [
          [
              'class'=>TimestampBehavior::className(),
              'createdAtAttribute' => 'created_at',
              'updatedAtAttribute' => null,
              'value' => date('Y-m-d H:i:s'),
          ],
          [
              'class' => UploadBehavior::class,
              'attribute' => 'image',
              'pathAttribute' => 'img_path',
              'baseUrlAttribute' => 'img_url',
          ],
          [
              'class' => SluggableBehavior::class,
              'attribute' => 'title_ru',
              'immutable' => true,
          ]
      ];
  }

  public static function getLastNews()
  {
    $result = Yii::$app->db->createCommand("
    select  title_ru as title,  slug,  description_ru as description,
    LEFT(img_path, POSITION('.' IN img_path)-1) as image_name, right(img_path, POSITION('.' IN REVERSE(img_path)) - 1) as image_ext, img_url as base_url
    from content_news
    where is_active is true
    order by created_at desc
    limit 2")->queryAll();

    return $result;
  }

  public static function getListNews()
  {
    $result = Yii::$app->db->createCommand("
    select  title_ru as title,  slug,  description_ru as description, created_at,
    LEFT(img_path, POSITION('.' IN img_path)-1) as image_name, right(img_path, POSITION('.' IN REVERSE(img_path)) - 1) as image_ext, img_url as base_url
    from content_news
    where is_active is true
    order by created_at desc
    limit 20")->queryAll();

    return $result;
  }

  public static function getOneNews($slug)
  {
    $result = Yii::$app->db->createCommand("
    select  title_ru as title,  slug,  description_ru as description, created_at,content_ru as content_text,
    LEFT(img_path, POSITION('.' IN img_path)-1) as image_name, right(img_path, POSITION('.' IN REVERSE(img_path)) - 1) as image_ext, img_url as base_url
    from content_news
    where is_active is true and slug=:slug
    limit 1", [':slug'=>$slug])->queryOne();

    return $result;
  }

}
