<?php

namespace common\models;

use Yii;
use trntv\filekit\behaviors\UploadBehavior;
/**
 * This is the model class for table "content_slider".
 *
 * @property int $id
 * @property string|null $title_ru
 * @property string|null $title_uz
 * @property string $img_url
 * @property string $img_path
 * @property string|null $link_to
 * @property string|null $bg_color
 */
class ContentSlider extends \yii\db\ActiveRecord
{
  public $thumbnail;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'content_slider';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['img_url', 'img_path'], 'safe'],
            [['title_ru', 'title_uz'], 'string', 'max' => 200],
            [['img_url', 'img_path'], 'string', 'max' => 1024],
            [['link_to'], 'string', 'max' => 100],
            [['bg_color'], 'string', 'max' => 12],
            [['thumbnail'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ru' => 'Название',
            'title_uz' => 'Название',
            'img_url' => 'Img Url',
            'img_path' => 'Img Path',
            'link_to' => 'Ссылка',
            'bg_color' => 'Задний фон цвет',
            'thumbnail' => 'Картинка',
        ];
    }

    /**
   * @inheritdoc
   */
    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::class,
                'attribute' => 'thumbnail',
                'pathAttribute' => 'img_path',
                'baseUrlAttribute' => 'img_url',
            ],
        ];
    }
}
