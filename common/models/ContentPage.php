<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "content_page".
 *
 * @property int $id
 * @property string|null $slug
 * @property string $title_uz
 * @property string $title_ru
 * @property string $content_uz
 * @property string $content_ru
 * @property bool $is_active
 * @property bool $is_menu
 * @property string $created_at
 * @property string $updated_at
 */
class ContentPage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'content_page';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ru','content_ru'], 'required'],
            [['content_uz', 'content_ru'], 'string'],
            [['is_active', 'is_menu'], 'boolean'],
            [['is_active'], 'default', 'value' => true],
            [['is_menu'], 'default', 'value' => false],
            [['created_at', 'updated_at'], 'safe'],
            [['slug', 'title_uz', 'title_ru'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'ЧПУ',
            'title_uz' => 'Название',
            'title_ru' => 'Название',
            'content_uz' => 'Текст',
            'content_ru' => 'Текст',
            'is_active' => 'Активно',
            'is_menu' => 'На меню',
            'created_at' => 'Создано',
            'updated_at' => 'Последнее обновление',
        ];
    }

    /**
   * @inheritdoc
   */
    public function behaviors()
    {
        return [
            [
                'class'=>TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => SluggableBehavior::class,
                'attribute' => 'title_ru',
                'immutable' => true,
            ]
        ];
    }

    public static function getOnePage($slug)
    {
      $result = Yii::$app->db->createCommand('SELECT id, title_ru as title, slug, content_ru as content_text
      FROM "content_page" where slug=:slug limit 1', [':slug'=>$slug])->queryOne();

    return $result;
    }
  }
