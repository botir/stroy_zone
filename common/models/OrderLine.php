<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_line".
 *
 * @property int $id
 * @property string $product_name
 * @property int $quantity
 * @property float $total_amount
 * @property int $price
 * @property int $order_id
 * @property int|null $product_id
 *
 * @property OrderOrder $order
 * @property CatalogProduct $product
 */
class OrderLine extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_line';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_name', 'quantity', 'total_amount', 'price', 'order_id'], 'required'],
            [['quantity', 'price', 'order_id', 'product_id'], 'default', 'value' => null],
            [['quantity', 'price', 'order_id', 'product_id'], 'integer'],
            [['total_amount'], 'number'],
            [['product_name'], 'string', 'max' => 1024],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatalogProduct::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderOrder::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_name' => 'Product Name',
            'quantity' => 'Quantity',
            'total_amount' => 'Total Amount',
            'price' => 'Price',
            'order_id' => 'Order ID',
            'product_id' => 'Product ID',
        ];
    }

    /**
     * Gets query for [[Order]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(OrderOrder::className(), ['id' => 'order_id']);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(CatalogProduct::className(), ['id' => 'product_id']);
    }

    public static function getOrderProducts($order_id)
    {
      $result = Yii::$app->db->createCommand("
      select  order_line.product_name,order_line.quantity,order_line.total_amount, order_line.price,  catalog_product.id,  product_image.product_id,  catalog_product.content_txt->>'title_ru' as title,  catalog_product.price,catalog_product.slug as slug,
      catalog_category.content_txt->>'title_ru' as category_title, catalog_category.slug as category_slug, top_selling, new_product,
      product_image.base_url, product_image.file_path,
      LEFT(product_image.file_path, POSITION('.' IN product_image.file_path)-1) as image_name, right(product_image.file_path, POSITION('.' IN REVERSE(product_image.file_path)) - 1) as image_ext
      from order_line
      inner join catalog_product on order_line.product_id = catalog_product.id
      inner join catalog_category on catalog_product.category_id = catalog_category.id

      left join (
          select * from catalog_product_image
          where id in (
              select min(id) from catalog_product_image group by product_id
          )
      ) as product_image on catalog_product.id = product_image.product_id


      where order_id=:order_id
      order by catalog_product.created_at desc
      limit 100", [':order_id'=>$order_id])->queryAll();

    return $result;
    }

}
