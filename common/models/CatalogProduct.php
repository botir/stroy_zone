<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use trntv\filekit\behaviors\UploadBehavior;
use common\models\behaviors\CategoryBehavior;
use yii\data\Pagination;
use yii\db\Query;

/**
 * This is the model class for table "catalog_product".
 *
 * @property int $id
 * @property string $content_txt
 * @property string|null $slug
 * @property int|null $category_id
 * @property int|null $brand_id
 * @property int|null $model_id
 * @property int $status
 * @property int $top_selling
 * @property int $new_product
 * @property int $is_tranding
 * @property int|null $price
 * @property string|null $search_vector
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CatalogBrand $brand
 * @property CatalogCategory $category
 * @property CatalogModel $model
 */
class CatalogProduct extends \yii\db\ActiveRecord
{
    public $title_uz;
    public $title_ru;
    public $description_uz;
    public $description_ru;

    const STATUS_PUBLISHED = 1;
    const STATUS_DRAFT     = 0;

    /**
   * @var array
   */
  public $attachments;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'catalog_product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ru', 'description_ru'], 'required'],
            [['title_uz', 'title_ru'], 'string', 'max' => 200],
            [['description_uz', 'description_ru'], 'string', 'max' => 1024],

            [['category_id'], 'required'],
            [['brand_id', 'model_id'], 'default', 'value' => null],

            [['content_txt', 'created_at', 'updated_at'], 'safe'],
            [['top_selling', 'new_product', 'is_tranding'], 'safe'],

            [['price'], 'default', 'value' => null],
            [['status'], 'default', 'value' => 2],
            [['category_id', 'brand_id', 'model_id', 'status', 'price'], 'integer'],
            [['search_vector'], 'string'],
             [['attachments'], 'safe'],
            [['slug'], 'string', 'max' => 255],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatalogBrand::className(), 'targetAttribute' => ['brand_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatalogCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatalogModel::className(), 'targetAttribute' => ['model_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_uz' => 'Название',
            'title_ru' => 'Название',
            'description_uz' => 'Описание',
            'description_ru' => 'Описание',
            'content_txt' => 'Content Txt',
            'slug' => 'ЧПУ',
            'category_id' => 'Категория',
            'brand_id' => 'Бренд',
            'model_id' => 'Модель',
            'status' => 'Активно',
            'price' => 'Цена',
            'search_vector' => 'Search Vector',
            'created_at' => 'Создано',
            'updated_at' => 'Последнее обновление',
            'attachments' => 'Картинки продукты',
            'top_selling' => 'Хит продаж',
            'new_product' => 'Новый товар',
            'is_tranding' => 'Популярные',
        ];
    }

    /**
 * @inheritdoc
 */
  public function behaviors()
  {
      return [
          [
              'class'=>TimestampBehavior::className(),
              'createdAtAttribute' => 'created_at',
              'updatedAtAttribute' => 'updated_at',
              'value' => date('Y-m-d H:i:s'),
          ],
          [
              'class'=>CategoryBehavior::className()
          ],
          [
              'class' => SluggableBehavior::class,
              'attribute' => 'title_ru',
              'immutable' => true,
          ],
          [
               'class' => UploadBehavior::class,
               'attribute' => 'attachments',
               'multiple' => true,
               'uploadRelation' => 'images',
               'pathAttribute' => 'file_path',
               'baseUrlAttribute' => 'base_url',
               'orderAttribute' => 'sort_order',
               'typeAttribute' => 'file_type',
               'sizeAttribute' => 'file_size',
               'nameAttribute' => 'file_name',
           ],
      ];
  }
    /**
     * Gets query for [[Brand]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(CatalogBrand::className(), ['id' => 'brand_id']);
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(CatalogCategory::className(), ['id' => 'category_id']);
    }

    /**
     * Gets query for [[Model]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(CatalogModel::className(), ['id' => 'model_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(CatalogProductImage::class, ['product_id' => 'id']);
    }

    /**
   * @return array statuses list
   */
  public static function statuses()
  {
      return [
          self::STATUS_DRAFT => Yii::t('common', 'Draft'),
          self::STATUS_PUBLISHED => Yii::t('common', 'Published'),
      ];
  }

  public static function getTrendProducts()
  {
    $result = Yii::$app->db->createCommand("
    select  catalog_product.id,  product_image.product_id,  catalog_product.content_txt->>'title_ru' as title,  catalog_product.price,catalog_product.slug as slug,
    catalog_category.content_txt->>'title_ru' as category_title, catalog_category.slug as category_slug, top_selling, new_product,
    product_image.base_url, product_image.file_path,
    LEFT(product_image.file_path, POSITION('.' IN product_image.file_path)-1) as image_name, right(product_image.file_path, POSITION('.' IN REVERSE(product_image.file_path)) - 1) as image_ext
    from catalog_product
    inner join catalog_category on catalog_product.category_id = catalog_category.id

    left join (
        select * from catalog_product_image
        where id in (
            select min(id) from catalog_product_image group by product_id
        )
    ) as product_image on catalog_product.id = product_image.product_id


    where catalog_product.status=1 and is_tranding is true
    order by catalog_product.created_at desc
    limit 8")->queryAll();

  return $result;
  }

  public static function getBrends($ids)
  {
    $where='';
    foreach ($ids as $brand) {
      if ($brand === reset( $ids))
        $where='where catalog_product.category_id='.$brand;
      else
        $where=' or catalog_product.category_id='.$brand;
    }
    $result = Yii::$app->db->createCommand("
    select id, slug, title, cnt
  from catalog_brand
  inner join (
  select brand_id, count(brand_id) as cnt
  from catalog_product
  inner join catalog_brand on catalog_product.brand_id = catalog_brand.id
  $where
  group by brand_id
  order by cnt desc
  limit 30
  ) brands on catalog_brand.id= brands.brand_id
  order by cnt desc
  limit 30")->queryAll();

  return $result;
  }

  public static function getCategoryProducts($ids, $order, $selected_brands, $pfrom, $pto)
  {
    $limit = 15;
    $where='';
    foreach ($ids as $brand) {
      if ($brand === reset( $ids))
        $where=' catalog_product.category_id='.$brand;
      else
        $where .=' or catalog_product.category_id='.$brand;
    }
    if ($selected_brands){
      if ($where !='' ){
        $where = $where.' and (';
      }else{
          $where = '(';
      }

      foreach ($selected_brands as $brand_id) {
        if ($brand_id === reset( $selected_brands))
          $where .=' catalog_product.brand_id='.$brand_id;
        else
          $where .=' or catalog_product.brand_id='.$brand_id;
      }
        $where = $where.' )';
    }

    if ($pfrom && $pfrom > 0){
      if ($where !='' ){
        $where = $where.' and catalog_product.price>='.$pfrom;
      }else{
          $where = ' catalog_product.price>='.$pfrom;
      }
    }

    if ($pto && $pto > 0){
      if ($where !='' ){
        $where = $where.' and catalog_product.price<='.$pto;
      }else{
          $where = ' catalog_product.price<='.$pto;
      }
    }



    $query = (new Query())
    ->select('catalog_product.id,  product_image.product_id,  "catalog_product"."content_txt"->>\'title_ru\' as title,  catalog_product.price,catalog_product.slug as slug,
    "catalog_category"."content_txt"->>\'title_ru\' as category_title, catalog_category.slug as category_slug, top_selling, new_product,
    product_image.base_url, product_image.file_path,
    LEFT(product_image.file_path, POSITION(\'.\' IN product_image.file_path)-1) as image_name, right(product_image.file_path, POSITION(\'.\' IN REVERSE(product_image.file_path)) - 1) as image_ext')
    ->from('catalog_product')
    ->innerJoin('catalog_category','catalog_product.category_id = catalog_category.id')
    ->leftJoin('(
        select * from catalog_product_image
        where id in (
            select min(id) from catalog_product_image group by product_id
        )
    ) as product_image','catalog_product.id = product_image.product_id')
    ->where($where);

    $count = Yii::$app->db->createCommand("select  count(1) as cnt
    from catalog_product
    inner join catalog_category on catalog_product.category_id = catalog_category.id where
$where")->queryOne();

    $pages = new Pagination([
          'totalCount' =>$count['cnt'],
          'pageSize' => $limit,
          'pageSizeLimit'=>false,
          'forcePageParam'=>false,
          'pageSizeParam'=>false,
      ]);
      $models = $query->offset($pages->offset)
         ->orderBy($order)
         ->limit($limit)
         ->all();

      return [
          'models'=>$models,
          'pages'=>$pages,
          'count'=>$count['cnt']
      ];

  }


  public static function getBySlug($slug)
  {
    $query = (new Query())
    ->select('catalog_product.id,  "catalog_product"."content_txt"->>\'title_ru\' as title,  catalog_product.price,catalog_product.slug as slug,
    "catalog_category"."content_txt"->>\'title_ru\' as category_title, catalog_category.slug as category_slug, top_selling, new_product,
catalog_brand.id as brand_id, catalog_brand.slug as brand_slug, catalog_brand.title as brand_title, "catalog_product"."content_txt"->>\'description_ru\' as description,
coalesce(catalog_brand.logo_url, \'\')||coalesce(catalog_brand.logo_path, \'\') AS "brand_logo", catalog_category.id as category_id
    ')
    ->from('catalog_product')
    ->innerJoin('catalog_category','catalog_product.category_id = catalog_category.id')
    ->leftJoin('catalog_brand','catalog_product.brand_id = catalog_brand.id')
    ->where(['catalog_product.slug'=>$slug])
    ->one();
      return $query;

  }


  public static function getByID($id)
  {
    $query = (new Query())
    ->select('catalog_product.id,  "catalog_product"."content_txt"->>\'title_ru\' as title,  catalog_product.price,catalog_product.slug as slug,
    "catalog_category"."content_txt"->>\'title_ru\' as category_title, catalog_category.slug as category_slug, top_selling, new_product,
catalog_brand.id as brand_id, catalog_brand.slug as brand_slug, catalog_brand.title as brand_title, "catalog_product"."content_txt"->>\'description_ru\' as description,
coalesce(catalog_brand.logo_url, \'\')||coalesce(catalog_brand.logo_path, \'\') AS "brand_logo", catalog_category.id as category_id
    ')
    ->from('catalog_product')
    ->innerJoin('catalog_category','catalog_product.category_id = catalog_category.id')
    ->leftJoin('catalog_brand','catalog_product.brand_id = catalog_brand.id')
    ->where(['catalog_product.id'=>$id])
    ->one();
      return $query;

  }

  public static function getAllImages($product_id)
  {
    $query = (new Query())
    ->select('base_url, file_path, coalesce(base_url, \'\')||coalesce(file_path, \'\') AS "image",id,
    LEFT(file_path, POSITION(\'.\' IN file_path)-1) as image_name, right(file_path, POSITION(\'.\' IN REVERSE(file_path)) - 1) as image_ext
    ')
    ->from('catalog_product_image')
    ->where(['product_id'=>$product_id])
    ->all();
      return $query;

  }

  public static function getRelatedProducts($category_id, $id)
  {
    $result = Yii::$app->db->createCommand("
    select  catalog_product.id,  product_image.product_id,  catalog_product.content_txt->>'title_ru' as title,  catalog_product.price,catalog_product.slug as slug,
    catalog_category.content_txt->>'title_ru' as category_title, catalog_category.slug as category_slug, top_selling, new_product,
    product_image.base_url, product_image.file_path,
    LEFT(product_image.file_path, POSITION('.' IN product_image.file_path)-1) as image_name, right(product_image.file_path, POSITION('.' IN REVERSE(product_image.file_path)) - 1) as image_ext
    from catalog_product
    inner join catalog_category on catalog_product.category_id = catalog_category.id

    left join (
        select * from catalog_product_image
        where id in (
            select min(id) from catalog_product_image group by product_id
        )
    ) as product_image on catalog_product.id = product_image.product_id


    where catalog_product.status=1 and catalog_product.category_id=:cat_id and catalog_product.id != :id
    order by catalog_product.created_at desc
    limit 8", [':cat_id'=>$category_id, ':id'=>$id])->queryAll();

  return $result;
  }

  public static function getBrendCategories($brand_id)
  {


    $result = Yii::$app->db->createCommand("
    select id, slug, content_txt->>'title_ru' as title
from catalog_category
inner join (
  select category_id
  from catalog_product
  where brand_id = :brand_id
  group by category_id
  limit 30
) cats on catalog_category.id= cats.category_id
  order by title asc
  limit 30", [':brand_id'=>$brand_id])->queryAll();

  return $result;
  }

  public static function getBrandProducts($brand_id, $order, $pfrom, $pto)
  {
    $limit = 15;
    $where='catalog_product.brand_id='.$brand_id;


    if ($pfrom && $pfrom > 0){
      if ($where !='' ){
        $where = $where.' and catalog_product.price>='.$pfrom;
      }else{
          $where = ' catalog_product.price>='.$pfrom;
      }
    }

    if ($pto && $pto > 0){
      if ($where !='' ){
        $where = $where.' and catalog_product.price<='.$pto;
      }else{
          $where = ' catalog_product.price<='.$pto;
      }
    }



    $query = (new Query())
    ->select('catalog_product.id,  product_image.product_id,  "catalog_product"."content_txt"->>\'title_ru\' as title,  catalog_product.price,catalog_product.slug as slug,
    "catalog_category"."content_txt"->>\'title_ru\' as category_title, catalog_category.slug as category_slug, top_selling, new_product,
    product_image.base_url, product_image.file_path,
    LEFT(product_image.file_path, POSITION(\'.\' IN product_image.file_path)-1) as image_name, right(product_image.file_path, POSITION(\'.\' IN REVERSE(product_image.file_path)) - 1) as image_ext')
    ->from('catalog_product')
    ->innerJoin('catalog_category','catalog_product.category_id = catalog_category.id')
    ->leftJoin('(
        select * from catalog_product_image
        where id in (
            select min(id) from catalog_product_image group by product_id
        )
    ) as product_image','catalog_product.id = product_image.product_id')
    ->where($where);

    $count = Yii::$app->db->createCommand("select  count(1) as cnt
    from catalog_product
    inner join catalog_category on catalog_product.category_id = catalog_category.id where
$where")->queryOne();

    $pages = new Pagination([
          'totalCount' =>$count['cnt'],
          'pageSize' => $limit,
          'pageSizeLimit'=>false,
          'forcePageParam'=>false,
          'pageSizeParam'=>false,
      ]);
      $models = $query->offset($pages->offset)
         ->orderBy($order)
         ->limit($limit)
         ->all();

      return [
          'models'=>$models,
          'pages'=>$pages,
          'count'=>$count['cnt']
      ];

  }



  public static function getSearching($q, $pfrom, $pto)
  {
    $limit = 15;
    $query_text='catalog_product.status=1';
    $arrs = explode(" ",$q);
    $iq = 0;


    foreach ($arrs as $value) {
            $one_str = str_replace("'", "%''%", $value);

            if ($one_str && $one_str!=''){
                if ($iq > 0 ){
                    $query_text.= " & ".$one_str.":*";
                }
                else{
                    $query_text .= " and (search_vector @@ tsquery('".$one_str.":*";
                }
                $iq++;
            }
        }
      $query_text.="'))";


    if ($pto && $pto > 0){
      if ($query_text !='' ){
        $query_text = $query_text.' and catalog_product.price<='.$pto;
      }else{
          $query_text = ' catalog_product.price<='.$pto;
      }
    }



    $query = (new Query())
    ->select('catalog_product.id,  product_image.product_id,  "catalog_product"."content_txt"->>\'title_ru\' as title,  catalog_product.price,catalog_product.slug as slug,
    "catalog_category"."content_txt"->>\'title_ru\' as category_title, catalog_category.slug as category_slug, top_selling, new_product,
    product_image.base_url, product_image.file_path,
    LEFT(product_image.file_path, POSITION(\'.\' IN product_image.file_path)-1) as image_name, right(product_image.file_path, POSITION(\'.\' IN REVERSE(product_image.file_path)) - 1) as image_ext')
    ->from('catalog_product')
    ->innerJoin('catalog_category','catalog_product.category_id = catalog_category.id')
    ->leftJoin('(
        select * from catalog_product_image
        where id in (
            select min(id) from catalog_product_image group by product_id
        )
    ) as product_image','catalog_product.id = product_image.product_id')
    ->where($query_text);

    $count = Yii::$app->db->createCommand("select  count(1) as cnt
    from catalog_product
    inner join catalog_category on catalog_product.category_id = catalog_category.id where
$query_text")->queryOne();

    $pages = new Pagination([
          'totalCount' =>$count['cnt'],
          'pageSize' => $limit,
          'pageSizeLimit'=>false,
          'forcePageParam'=>false,
          'pageSizeParam'=>false,
      ]);
      $models = $query->offset($pages->offset)
         // ->orderBy($order)
         ->limit($limit)
         ->all();

      return [
          'models'=>$models,
          'pages'=>$pages,
          'count'=>$count['cnt']
      ];

  }

  public static function getCartProducts($ids)
  {
    $limit = 30;
    $where='';
    foreach ($ids as $cart) {
      if ($cart === reset( $ids))
        $where=' catalog_product.id='.$cart;
      else
        $where.=' or catalog_product.id='.$cart;
    }
    $query = (new Query())
    ->select('catalog_product.id,  product_image.product_id,  "catalog_product"."content_txt"->>\'title_ru\' as title,  catalog_product.price,catalog_product.slug as slug,
    "catalog_category"."content_txt"->>\'title_ru\' as category_title, catalog_category.slug as category_slug, top_selling, new_product,
    product_image.base_url, product_image.file_path,
    LEFT(product_image.file_path, POSITION(\'.\' IN product_image.file_path)-1) as image_name, right(product_image.file_path, POSITION(\'.\' IN REVERSE(product_image.file_path)) - 1) as image_ext')
    ->from('catalog_product')
    ->innerJoin('catalog_category','catalog_product.category_id = catalog_category.id')
    ->leftJoin('(
        select * from catalog_product_image
        where id in (
            select min(id) from catalog_product_image group by product_id
        )
    ) as product_image','catalog_product.id = product_image.product_id')
    ->where($where);
    $models = $query->orderBy('catalog_product.price asc')
         ->limit($limit)
         ->all();

      return $models;

  }


}
