<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "catalog_model".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $slug
 * @property int $brand_id
 *
 * @property CatalogProduct[] $catalogProducts
 */
class CatalogModel extends \yii\db\ActiveRecord
{
    public $brand_name;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'catalog_model';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brand_id'], 'required'],
            [['brand_id'], 'default', 'value' => null],
            [['brand_id'], 'integer'],
            [['title', 'slug'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'slug' => 'ЧПУ',
            'brand_id' => 'Бренд',
        ];
    }

    /**
   * @inheritdoc
   */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::class,
                'attribute' => 'title',
                'immutable' => true,
            ]

        ];
    }

    /**
     * Gets query for [[CatalogProducts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogProducts()
    {
        return $this->hasMany(CatalogProduct::className(), ['model_id' => 'id']);
    }

    public function getBrand()
    {
        return $this->hasOne(CatalogBrand::className(), ['id' => 'brand_id']);
    }

    public static function getListData()
    {
        $list_data = self::find()->all();
        $list = [];

        foreach ($list_data as $data) {

          $list[$data->id] = $data->title;
        }

        return $list;
    }
}
