<?php

use yii\db\Migration;

/**
 * Class m201008_083913_alter_table_add_field_category
 */
class m201008_083913_alter_table_add_field_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute('ALTER TABLE catalog_category
                ADD COLUMN "icon_path" varchar(1024)  NULL,
                ADD COLUMN "icon_url" varchar(1024)  NULL
            ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201008_083913_alter_table_add_field_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201008_083913_alter_table_add_field_category cannot be reverted.\n";

        return false;
    }
    */
}
