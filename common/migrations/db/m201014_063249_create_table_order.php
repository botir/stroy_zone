<?php

use yii\db\Migration;

/**
 * Class m201014_063249_create_table_order
 */
class m201014_063249_create_table_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute('
          CREATE TABLE order_order (
              id  bigserial NOT NULL,
              user_full_name varchar(254) NOT NULL,
              user_phone varchar(50) NOT NULL,
              user_email varchar(254) NULL,
              order_code int8 NOT NULL,
              "comments" varchar(1024) NULL,
              total_amount numeric(12,2) NOT NULL,
              region_id int4 NULL,
              district_id int4 NULL,
              shipping_address varchar(254) NULL,
              status int2 NOT NULL DEFAULT 1,
              created_at timestamp NOT NULL,
              updated_at timestamp NOT NULL,
              CONSTRAINT order_order_pkey PRIMARY KEY (id),
              FOREIGN KEY (region_id) REFERENCES geo_region(id) ON UPDATE CASCADE ON DELETE SET NULL,
              FOREIGN KEY (district_id) REFERENCES geo_district(id) ON UPDATE CASCADE ON DELETE SET NULL
            )
      ');
      $this->execute('
          CREATE TABLE order_line (
              id  bigserial NOT NULL,
              product_name varchar(1024) NOT NULL,
              quantity int4 NOT NULL,
              total_amount numeric(12,2) NOT NULL,
              price int4 NOT NULL,
              order_id int8 NOT NULL,
              product_id int8 NULL,
              CONSTRAINT order_line_pkey PRIMARY KEY (id),
              FOREIGN KEY (order_id) REFERENCES order_order(id) ON UPDATE CASCADE ON DELETE CASCADE,
              FOREIGN KEY (product_id) REFERENCES catalog_product(id) ON UPDATE CASCADE ON DELETE SET NULL
            )
      ');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201014_063249_create_table_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201014_063249_create_table_order cannot be reverted.\n";

        return false;
    }
    */
}
