<?php

use yii\db\Migration;

/**
 * Class m200907_152028_new_table_catalog_brand
 */
class m200907_152028_new_table_catalog_brand extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute('
            CREATE TABLE catalog_brand (
                id  serial NOT NULL,
                title varchar(200) NOT NULL,
                slug varchar(200) NULL,
                logo_url varchar(1024) NULL,
                logo_path varchar(1024) NULL,
                CONSTRAINT catalog_brand_pkey PRIMARY KEY (id)
              )
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200907_152028_new_table_catalog_brand cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200907_152028_new_table_catalog_brand cannot be reverted.\n";

        return false;
    }
    */
}
