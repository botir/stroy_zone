<?php

use yii\db\Migration;

/**
 * Class m200907_145513_new_table_catalog_category
 */
class m200907_145513_new_table_catalog_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute('
            CREATE TABLE catalog_category (
                id  serial NOT NULL,
                content_txt jsonb NOT NULL,
                slug varchar(200) NULL,
                parent_id int4 NULL DEFAULT NULL,
                sort_order int4 NULL,
                status int2 NOT NULL DEFAULT 2,
                background_image varchar(100) NULL,
                created_at timestamp NOT NULL,
                updated_at timestamp NOT NULL,
                CONSTRAINT catalog_category_pkey PRIMARY KEY (id),
                FOREIGN KEY (parent_id) REFERENCES catalog_category(id) ON UPDATE CASCADE ON DELETE CASCADE
              )
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200907_145513_new_table_catalog_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200907_145513_new_table_catalog_category cannot be reverted.\n";

        return false;
    }
    */
}
