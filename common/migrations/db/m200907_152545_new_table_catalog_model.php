<?php

use yii\db\Migration;

/**
 * Class m200907_152545_new_table_catalog_model
 */
class m200907_152545_new_table_catalog_model extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute('
            CREATE TABLE catalog_model (
                id  serial NOT NULL,
                title varchar(200) NULL,
                slug varchar(200) NULL,
                brand_id int4 NOT NULL,
                CONSTRAINT catalog_model_pkey PRIMARY KEY (id)
              )
        ');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200907_152545_new_table_catalog_model cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200907_152545_new_table_catalog_model cannot be reverted.\n";

        return false;
    }
    */
}
