<?php

use yii\db\Migration;

/**
 * Class m200907_152908_new_table_catalog_product
 */
class m200907_152908_new_table_catalog_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE catalog_product (
                id  bigserial NOT NULL,
                content_txt jsonb NOT NULL,
                slug varchar(255) NULL,
                category_id int4 NULL DEFAULT NULL,
                brand_id int4 NULL DEFAULT NULL,
                model_id int4 NULL DEFAULT NULL,
                status int2 NOT NULL DEFAULT 2,
                price int4 NULL DEFAULT NULL,
                search_vector tsvector NULL,
                created_at timestamp NOT NULL,
                updated_at timestamp NOT NULL,
                CONSTRAINT catalog_producty_pkey PRIMARY KEY (id),
                FOREIGN KEY (category_id) REFERENCES catalog_category(id) ON UPDATE CASCADE ON DELETE SET NULL,
                FOREIGN KEY (brand_id) REFERENCES catalog_brand(id) ON UPDATE CASCADE ON DELETE SET NULL,
                FOREIGN KEY (model_id) REFERENCES catalog_model(id) ON UPDATE CASCADE ON DELETE SET NULL
              )
        ');
        $this->execute('CREATE INDEX idx_product_vector_vec ON catalog_product USING gin(search_vector);');
        $this->execute('
            CREATE TABLE catalog_product_image (
                id  bigserial NOT NULL,
                product_id int4 NOT NULL,
              	file_name varchar(1024) NOT NULL,
              	file_path varchar(1024) NOT NULL,
              	base_url varchar(1024) NOT NULL,
              	file_type varchar(100) NULL,
              	file_size int4 NULL,
              	sort_order int4 NULL,
              	CONSTRAINT catalog_product_image_pkey PRIMARY KEY (id)
              )
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200907_152908_new_table_catalog_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200907_152908_new_table_catalog_product cannot be reverted.\n";

        return false;
    }
    */
}
