<?php

use yii\db\Migration;

/**
 * Class m201008_124713_alter_table_add_field_product
 */
class m201008_124713_alter_table_add_field_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute('ALTER TABLE catalog_product
                ADD COLUMN top_selling BOOLEAN NOT NULL DEFAULT FALSE,
                ADD COLUMN new_product BOOLEAN NOT NULL DEFAULT FALSE,
                ADD COLUMN is_tranding BOOLEAN NOT NULL DEFAULT FALSE
            ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201008_124713_alter_table_add_field_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201008_124713_alter_table_add_field_product cannot be reverted.\n";

        return false;
    }
    */
}
