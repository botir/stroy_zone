<?php

use yii\db\Migration;

/**
 * Class m201008_112527_create_table_slider
 */
class m201008_112527_create_table_slider extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute('
          CREATE TABLE content_slider (
              id  serial NOT NULL,
              title_ru varchar(200) NULL,
              title_uz varchar(200) NULL,
              img_url varchar(1024) NOT NULL,
              img_path varchar(1024) NOT NULL,
              link_to varchar(100) NULL,
              bg_color varchar(12) NULL,
              CONSTRAINT content_slider_pkey PRIMARY KEY (id)
            )
      ');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201008_112527_create_table_slider cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201008_112527_create_table_slider cannot be reverted.\n";

        return false;
    }
    */
}
