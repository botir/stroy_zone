<?php

use yii\db\Migration;

/**
 * Class m201012_064002_alter_table_maps
 */
class m201012_064002_alter_table_maps extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute('ALTER TABLE system_settings
                ADD COLUMN "maps_iframe" text  NULL
            ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201012_064002_alter_table_maps cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201012_064002_alter_table_maps cannot be reverted.\n";

        return false;
    }
    */
}
