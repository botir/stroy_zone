<?php

use yii\db\Migration;

/**
 * Class m201019_104529_session_table
 */
class m201019_104529_session_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute('CREATE TABLE web_session
        (
            id varchar(50) NOT NULL,
            expire int4 NOT NULL,
            last_write int4 NULL,
            data BYTEA NOT NULL,
            user_id int8 NULL,
            browser_platform varchar(400) NULL,
            PRIMARY KEY (id),
            FOREIGN KEY (user_id) REFERENCES "user"(id) ON UPDATE CASCADE ON DELETE CASCADE
        );');

        $this->execute('CREATE INDEX web_session_expire_with_id_idx ON web_session(id, expire);');
        $this->execute('CREATE INDEX web_session_expire_only_idx ON web_session(expire);');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201019_104529_session_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201019_104529_session_table cannot be reverted.\n";

        return false;
    }
    */
}
