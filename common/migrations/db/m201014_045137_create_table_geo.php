<?php

use yii\db\Migration;

/**
 * Class m201014_045137_create_table_geo
 */
class m201014_045137_create_table_geo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $sql = <<< SQLSQLSQL
      CREATE TABLE geo_country (
id serial NOT NULL,
iso_code varchar(2) NULL,
iso_code3 varchar(3) NULL,
name_uz varchar(100) NOT NULL,
name_ru varchar(100) NOT NULL,
lat numeric(18,12) NULL,
lng numeric(18,12) NULL,
UNIQUE (iso_code3),
UNIQUE (iso_code),
PRIMARY KEY (id)
)

SQLSQLSQL;
      $this->execute($sql);
      $sql = <<< SQLSQLSQL
      CREATE TABLE geo_region (
  	id serial NOT NULL,
    name_uz varchar(100) NOT NULL,
    name_ru varchar(100) NOT NULL,
  	country_id int4 NOT NULL,
  	PRIMARY KEY (id),
  	FOREIGN KEY (country_id) REFERENCES geo_country(id) ON DELETE CASCADE
  )

SQLSQLSQL;
      $this->execute($sql);

      $sql = <<< SQLSQLSQL
      CREATE TABLE geo_district (
  	id serial NOT NULL,
    name_uz varchar(100) NOT NULL,
    name_ru varchar(100) NOT NULL,
  	country_id int4 NOT NULL,
  	region_id int4 NOT NULL,
  	PRIMARY KEY (id),
  	FOREIGN KEY (country_id) REFERENCES geo_country(id) ON DELETE CASCADE,
  	FOREIGN KEY (region_id) REFERENCES geo_region(id) ON DELETE CASCADE
  );

SQLSQLSQL;
      $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201014_045137_create_table_geo cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201014_045137_create_table_geo cannot be reverted.\n";

        return false;
    }
    */
}
