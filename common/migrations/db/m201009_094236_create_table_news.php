<?php

use yii\db\Migration;

/**
 * Class m201009_094236_create_table_news
 */
class m201009_094236_create_table_news extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute('
          CREATE TABLE content_news (
              id  serial NOT NULL,
              slug varchar(255) NOT NULL,
              title_ru varchar(255) NOT NULL,
              title_uz varchar(255) NOT NULL,
              description_uz varchar(1024) NOT NULL,
              description_ru varchar(1024) NOT NULL,
              img_url varchar(1024) NOT NULL,
              img_path varchar(1024) NOT NULL,
              content_ru text NOT NULL,
              content_uz text NOT NULL,
              is_active BOOLEAN NOT NULL DEFAULT true,
              created_at timestamp NOT NULL,
              CONSTRAINT content_news_pkey PRIMARY KEY (id)
            )
      ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201009_094236_create_table_news cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201009_094236_create_table_news cannot be reverted.\n";

        return false;
    }
    */
}
