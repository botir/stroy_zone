<?php

use yii\db\Migration;

/**
 * Class m201008_060116_system_settings
 */
class m201008_060116_system_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute('
          CREATE TABLE system_settings (
              id  serial NOT NULL,
              store_name varchar(100) NULL,
              phone_number varchar(30) NULL,
              email_main varchar(255) NOT NULL,
              email_support varchar(255) NOT NULL,
              address varchar(255) NULL,
              landmark varchar(255) NULL,
              schedule varchar(255) NULL,
              maps_address varchar(255) NULL,
              maps_lat varchar(50) NULL,
              maps_lon varchar(50) NULL,
              fb_link varchar(50) NULL,
              ints_link varchar(50) NULL,
              tw_link varchar(50) NULL,
              you_link varchar(50) NULL,
              logo1_url varchar(1024) NULL,
              logo1_path varchar(1024) NULL,
              logo2_url varchar(1024) NULL,
              logo2_path varchar(1024) NULL,
              CONSTRAINT system_settings_pkey PRIMARY KEY (id)
            )
      ');
      $this->insert('{{%system_settings}}', [
          'id' => 1,
          'store_name' => 'Stroy Zone',
          'phone_number' => '+998 99 8186100',
          'email_main' => 'mbstudio.inf@gmail.com',
          'email_support' => 'mbstudio.inf@gmail.com',
          'address' => 'Ойбек 38 А, г.Ташкент ',
          'landmark' => 'М Ойбек',
          'schedule' => 'Ежедневно 10:00 – 19:00',
          'maps_address' => null,
          'maps_lat' => '69.243326802655',
          'maps_lon' => '41.30398770619608'
      ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201008_060116_system_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201008_060116_system_settings cannot be reverted.\n";

        return false;
    }
    */
}
