<?php

use yii\db\Migration;

/**
 * Class m201014_050342_seed_data_geo
 */
class m201014_050342_seed_data_geo extends Migration
{


    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $sql_string = "
      INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(1, 'AF', 'AFG', 'Afg''oniston', 'Афганистан', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(2, 'AX', 'ALA', 'Aland orollari', 'Аландские острова', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(3, 'AL', 'ALB', 'Albaniya', 'Албания', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(4, 'DZ', 'DZA', 'Jazoir', 'Алжир', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(5, 'AS', 'ASM', 'Amerika Samoasi', 'Американское Самоа', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(6, 'AD', 'AND', 'Andorra', 'Андорра', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(7, 'AO', 'AGO', 'Angola', 'Ангола', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(8, 'AI', 'AIA', 'Angilya', 'Ангилья', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(9, 'AQ', 'ATA', 'Antarktida', 'Антарктида', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(10, 'AG', 'ATG', 'Antigua va Barbuda', 'Антигуа и Барбуда', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(11, 'AR', 'ARG', 'Argentina', 'Аргентина', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(12, 'AM', 'ARM', 'Armaniston', 'Армения', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(13, 'AW', 'ABW', 'Aruba', 'Аруба', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(14, 'AU', 'AUS', 'Avstraliya', 'Австралия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(15, 'AT', 'AUT', 'Avstriya', 'Австрия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(16, 'AZ', 'AZE', 'Ozarbayjon', 'Азербайджан', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(17, 'BS', 'BHS', 'Bahamas', 'Багамы', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(18, 'BH', 'BHR', 'Bahrayn', 'Бахрейн', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(19, 'BD', 'BGD', 'Bangladesh', 'Бангладеш', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(20, 'BB', 'BRB', 'Barbados', 'Барбадос', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(21, 'BY', 'BLR', 'Belarus', 'Белоруссия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(22, 'BE', 'BEL', 'Belgiya', 'Бельгия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(23, 'BZ', 'BLZ', 'Beliz', 'Белиз', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(24, 'BJ', 'BEN', 'Benin', 'Бенин', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(25, 'BM', 'BMU', 'Bermuda', 'Бермуды', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(26, 'BT', 'BTN', 'Butan', 'Бутан', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(27, 'BO', 'BOL', 'Boliviya', 'Боливия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(28, 'BQ', 'BES', 'Bonaire, Saba va Sint-Eustatius', 'Бонайре, Саба и Синт-Эстатиус', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(29, 'BA', 'BIH', 'Bosniya va Gertsegovina', 'Босния и Герцеговина', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(30, 'BW', 'BWA', 'Botswana', 'Ботсвана', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(31, 'BV', 'BVT', 'Buve Oroli', 'Остров Буве', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(32, 'BR', 'BRA', 'Braziliya', 'Бразилия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(33, 'IO', 'IOT', 'Hind okeanidagi Britaniya hududi', 'Британская территория в Индийском океане', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(34, 'BN', 'BRN', 'Bruney', 'Бруней', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(35, 'BG', 'BGR', 'Bolgariya', 'Болгария', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(36, 'BF', 'BFA', 'Burkina Faso', 'Буркина-Фасо', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(37, 'BI', 'BDI', 'Burundi', 'Бурунди', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(38, 'KH', 'KHM', 'Kambodja', 'Камбоджа', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(39, 'CM', 'CMR', 'Kamerun', 'Камерун', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(40, 'CA', 'CAN', 'Kanada', 'Канада', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(41, 'CV', 'CPV', 'Cape Verde', 'Кабо-Верде', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(42, 'KY', 'CYM', 'Kayman orollari', 'Каймановы Острова', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(43, 'CF', 'CAF', 'Markaziy Afrika Respublikasi', 'Центрально-Африканская Республика', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(44, 'TD', 'TCD', 'Chad', 'Чад', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(45, 'CL', 'CHL', 'Chili', 'Чили', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(46, 'CN', 'CHN', 'Xitoy', 'Китай', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(47, 'CX', 'CXR', 'Rojdestvo oroli', 'Остров Рождества', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(48, 'CC', 'CCK', 'Cocos (Keeling) orollari', 'Кокосовые (Килинг) острова', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(49, 'CO', 'COL', 'Kolumbiya', 'Колумбия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(50, 'KM', 'COM', 'Komorlar', 'Коморские острова', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(51, 'CG', 'COG', 'Kongo', 'Конго', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(52, 'CK', 'COK', 'Kuk orollari', 'Острова Кука', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(53, 'CR', 'CRI', 'Kosta-Rika', 'Коста-Рика', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(54, 'CI', 'CIV', 'Cote d''Ivoire', 'Кот-д''Ивуар', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(55, 'HR', 'HRV', 'Xorvatiye', 'Хорватия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(56, 'CU', 'CUB', 'Kuba', 'Куба', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(57, 'CW', 'CUW', 'Curacao', 'Кюрасао', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(58, 'CY', 'CYP', 'Kipr', 'Кипр', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(59, 'CZ', 'CZE', 'Chexiya', 'Чехия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(61, 'DK', 'DNK', 'Daniya', 'Дания', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(62, 'DJ', 'DJI', 'Djibouti', 'Джибути', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(63, 'DM', 'DMA', 'Dominika', 'Доминика', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(64, 'DO', 'DOM', 'Dominikan Respublikasi', 'Доминиканская Республика', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(65, 'EC', 'ECU', 'Ekvador', 'Эквадор', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(66, 'EG', 'EGY', 'Misr', 'Египет', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(67, 'SV', 'SLV', 'Salvador', 'Эль-Сальвадор', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(68, 'GQ', 'GNQ', 'Ekvatorial Gvineya', 'Экваториальная Гвинея', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(69, 'ER', 'ERI', 'Eritreya', 'Эритрея', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(70, 'EE', 'EST', 'Estoniya', 'Эстония', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(71, 'ET', 'ETH', 'Efiopiya', 'Эфиопия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(72, 'FK', 'FLK', 'Falkland orollari', 'Фолклендские острова', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(73, 'FO', 'FRO', 'Farer orollari', 'Фарерские острова', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(74, 'FJ', 'FJI', 'Fidji', 'Фиджи', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(75, 'FI', 'FIN', 'Finlandiya', 'Финляндия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(76, 'FR', 'FRA', 'Fransiya', 'Франция', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(77, 'GF', 'GUF', 'Fransiya Guyanası', 'Французская Гвиана', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(78, 'PF', 'PYF', 'Frantsiya Polineziyasi', 'Французская Полинезия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(79, 'TF', 'ATF', 'Janubiy Fransuz hududlari', 'Южные Французские Территории', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(80, 'GA', 'GAB', 'Gabon', 'Габон', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(81, 'GM', 'GMB', 'Gambiya', 'Гамбия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(82, 'GE', 'GEO', 'Gruziya', 'Грузия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(83, 'DE', 'DEU', 'Germaniya', 'Германия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(84, 'GH', 'GHA', 'Gana', 'Гана', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(85, 'GI', 'GIB', 'Gibraltar', 'Гибралтар', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(86, 'GR', 'GRC', 'Gretsiya', 'Греция', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(87, 'GL', 'GRL', 'Grenlandiya', 'Гренландия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(88, 'GD', 'GRD', 'Grenada', 'Гренада', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(89, 'GP', 'GLP', 'Gvadelupa', 'Гваделупа', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(90, 'GU', 'GUM', 'Guam', 'Гуам', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(91, 'GT', 'GTM', 'Gvatemala', 'Гватемала', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(92, 'GG', 'GGY', 'Gernsi', 'Гернси', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(93, 'GN', 'GIN', 'Gvineya', 'Гвинея', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(94, 'GW', 'GNB', 'Gvineya-Bissau', 'Гвинея-Бисау', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(95, 'GY', 'GUY', 'Guyana', 'Гайана', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(96, 'HT', 'HTI', 'Gaiti', 'Гаити', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(97, 'HM', 'HMD', 'Heard orolining va McDonald orollari', 'Остров Херд и острова Макдональд', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(98, 'HN', 'HND', 'Gonduras', 'Гондурас', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(99, 'HK', 'HKG', 'Gongkong', 'Гонконг', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(100, 'HU', 'HUN', 'Vengriya', 'Венгрия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(101, 'IS', 'ISL', 'Islandiya', 'Исландия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(102, 'IN', 'IND', 'Hindiston', 'Индия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(103, 'ID', 'IDN', 'Indoneziya', 'Индонезия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(104, 'IR', 'IRN', 'Eron', 'Иран', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(105, 'IQ', 'IRQ', 'Iroq', 'Ирак', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(106, 'IE', 'IRL', 'Irlandiya', 'Ирландия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(107, 'IM', 'IMN', 'Man oroli', 'Остров Мэн', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(108, 'IL', 'ISR', 'Isroil', 'Израиль', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(109, 'IT', 'ITA', 'Italiya', 'Италия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(110, 'JM', 'JAM', 'Yamayka', 'Ямайка', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(111, 'JP', 'JPN', 'Yaponiya', 'Япония', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(112, 'JE', 'JEY', 'Jersi', 'Джерси', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(113, 'JO', 'JOR', 'Iordaniya', 'Иордания', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(114, 'KZ', 'KAZ', 'Qozog''iston', 'Казахстан', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(115, 'KE', 'KEN', 'Keniya', 'Кения', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(116, 'KI', 'KIR', 'Kiribati', 'Кирибати', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(117, 'XK', 'XKX', 'Kosovo', 'Косово', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(118, 'KW', 'KWT', 'Quvayt', 'Кувейт', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(119, 'KG', 'KGZ', 'Qirg''iziston', 'Киргизия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(120, 'LA', 'LAO', 'Laos', 'Лаос', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(121, 'LV', 'LVA', 'Latviya', 'Латвия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(122, 'LB', 'LBN', 'Livan', 'Ливан', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(123, 'LS', 'LSO', 'Lesoto', 'Лесото', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(124, 'LR', 'LBR', 'Liberiya', 'Либерия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(125, 'LY', 'LBY', 'Liviya', 'Ливия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(126, 'LI', 'LIE', 'Lixtenshteyn', 'Лихтенштейн', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(127, 'LT', 'LTU', 'Litva', 'Литва', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(128, 'LU', 'LUX', 'Lyuksemburg', 'Люксембург', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(129, 'MO', 'MAC', 'Makao', 'Макао', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(130, 'MK', 'MKD', 'Makedoniya', 'Македония', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(131, 'MG', 'MDG', 'Madagaskar', 'Мадагаскар', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(132, 'MW', 'MWI', 'Malavi', 'Малави', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(133, 'MY', 'MYS', 'Malayziya', 'Малайзия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(134, 'MV', 'MDV', 'Maldiv orollari', 'Мальдивы', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(135, 'ML', 'MLI', 'Mali', 'Мали', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(136, 'MT', 'MLT', 'Malta', 'Мальта', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(137, 'MH', 'MHL', 'Marshall orollari', 'Маршалловы Острова', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(138, 'MQ', 'MTQ', 'Martinique', 'Мартиника', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(139, 'MR', 'MRT', 'Mavritaniya', 'Мавритания', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(140, 'MU', 'MUS', 'Mavrikiy', 'Маврикий', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(141, 'YT', 'MYT', 'Mayotda', 'Майотта', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(142, 'MX', 'MEX', 'Meksika', 'Мексика', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(143, 'FM', 'FSM', 'Mikroneziya, federal davlatlar', 'Микронезии, Федеративные Штаты', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(144, 'MD', 'MDA', 'Moldova', 'Молдавия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(145, 'MC', 'MCO', 'Monako', 'Монако', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(146, 'MN', 'MNG', 'Mo''g''uliston', 'Монголия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(147, 'ME', 'MNE', 'Chernogoriya', 'Черногория', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(148, 'MS', 'MSR', 'Montserrat', 'Монтсерат', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(149, 'MA', 'MAR', 'Marokash', 'Марокко', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(150, 'MZ', 'MOZ', 'Mozambik', 'Мозамбик', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(151, 'MM', 'MMR', 'Myanma', 'Мьянма', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(152, 'NA', 'NAM', 'Namibiya', 'Намибия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(153, 'NR', 'NRU', 'Nauru', 'Науру', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(154, 'NP', 'NPL', 'Nepal', 'Непал', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(155, 'NL', 'NLD', 'Gollandiya', 'Нидерланды', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(156, 'NC', 'NCL', 'Yangi Kaledoniya', 'Новая Каледония', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(157, 'NZ', 'NZL', 'Yangi Zelandiya', 'Новая Зеландия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(158, 'NI', 'NIC', 'Nikaragua', 'Никарагуа', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(159, 'NE', 'NER', 'Niger', 'Нигер', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(160, 'NG', 'NGA', 'Nigeriya', 'Нигерия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(161, 'NU', 'NIU', 'Niue', 'Ниуэ', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(162, 'NF', 'NFK', 'Norvegiya', 'Норвегия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(163, 'KP', 'PRK', 'Shimoliy Koreya', 'Корея, северная', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(164, 'MP', 'MNP', 'Shimoliy Mariana orollari', 'Северные Марианские острова', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(165, 'NO', 'NOR', 'Norvegiya', 'Норвегия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(166, 'OM', 'OMN', 'Ummon', 'Оман', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(167, 'PK', 'PAK', 'Pokiston', 'Пакистан', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(168, 'PW', 'PLW', 'Palau', 'Палау', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(169, 'PS', 'PSE', 'Falastin', 'Палестина', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(170, 'PA', 'PAN', 'Panama', 'Панама', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(171, 'PG', 'PNG', 'Papua-Yangi Gvineya', 'Папуа — Новая Гвинея', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(172, 'PY', 'PRY', 'Paragvay', 'Парагвай', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(173, 'PE', 'PER', 'Peru', 'Перу', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(174, 'PH', 'PHL', 'Filippin', 'Филиппины', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(175, 'PN', 'PCN', 'Pitkern', 'Питкерн', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(176, 'PL', 'POL', 'Polsha', 'Польша', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(177, 'PT', 'PRT', 'Portugaliya', 'Португалия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(178, 'PR', 'PRI', 'Puerto-Riko', 'Пуэрто-Рико', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(179, 'QA', 'QAT', 'Qatar', 'Катар', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(180, 'RE', 'REU', 'Reunion', 'Реюньон', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(181, 'RO', 'ROU', 'Ruminiya', 'Румыния', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(182, 'RU', 'RUS', 'Rossiya', 'Россия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(183, 'RW', 'RWA', 'Ruandada', 'Руанда', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(184, 'BL', 'BLM', 'Saint Barthelemy', 'Сен-Бартельми', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(185, 'SH', 'SHN', 'Saint Helena', 'Святая Елена', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(186, 'KN', 'KNA', 'Sent-Kits va Nevis', 'Сент-Китс и Невис', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(187, 'LC', 'LCA', 'Saint Lucia', 'Сент-Люсия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(188, 'MF', 'MAF', 'Saint-Martin', 'Сен-Мартен', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(189, 'PM', 'SPM', 'Saint Pierre and Miquelon', 'Сент-Пьер и Микелон', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(190, 'VC', 'VCT', 'Saint Vincent and the Grenadines', 'Сент-Винсент и Гренадины', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(191, 'WS', 'WSM', 'Samoa', 'Самоа', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(192, 'SM', 'SMR', 'San-Marino', 'Сан-Марино', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(193, 'ST', 'STP', 'San-Tome va Principe', 'Сан-Томе и Принсипи', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(194, 'SA', 'SAU', 'Saudiya Arabistoni', 'Саудовская Аравия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(195, 'SN', 'SEN', 'Senegal', 'Сенегал', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(196, 'RS', 'SRB', 'Serbiya', 'Сербия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(197, 'SC', 'SYC', 'Seyshel orollari', 'Сейшельские острова', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(198, 'SL', 'SLE', 'Sierra Leon', 'Сьерра-Леоне', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(199, 'SG', 'SGP', 'Singapur', 'Сингапур', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(200, 'SX', 'SXM', 'Sint-Marten', 'Синт-Мартен', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(201, 'SK', 'SVK', 'Slovakiya', 'Словакия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(202, 'SI', 'SVN', 'Sloveniya', 'Словения', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(203, 'SB', 'SLB', 'Solomon orollari', 'Соломоновы Острова', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(204, 'SO', 'SOM', 'Somali', 'Сомали', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(205, 'ZA', 'ZAF', 'Janubiy Afrika', 'Южно-Африканская Республика (ЮАР)', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(206, 'GS', 'SGS', 'Janubiy Jorjiya va Janubiy Sandvich orollari', 'Южная Джорджия и Южные Сандвичевы острова', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(207, 'KR', 'KOR', 'Janubiy Koreya', 'Корея, южная', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(208, 'SS', 'SSD', 'Janubiy Sudan', 'южный Судан', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(209, 'ES', 'ESP', 'Ispaniya', 'Испания', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(210, 'LK', 'LKA', 'Shri Lanka', 'Шри-Ланка', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(211, 'SD', 'SDN', 'Sudan', 'Судан', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(212, 'SR', 'SUR', 'Surinam', 'Суринам', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(213, 'SJ', 'SJM', 'Spitsbergen va Yan Mayen', 'Шпицберген и Ян Майен', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(214, 'SZ', 'SWZ', 'Svazilend', 'Свазиленд', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(215, 'SE', 'SWE', 'Shvetsiya', 'Швеция', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(216, 'CH', 'CHE', 'Shveytsariya', 'Швейцария', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(217, 'SY', 'SYR', 'Suriya', 'Сирия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(218, 'TW', 'TWN', 'Tayvan', 'Тайвань', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(219, 'TJ', 'TJK', 'Tojikiston', 'Таджикистан', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(220, 'TZ', 'TZA', 'Tanzaniya', 'Танзания', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(221, 'TH', 'THA', 'Tailand', 'Таиланд', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(222, 'TL', 'TLS', 'Sharqiy Timor', 'Восточный Тимор', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(223, 'TG', 'TGO', 'Togo', 'Того', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(224, 'TK', 'TKL', 'Tokelau', 'Токелау', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(225, 'TO', 'TON', 'Togo', 'Того', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(226, 'TT', 'TTO', 'Trinidad va Tobago', 'Тринидад и Тобаго', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(227, 'TN', 'TUN', 'Tunis', 'Тунис', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(228, 'TR', 'TUR', 'Turkiya', 'Турция', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(229, 'TM', 'TKM', 'Turkmaniston', 'Туркменистан', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(230, 'TC', 'TCA', 'Turklar va Kaykos orollari', 'Острова Теркс и Кайкос', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(231, 'TV', 'TUV', 'Tuvalu', 'Тувалу', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(232, 'UG', 'UGA', 'Uganda', 'Уганда', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(233, 'UA', 'UKR', 'Ukraina', 'Украина', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(234, 'AE', 'ARE', 'Birlashgan Arab Amirliklari', 'Объединённые Арабские Эмираты (ОАЭ)', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(235, 'GB', 'GBR', 'Buyuk Britianiya', 'Великобритания', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(236, 'US', 'USA', 'AQSh', 'США', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(237, 'UM', 'UMI', 'Amerika Qo''shma Shtatlari Kichik Tashqi orollari', 'Внешние малые острова США', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(238, 'UY', 'URY', 'Urugvay', 'Уругвай', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(239, 'UZ', 'UZB', 'O''zbekiston', 'Узбекистан', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(240, 'VU', 'VUT', 'Vanuatu', 'Вануату', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(241, 'VA', 'VAT', 'Vatikan', 'Ватикан', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(242, 'VE', 'VEN', 'Venesuela', 'Венесуэла', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(243, 'VN', 'VNM', 'Vetnam', 'Вьетнам', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(244, 'VG', 'VGB', 'Britaniya Virjiniya orollari, ingliz', 'Виргинские Острова, Британские', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(245, 'VI', 'VIR', 'Virgin orollari, amerika', 'Виргинские Острова, Американские', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(246, 'WF', 'WLF', 'Wallis va Futuna', 'Уоллис и Футуна', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(247, 'EH', 'ESH', 'G''arbiy Sahara', 'Западная Сахара', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(248, 'YE', 'YEM', 'Yaman', 'Йемен', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(249, 'ZM', 'ZMB', 'Zambiya', 'Замбия', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(250, 'ZW', 'ZWE', 'Zimbabve', 'Зимбабве', NULL, NULL);
    INSERT INTO public.geo_country
    (id, iso_code, iso_code3, name_uz, name_ru, lat, lng)
    VALUES(251, '12', '123', 'Angliya', 'Англия', 0.000000000000, 0.000000000000);
";
$arr =  explode(";", $sql_string);
foreach ($arr as $sql) {
try {
    $this->execute($sql);
} catch (\Exception $e) {
  echo "ERROR: ". $sql;
}
}
$sql_string = "
INSERT INTO public.geo_region
(id, name_uz, name_ru, country_id)
VALUES(1, 'Andijon', 'Андижан', 239);
INSERT INTO public.geo_region
(id, name_uz, name_ru, country_id)
VALUES(2, 'Buxoro', 'Бухара', 239);
INSERT INTO public.geo_region
(id, name_uz, name_ru, country_id)
VALUES(3, 'Jizzah', 'Джиза́к', 239);
INSERT INTO public.geo_region
(id, name_uz, name_ru, country_id)
VALUES(4, 'Navoiy', 'Навои', 239);
INSERT INTO public.geo_region
(id, name_uz, name_ru, country_id)
VALUES(5, 'Namangan', 'Наманган', 239);
INSERT INTO public.geo_region
(id, name_uz, name_ru, country_id)
VALUES(6, 'Farg’ona', 'Фергана', 239);
INSERT INTO public.geo_region
(id, name_uz, name_ru, country_id)
VALUES(7, 'Sirdaryo', '	Сырдарья', 239);
INSERT INTO public.geo_region
(id, name_uz, name_ru, country_id)
VALUES(8, 'Samarqand', 'Самарканд', 239);
INSERT INTO public.geo_region
(id, name_uz, name_ru, country_id)
VALUES(9, 'Qashqadaryo', '	Кашкадарья', 239);
INSERT INTO public.geo_region
(id, name_uz, name_ru, country_id)
VALUES(10, 'Surxondaryo', 'Сурхандарья', 239);
INSERT INTO public.geo_region
(id, name_uz, name_ru, country_id)
VALUES(11, 'Xorazm', 'Хорезм', 239);
INSERT INTO public.geo_region
(id, name_uz, name_ru, country_id)
VALUES(12, 'Toshkent viloyati', 'Ташкентская область', 239);
INSERT INTO public.geo_region
(id, name_uz, name_ru, country_id)
VALUES(13, 'Toshkent shahri', 'Город Ташкент', 239);
INSERT INTO public.geo_region
(id, name_uz, name_ru, country_id)
VALUES(14, 'Qoraqalpog’iston Respublikasi', 'Республика Каракалпакстан', 239);
";
  $arr =  explode(";", $sql_string);
  foreach ($arr as $sql) {
    try {
        $this->execute($sql);
    } catch (\Exception $e) {
      echo "ERROR: ". $sql;
    }
  }

    $sql_string = "
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(1, 'Andijon (tuman)', 'Андижанский', 239, 1);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(3, 'Asaka tumani', 'Асакинский', 239, 1);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(4, 'Baliqchi tumani', 'Балыкчинский', 239, 1);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(5, 'Bo`z tumani', 'Бозский', 239, 1);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(6, 'Buloqboshi tumani', 'Булакбашинский', 239, 1);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(7, 'Izboskan tumani', 'Избасканский', 239, 1);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(8, 'Jalaquduq tumani', 'Джалакудукский', 239, 1);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(9, 'Xo''jaobod tumani', 'Ходжаабадский', 239, 1);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(10, 'Qo`rg`ontepa  tumani', 'Кургантепинский', 239, 1);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(11, 'Marhamat tumani', 'Мархаматский', 239, 1);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(12, 'Oltinko`l tumani', 'Алтынкульский', 239, 1);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(13, 'Paxtaobod tumani', 'Пахтаабадский', 239, 1);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(14, 'Shahrixon tumani', 'Шахриханский', 239, 1);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(15, 'Ulug`nor tumani', 'Улугнорский', 239, 1);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(16, 'Olot tumani', 'Алатский', 239, 2);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(17, 'Buxoro tumani', 'Бухарский', 239, 2);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(18, 'G`ijduvon tumani', 'Гиждуванский', 239, 2);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(19, 'Jondor tumani', 'Жондорский', 239, 2);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(20, 'Kogon tumani', 'Каганский', 239, 2);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(21, 'Qorako`l tumani', 'Каракульский ', 239, 2);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(22, 'Qorovulbozor tumani', 'Караулбазарский', 239, 2);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(23, 'Peshku tumani', 'Пешкунский ', 239, 2);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(24, 'Romitan tumani', 'Ромитанский ', 239, 2);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(25, 'Shofirkon tumani', 'Шафирканский', 239, 2);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(26, 'Vobkent tumani​', 'Вабкентский', 239, 2);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(27, 'Arnasoy tumani', 'Арнасайский', 239, 3);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(28, 'Baxmal tumani​', 'Галляаральский', 239, 3);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(30, 'Do''stlik tumani', 'Джизакский', 239, 3);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(31, 'Forish tumani', 'Фаришский', 239, 3);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(32, 'G`allaorol tumani', 'Галляаральский', 239, 3);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(33, 'Sharof Rashidov tumani', 'Шароф Рашидовский район', 239, 3);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(34, 'Mirzacho''l tumani​', 'Мирзачульский', 239, 3);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(35, 'Paxtakor tumani', 'Пахтакорский', 239, 3);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(36, 'Yangiobod tumani', 'Янгиабадский', 239, 3);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(37, 'Zomin tumani', 'Зааминский', 239, 3);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(38, 'Zafarobod tumani', 'Зафарабадский', 239, 3);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(39, 'Zarbdor tumani', 'Зарбдарский', 239, 3);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(40, 'Konimex tumani', 'Канимехский', 239, 4);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(41, 'Karmana tumani​', 'Карманский район', 239, 4);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(42, 'Qiziltepa tumani', 'Кызылтепинский', 239, 4);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(43, 'Xatirchi tumani', 'Хатырчинский', 239, 4);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(44, 'Navbahor tumani', 'Навбахорский', 239, 4);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(45, 'Nurota tumani', 'Нуратинский', 239, 4);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(46, 'Tomdi tumani​', 'Тамдынский район', 239, 4);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(47, 'Uchquduq tumani​', 'Учкурганский', 239, 4);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(48, 'Chortoq tumani', 'Чартакский', 239, 5);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(49, 'Chust tumani', 'Чустский', 239, 5);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(50, 'Kosonsoy tumani', 'Касансайский', 239, 5);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(51, 'Mingbuloq tumani​', 'Мингбулакский', 239, 5);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(52, 'Namangan tumani​', 'Наманганский', 239, 5);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(53, 'Norin tumani ', 'Нарынский', 239, 5);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(54, 'Pop tumani', 'Папский', 239, 5);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(56, 'To''raqo''rg''on tumani', 'Туракурганский', 239, 5);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(57, 'Uchqo''rg''on tumani', 'Учкурганский', 239, 5);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(58, 'Uychi tumani​', 'Уйчинский', 239, 5);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(59, 'Yangiqo''rg''on tumani', 'Янгикурганский', 239, 5);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(61, 'Zafarobod tumani', 'Зафаборский район', 239, 5);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(62, 'Oltiariq tumani​', 'Алтыарыкский', 239, 6);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(64, 'Bag''dod tumani', 'Багдадский', 239, 6);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(65, 'Beshariq tumani', 'Бешарыкский', 239, 6);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(66, 'Buvayda tumani​', 'Бувайдинский', 239, 6);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(67, 'Dang''ara tumani', 'Дангаринский', 239, 6);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(68, 'Farg''ona tumani', 'Ферганский', 239, 6);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(69, 'Furqat tumani', 'Фуркатский', 239, 6);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(70, 'Qo''shtepa tumani', 'Куштепинский', 239, 6);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(71, 'Quva tumani', 'Кувинский', 239, 6);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(72, 'Rishton tumani', 'Риштанский', 239, 6);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(73, 'So''x tumani', 'Сохский', 239, 6);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(74, 'Toshloq tumani​', 'Ташлакский', 239, 6);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(75, 'Uchko''prik tumani', 'Учкуприкский', 239, 6);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(76, 'O''zbekiston tumani', 'Узбекистанский', 239, 6);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(77, 'Yozyovon tumani', 'Язъяванский', 239, 6);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(78, 'Oqoltin tumani', 'Акалтынский', 239, 7);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(79, 'Boyovut tumani', 'Баяутский', 239, 7);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(80, 'Guliston tumani', 'Гулистанский', 239, 7);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(81, 'Xovos tumani​', 'Хавастский', 239, 7);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(82, 'Mirzaobod tumani​', 'Мирзаабадский', 239, 7);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(83, 'Sayxunobod tumani', 'Сайхунабадский', 239, 7);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(84, 'Sharof Rashidov tumani', 'Шароф Рашидовский район', 239, 7);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(85, 'Sirdaryo tumani', 'Сырдарьинский', 239, 7);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(86, 'Mehnatobod tumani', 'Мирзаабадский', 239, 7);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(87, 'Bulung''ur tumani', 'Булунгурский район', 239, 8);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(88, 'Ishtixon tumani', 'Иштыханский район', 239, 8);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(89, 'Jomboy tumani', 'Джамбайский район', 239, 8);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(90, 'Kattaqo''rg''on tumani', 'Каттакурганский район', 239, 8);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(91, 'Qo''shrabot tumani', 'Кошрабадский район', 239, 8);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(92, 'Narpay tumani', 'Нарпайский район', 239, 8);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(93, 'Nurobod tumani', 'Нурабадский район', 239, 8);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(94, 'Oqdaryo tumani', 'Окгарийский район', 239, 8);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(95, 'Paxtachi tumani', 'Пахтачийский район', 239, 8);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(96, 'Payariq tumani', 'Пайарыкский район', 239, 8);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(97, 'Pastdarg''om tumani', 'Пастдаргомский район', 239, 8);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(98, 'Samarqand tumani', 'Самаркандский район', 239, 8);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(99, 'Toyloq tumani​', 'Тайлакский район', 239, 8);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(100, 'Urgut tumani', 'Ургутский район', 239, 8);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(101, 'Qarshi tumani', 'Каршинский район', 239, 9);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(102, 'Qamashi tumani', 'Камашинский район', 239, 9);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(103, 'Koson tumani', 'Касанский район', 239, 9);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(104, 'Kasbi tumani', 'Касбийский район', 239, 9);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(105, 'Kitob tumani', 'Китабский район', 239, 9);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(106, 'Mirishkor tumani', 'Миришкорский район', 239, 9);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(107, 'Muborak tumani', 'Мубарекский район', 239, 9);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(108, 'Nishon tumani', 'Нишанский район', 239, 9);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(109, 'Shahrisabz tumani', 'Шахрисабзский район', 239, 9);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(110, 'Yakkabog'' tumani', 'Яккабагский район', 239, 9);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(111, 'Chiroqchi tumani', 'Чиракчинский район', 239, 9);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(112, 'Dehqonobod tumani', 'Дехканабадский район', 239, 9);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(113, 'G''uzor tumani', 'Гузарский район', 239, 9);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(114, 'Angor tumani', 'Ангорский район‎', 239, 10);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(115, 'Bandixon tumani', 'Бандыханский район', 239, 10);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(116, 'Boysun tumani', 'Байсунский район‎ ', 239, 10);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(117, 'Denov tumani', 'Денауский район‎', 239, 10);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(118, 'Jarqo''rg''on tumani', 'Джаркурганский район‎ ', 239, 10);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(119, 'Qiziriq tumani', 'Кизирикский район‎ ', 239, 10);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(120, 'Qumqo''rg''on tumani', 'Кумкурганский район‎', 239, 10);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(121, 'Muzrabot tumani', 'Музрабадский район‎', 239, 10);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(122, 'Oltinsoy tumani', 'Алтынсайский район‎', 239, 10);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(123, 'Sariosiyo tumani', 'Сариасийский район', 239, 10);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(124, 'Sherobod tumani', 'Шерабадский район‎ ', 239, 10);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(125, 'Sho''rchi tumani', 'Шурчинский район‎', 239, 10);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(126, 'Termiz tumani', 'Термезский район‎', 239, 10);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(127, 'Uzun tumani', 'Узунский район‎', 239, 10);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(128, 'Bog''ot tumani', 'Багатский район', 239, 11);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(129, 'Gurlan tumani', 'Гурленский район', 239, 11);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(130, 'Xonqa tumani', 'Ханкинский район', 239, 11);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(131, 'Hazorasp tumani', 'Хазараспский район', 239, 11);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(132, 'Xiva tumani', 'Хивинский район', 239, 11);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(133, 'Qo''shko''pir tumani', 'Кошкупырский район', 239, 11);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(134, 'Shovot tumani', 'Шаватский район', 239, 11);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(135, 'Urganch tumani', 'Ургенчский район', 239, 11);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(136, 'Yangiariq tumani', 'Янгиарыкский район', 239, 11);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(137, 'Yangibozor tumani', 'Янгибазарский район', 239, 11);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(138, 'Bekobod tumani', 'Бекабадский район', 239, 12);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(139, 'Bo''stonliq tumani', 'Бостанлыкский район', 239, 12);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(140, 'Bo''ka tumani', 'Букинский район', 239, 12);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(141, 'Chinoz tumani', 'Чиназский район', 239, 12);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(142, 'Qibray tumani', 'Кибрайский район', 239, 12);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(143, 'Ohangaron tumani', 'Ахангаранский район', 239, 12);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(144, 'Oqqo''rg''on tumani', 'Аккурганский район', 239, 12);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(145, 'Parkent tumani', 'Паркентский район', 239, 12);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(146, 'Piskent tumani', 'Пскентский район', 239, 12);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(147, 'Quyi chirchiq tumani', 'Куйичирчикский район', 239, 12);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(148, 'O''rta Chirchiq tumani', 'Уртачирчикский район ', 239, 12);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(149, 'Yangiyo''l tumani', 'Янгиюльский район', 239, 12);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(150, 'Yuqori Chirchiq tumani', 'Юкоричирчикский район ', 239, 12);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(151, 'Zangiota tumani', 'Зангиатинский район', 239, 12);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(152, 'Бектемир тумани', 'Бектемирский район', 239, 13);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(153, 'Chilonzor tumani', 'Чиланзарский район', 239, 13);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(154, 'Yashnobod tumani', 'Яшнабадский район', 239, 13);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(155, 'Mirobod tumani', 'Мирабадский район', 239, 13);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(156, 'Mirzo Ulug''bek tumani', 'Мирзо-Улугбекский район', 239, 13);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(157, 'Sergeli tumani', 'Сергелийский район', 239, 13);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(158, 'Shayxontohur tumani', 'Шайхантахурский район', 239, 13);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(159, 'Olmazor tumani', 'Алмазарский район', 239, 13);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(160, 'Uchtepa tumani', 'Учтепинский район', 239, 13);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(161, 'Yakkasaroy tumani', 'Яккасарайский район', 239, 13);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(162, 'Yunusobod tumani', 'Юнусабадский район', 239, 13);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(163, 'Amudaryo tumani', 'Амударьинский', 239, 14);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(164, 'Beruniy tumani', 'Берунийский', 239, 14);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(165, ' 	Chimboy tumani', ' Чимбайский ', 239, 14);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(166, 'Ellikqal`a tumani', ' Элликкалинский', 239, 14);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(167, 'Kegeyli tumani', 'Кегейлийский', 239, 14);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(168, 'Mo''ynoq tumani', ' 	Муйнакский', 239, 14);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(169, 'Nukus tumani', 'Нукусский', 239, 14);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(170, 'Qanliko''l tumani', 'Канлыкульский', 239, 14);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(171, 'Qo''ng''irot tumani', 'Кунградский', 239, 14);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(172, 'Qorao''zak tumani​', ' 	Караузякский', 239, 14);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(173, 'Shumanay tumani', 'Шуманайский', 239, 14);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(174, 'Taxtako''pir tumani', 'Тахтакупырский', 239, 14);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(175, 'To''rtko''l tumani', 'Турткульский', 239, 14);
    INSERT INTO public.geo_district
    (id, name_uz, name_ru, country_id, region_id)
    VALUES(176, 'Xo''jayli tumani', 'Ходжейлийский', 239, 14);

      ";
      $arr =  explode(";", $sql_string);
      foreach ($arr as $sql) {
      try {
        $this->execute($sql);
      } catch (\Exception $e) {
      echo "ERROR: ". $sql;
      }
      }
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201014_050342_seed_data_geo cannot be reverted.\n";

        return false;
    }
    */
}
