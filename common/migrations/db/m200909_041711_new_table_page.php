<?php

use yii\db\Migration;

/**
 * Class m200909_041711_new_table_page
 */
class m200909_041711_new_table_page extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute('
          CREATE TABLE content_page (
              id  bigserial NOT NULL,
              slug varchar(255) NULL,
              title_uz varchar(255) NOT NULL,
              title_ru varchar(255) NOT NULL,
              content_uz text NOT NULL,
              content_ru text NOT NULL,
              is_active bool NOT NULL DEFAULT true,
              is_menu bool NOT NULL DEFAULT false,
              created_at timestamp NOT NULL,
              updated_at timestamp NOT NULL,
              CONSTRAINT content_page_pkey PRIMARY KEY (id)
            )
      ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200909_041711_new_table_page cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200909_041711_new_table_page cannot be reverted.\n";

        return false;
    }
    */
}
