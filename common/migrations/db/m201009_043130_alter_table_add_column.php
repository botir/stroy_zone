<?php

use yii\db\Migration;

/**
 * Class m201009_043130_alter_table_add_column
 */
class m201009_043130_alter_table_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute('ALTER TABLE catalog_category
                ADD COLUMN is_main BOOLEAN NOT NULL DEFAULT FALSE
            ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201009_043130_alter_table_add_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201009_043130_alter_table_add_column cannot be reverted.\n";

        return false;
    }
    */
}
