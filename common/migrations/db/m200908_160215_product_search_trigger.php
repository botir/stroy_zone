<?php

use yii\db\Migration;

/**
 * Class m200908_160215_product_search_trigger
 */
class m200908_160215_product_search_trigger extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->execute('
             CREATE OR REPLACE FUNCTION product_vector_update() RETURNS TRIGGER AS $$
                 BEGIN
                 NEW.search_vector := to_tsvector(NEW.content_txt->>\'title_ru\') || \' \' || to_tsvector(NEW.content_txt->>\'title_uz\') || \' \' || to_tsvector(NEW.content_txt->>\'description_uz\') || \' \' || to_tsvector(NEW.content_txt->>\'description_ru\');
               RETURN NEW;
             END;
           $$ LANGUAGE plpgsql;');
           $this->execute('
           CREATE TRIGGER product_content_vector_changed
             BEFORE INSERT OR UPDATE ON catalog_product
             FOR EACH ROW
             EXECUTE PROCEDURE product_vector_update();');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200908_160215_product_search_trigger cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200908_160215_product_search_trigger cannot be reverted.\n";

        return false;
    }
    */
}
